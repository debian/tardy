#
#       tardy - a tar post-processor
#       Copyright (C) 1996, 1999, 2000, 2008 Peter Miller
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program. If not, see
#       <http://www.gnu.org/licenses/>.
#
# This could be done much more simply using GNU awk's gsub function, but
# we can't expect the installing site to have already installed it.
# So, do it the slow way.
#
/^['.]\\"/ { next }
{
        len = length($0)
        result = ""
        for (j = 1; j <= len; ++j)
        {
                c = substr($0, j, 1)
                if (c == "\\")
                        c = "\\\\"
                else if (c == "\"")
                        c = "\\\""
                result = result c
        }
        print "\"" result "\","
}
