#!/bin/sh
#
#       tardy - a tar post-processor
#       Copyright (C) 1993-1995, 1998-2000, 2008, 2011 Peter Miller
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program. If not, see
#       <http://www.gnu.org/licenses/>.
#
TAB=`awk 'BEGIN{printf("%c",9)}' /dev/null`
man1_files=
test_files=
common_files=
clean_files="core common/lib.a .bin .bindir .man1dir"
for file in $*
do
    case $file in

    tardy/*.y)
        stem=`echo $file | sed 's/\.y$//'`
        clean_files="$clean_files ${stem}.gen.cc ${stem}.gen.h"
        tardy_files="$tardy_files ${stem}.gen.o"
        clean_files="$clean_files ${stem}.gen.o"
        ;;

    tardy/*.cc)
        stem=`echo $file | sed 's/\.cc$//'`
        tardy_files="$tardy_files ${stem}.o"
        clean_files="$clean_files ${stem}.o"
        ;;

    common/*.cc)
        root=`echo $file | sed 's|\.cc$||'`
        common_files="$common_files ${root}.o"
        clean_files="$clean_files ${root}.o"
        ;;

    man1/*.1)
        root=`basename $file .1`
        clean_files="$clean_files man1/${root}.h"
        man1_files="$man1_files \$(mandir)/$file"
        ;;

    man1/*.so)
        root=`basename $file .so`
        clean_files="$clean_files man1/${root}.h"
        ;;

    test/*/*)
        root=`basename $file .sh`
        test_files="$test_files $root"
        ;;

    *)
        ;;
    esac
done

echo
echo "TardyFiles =" $tardy_files
echo
echo "CommonFiles =" $common_files
echo
echo "Man1Files =" $man1_files
echo
echo "TestFiles =" $test_files

#
# clean up the area
#       (make sure command lines do not get too long)
#
echo ''
echo 'clean-obj:'
echo $clean_files | tr ' ' '\12' | gawk '{
    if (pos > 0 && pos + length($1) > 71) { printf("\n"); pos = 0; }
    if (pos == 0) { printf "\trm -f"; pos = 13; }
    printf " %s", $1
    pos += 1 + length($1);
}
END { if (pos) printf "\n"; }'

cat << fubar

clean: clean-obj
${TAB}rm -f bin/tardy

distclean: clean
${TAB}rm -f config.status config.cache config.log
${TAB}rm -f Makefile common/config.h etc/howto.conf

.bin:
${TAB}-mkdir bin
${TAB}@touch .bin

common/lib.a: \$(CommonFiles)
${TAB}rm -f common/lib.a
${TAB}\$(AR) qc common/lib.a \$(CommonFiles)
${TAB}\$(RANLIB) common/lib.a

bin/tardy: \$(TardyFiles) common/lib.a .bin
${TAB}@sleep 1
${TAB}\$(CXX) -o bin/tardy \$(TardyFiles) common/lib.a \$(LIBS)
${TAB}@sleep 1

sure: \$(TestFiles)
${TAB}@echo Passed All Tests

.bindir:
${TAB}-\$(INSTALL) -m 0755 -d \$(bindir)
${TAB}@touch \$@
${TAB}@sleep 1

.man1dir:
${TAB}-\$(INSTALL) -m 0755 -d \$(mandir)/man1
${TAB}@touch \$@
${TAB}@sleep 1

\$(bindir)/tardy: bin/tardy .bindir
${TAB}\$(INSTALL_PROGRAM) bin/tardy \$@

install-bin: \$(bindir)/tardy

install-man: \$(Man1Files)

install: install-bin install-man
fubar
exit 0
