#! /bin/sh
#
#       tardy - a tar post-processor
#       Copyright (C) 1993-1995, 1998-2001, 2008 Peter Miller
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program. If not, see
#       <http://www.gnu.org/licenses/>.
#
case $# in
3)
    depfile="$3"
    ;;
2)
    depfile=/dev/null
    ;;
*)
    echo "usage: $0 filename resolved-filename" 1>&2
    exit 1
    ;;
esac
file="$1"
rfn="$2"

TAB=`awk 'BEGIN{printf("%c",9)}' /dev/null`

case $file in

*/*.y)
    root=`basename $file .y`
    stem=`echo $file | sed 's/\.y$//'`
    dir=`echo $file | sed 's|/.*||'`

    numconf="no"

    echo ""
    echo "${stem}.gen.cc ${stem}.gen.h: $file"
    echo "${TAB}@echo Expect $numconf conflicts."
    echo "${TAB}\$(YACC) -d $file"
    echo "${TAB}sed -e 's/[yY][yY]/${root}_/g' y.tab.c > ${stem}.gen.cc"
    echo "${TAB}sed -e 's/[yY][yY]/${root}_/g' y.tab.h > ${stem}.gen.h"
    echo "${TAB}rm y.tab.c y.tab.h"

    depfile=`echo $file | sed 's/\.y$/.gen.cc.d/'`
    if [ -r $depfile ]; then
        dep=`sed -e '1d' -e '$d' -e 's_.arch]/__' $depfile`
    else
        depfile=`echo $rfn | sed 's/\.y$/.gen.cc.d/'`
        if [ -r $depfile ]; then
            dep=`sed -e '1d' -e '$d' -e 's_.arch]/__' $depfile`
        fi
    fi

    echo ""
    echo "${stem}.gen.o: ${stem}.gen.cc" $dep
    echo "${TAB}\$(CXX) \$(CFLAGS) -I$dir -Icommon -c ${stem}.gen.cc"
    echo "${TAB}mv ${root}.gen.o ${stem}.gen.o"
    ;;

*/*.cc)
    root=`basename $file .cc`
    stem=`echo $file | sed 's/\.cc$//'`
    dir=`echo $file | sed 's|/.*||'`

    dep=`cat $depfile`

    echo ""
    echo "${stem}.o: $file" $dep
    echo "${TAB}\$(CXX) \$(CFLAGS)" "-I$dir -Icommon -c $file"
    echo "${TAB}mv ${root}.o ${stem}.o"
    ;;

man1/*.1)
    root=`basename $file .1`
    echo ""
    echo "man1/$root.h: $file etc/txt2c.awk"
    echo "${TAB}awk -f etc/txt2c.awk $file > man1/$root.h"
    echo ""
    echo "\$(mandir)/$file: $file .man1dir"
    echo "${TAB}(cd man1; soelim $root.1) > tmp"
    echo "${TAB}\$(INSTALL_DATA) tmp \$(mandir)/$file"
    echo "${TAB}@rm -f tmp"
    ;;

man1/*.so)
    root=`basename $file .so`
    echo ""
    echo "man1/$root.h: $file etc/txt2c.awk"
    echo "${TAB}awk -f etc/txt2c.awk $file > man1/$root.h"
    ;;

test/*/*.sh)
    root=`basename $file .sh`
    echo ""
    echo "$root: $file all"
    echo "${TAB}\$(SH) $file"
    ;;

*)
    ;;
esac
exit 0
