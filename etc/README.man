'\" t
.\" tardy - a tar post-processor
.\" Copyright (C) 1993, 1995, 1998-2000, 2002, 2008, 2010, 2011 Peter Miller
.\"
.\" This program is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3 of the License, or
.\" (at your option) any later version.
.\"
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
.\" General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with this program. If not, see <http://www.gnu.org/licenses/>.
.\"
.TH readme tardy
.so etc/version.so
.SH NAME
tardy version \*(v) \- a tar post-processor
.XX "" "The README file"
.br
.if t .ds C) \(co
.if n .ds C) (C)
Copyright \*(C) \*(Y) Peter Miller
.br
.PP
The
.I tardy
program is distributed under the terms of the
GNU General Public License.
See the LICENSE section, below, for more details.
.PP
.B tardy
.I a.,
slow to act,
behind time.
.SH DESCRIPTION
The
.I tardy
program is a
.IR tar (1)
post-processor.
It may be used to manipulate the file headers
in
.IR tar (5)
archive files
in various ways.
.PP
The reason the
.I tardy
program was written was because the author wanted to "spruce up" his
tar files before posting them to the net,
mostly to remove artefacts of the development environment,
without introducing more.
.PP
The
.I tardy
program was designed
to allow you to alter certain characteristics of files AFTER they
have been included in the TAR file.
Among them are:
.PP
.if n *
.if t \(bu
change file owner (by NUMBER or NAME)
.br
.if n *
.if t \(bu
change file group (by NUMBER or NAME)
.br
.if n *
.if t \(bu
add directory prefix
(dot is a really useful prefix)
.br
.if n *
.if t \(bu
change file protections (for example from 600 to 644)
.PP
Note that all of these affect ALL files in the archive.
.br
.ne 1i
.br
.ne 1i
.SH BUILDING
Instructions on how to build and test the
.I tardy
program are to be found in the
.I BUILDING
file included in this distribution.
.br
.ne 1i
.SH LICENSE
The
.I tardy
program is free software;
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation;
either version 3 of the License,
or (at your option) any later version.
.PP
The
.I tardy
program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY;
without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.
.PP
You should have received a copy of the GNU General Public License along
with this program. If not, see <http://www.gnu.org/licenses/>.
.PP
It should be in the
.I LICENSE
file included in this distribution.
.br
.ne 1i
.SH AUTHOR
.TS
tab(;);
l r l.
Peter Miller;EMail:;pmiller@opensource.org.au
\f(CW/\e/\e*\fR;WWW:;http://miller.emu.id.au/pmiller/
.TE
.bp
.SH RELEASE NOTES
.XX "" "Release Notes"
.so etc/new.so
