'\" t
.\" tardy - a tar post-processor
.\" Copyright (C) 1993, 1995, 1998-2000, 2008, 2010-2012 Peter Miller
.\"
.\" This program is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3 of the License, or
.\" (at your option) any later version.
.\"
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
.\" General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with this program. If not, see <http://www.gnu.org/licenses/>.
.\"
.TH build tardy
.so etc/version.so
.SH NAME
How to build Tardy
.XX "" "How to build Tardy"
.br
.if t .ds C) \(co
.if n .ds C) (C)
Copyright \*(C) \*(Y) Peter Miller
.br
.PP
The
.I tardy
program is distributed under the terms of the
GNU General Public License.
See the LICENSE section, below, for more details.
.PP
.B tardy
.I a.,
slow to act,
behind time.
.SH BEFORE YOU START
There are a few pieces of software you may want to
fetch and install before you proceed with your installation of Tardy.
.TP 8n
Boost Library
You will need the C++ Boost Library.
If you are using a package based system, you will need the
libboost\[hy]devel package, or one named something very similar.
.br
http://boost.org/
.TP 8n
BZ2 library
You will need the bzip2 library.
If you are using a package based system, you will need the
libbz2\[hy]devel package, or one named something very similar.
.TP 8n
LZMA library
You will need the lzma library, for .xz compression.
If you are using a package based system, you will need the
liblzma\[hy]devel package, or one named something very similar.
.TP 8n
cpio
The test suite uses the \f[I]cpio\fP(1)
command to create CPIO archives for testing.
.TP 8n
GNU Groff
The documentation for the
.I Tardy
package was prepared using the GNU Groff package (version 1.14 or later).
This distribution includes full documentation,
which may be processed into PostScript or DVI files
at install time \- if GNU Groff has been installed.
.TP 8n
GCC
You may also want to consider fetching and installing the GNU C Compiler
if you have not done so already.  This is not essential.
Tardy was developed using the GNU C++ compiler, and the GNU C++ libraries.
.TP 8n
libexplain
The \f[I]libexplain\fP project provides a library if
system\[hy]call\[hy]specific sterror(3) replacements, for more informative
error messages.
.br
http://libexplain.sourceforge.net/
.TP 8n
zlib
zlib is a library implementing the deflate compression method (and
inflate decompression method) found in \f[I]gzip\fP(1) and PKZIP.
.br
http://zlib.net/
.SH SITE CONFIGURATION
The
.B tardy
program is configured using the
.I configure
shell script included in this distribution.
.PP
The
.I configure
shell script attempts to guess correct values for
various system-dependent variables used during compilation,
and creates the
.I Makefile
and
.I libtardy/config.h
files.
It also creates a shell script
.I config.status
that you can run in the future to recreate the current configuration.
.PP
Normally,
you just
.I cd
to the directory containing
.IR tardy "'s"
source code and type
.RS
.ft CW
.nf
% \f(CB./configure\fP
\fI\&...lots of output...\fP
%
.fi
.ft R
.RE
If you're using
.I csh
on an old version of System V,
you might need to type
.RS
.ft CW
.nf
% \f(CBsh configure\fP
\fI\&...lots of output...\fP
%
.fi
.ft R
.RE
instead to prevent
.I csh
from trying to execute
.I configure
itself.
.PP
Running
.I configure
takes a minute or two.
While it is running,
it prints some messages that tell what it is doing.
If you don't want to see the messages,
run
.I configure
using the --quiet option;
for example,
.RS
.FT CW
.nf
% \f(CB./configure --quiet\fP
%
.fi
.ft R
.RE
.PP
By default,
.I configure
will arrange for the
.I "make install"
command to install the
.B tardy
program's files in
.I /usr/local/bin
and
.IR /usr/local/man .
You can specify an installation prefix other than
.I /usr/local
by giving
.I configure
the option \f(CW--prefix=\fP\fIPATH\fP.
.PP
You can specify separate installation prefixes for
architecture-specific files and architecture-independent files.
If you give
.I configure
the option \f(CW--exec-prefix=\fP\fIPATH\fP
the
.B tardy
package will use
.I PATH
as the prefix for installing programs and libraries.
Data files and documentation will still use the regular prefix.
Normally,
all files are installed using the same prefix.
.PP
.I configure
ignores any other arguments that you give it.
.PP
On systems that require unusual options for compilation or linking
that the
.I tardy
package's
.I configure
script does not know about,
you can give
.I configure
initial values for variables by setting them in the environment.
In Bourne-compatible shells,
you can do that on the command line like this:
.RS
.ft CW
.nf
$ \f(CBCC='gcc -traditional' LIBS=-lposix ./configure\fP
\fI\&...lots of output...\fP
$
.fi
.ft R
.RE
Here are the
.I make
variables that you might want to override with
environment variables when running
.IR configure .
.TP 8n
Variable: CC
C compiler program.
The default is
.IR cc .
.TP 8n
Variable: INSTALL
Program to use to install files.
The default is
.I install
if you have it,
.I cp
otherwise.
.TP 8n
Variable: LIBS
Libraries to link with,
in the form \f(CW-l\fP\fIfoo\fP \f(CW-l\fP\fIbar\fP.
The
.I configure
script will append to this,
rather than replace it.
.PP
If you need to do unusual things to compile the package,
the author encourages you to figure out how
.I configure
could check whether to do them,
and mail diffs or instructions to the author
so that they can be included in the next release.
.br
.ne 1i
.SH BUILDING TARDY
All you should need to do is use the
.RS
.ft CW
.nf
% \f(CBmake\fP
\fI\&...lots of output...\fP
%
.fi
.ft R
.RE
command and wait.
When this finishes you should see a directory called
.I bin
containing one file:
.IR tardy .
The
.I tardy
program is a tar post-processor.
.PP
You can remove the program binaries and object files from the
source directory by using the
.RS
.ft CW
.nf
% \f(CBmake clean\fP
\fI\&...lots of output...\fP
%
.fi
.ft R
.RE
command.
To remove all of the above files, and also remove the
.I Makefile
and
.I libtardy/config.h
and
.I config.status
files, use the
.RS
.ft CW
.nf
% \f(CBmake distclean\fP
\fI\&...lots of output...\fP
%
.fi
.ft R
.RE
command.
.PP
The file
.I etc/configure.ac
is used to create
.I configure
by a GNU program called
.IR autoconf .
You only need to know this if you want to regenerate
.I configure
using a newer version of
.IR autoconf .
.br
.ne 1i
.SH TESTING TARDY
The
.I tardy
program comes with a test suite.
To run this test suite, use the command
.RS
.ft CW
.nf
% \f(CBmake sure\fP
\fI\&...lots of output...\fP
Passed All Tests
%
.fi
.ft R
.RE
.PP
The tests take a about a minute each,
with a few very fast,
and a couple very slow,
but it varies greatly depending on your CPU.
.br
.ne 1i
.SH INSTALLING TARDY
The
.I tardy
program is installed under the
.I /usr/local
tree by default.
Use the \f(CW--prefix=\fP\fIPATH\fP option to
.I configure
if you want some other path.
.PP
All that is required to install the
.I tardy
program is to use the
.RS
.ft CW
.nf
% \f(CBmake install\fP
\fI\&...lots of output...\fP
%
.fi
.ft R
.RE
command.
Control of the directories used may be found in the first
few lines of the
.I Makefile
file if you want to bypass the
.I configure
script.
.PP
The above procedure assumes that the
.IR soelim (1)
command is somewhere in the command search
.IR PATH .
The
.IR soelim (1)
command is available as part of the
.I "GNU Roff"
package.
.PP
The above procedure also assumes that the
.I $(prefix)/man/man1
and
.I $(prefix)/man/man5
directories already exist.
If they do not,
you will need to
.I mkdir
them manually.
.br
.ne 1i
.SH PRINTED MANUALS
This distribution contains the sources to
all of the documentation for
.IR tardy .
The author used the GNU groff package
and a postscript printer to prepare the documentation.
If you do not have this software,
you will need to substitute commands appropriate to your site.
.PP
To print copies of the
.IR README ,
and
.I BUILDING
files,
the following commands may be used
.RS
.ft CW
.nf
% \f(CBgroff -t -man etc/*.man | lpr\fP
%
.fi
.ft R
.RE
This will produce about 4 pages.
The "-t" flag means preprocess with
.IR tbl (1).
.PP
To print copies of the manual entry,
the following commands may be used
.RS
.ft CW
.nf
% \f(CBcd man1\fP
% \f(CBgroff -s -t -man *.1 | lpr\fP
% \f(CBcd ..\fP
%
.fi
.ft R
.RE
This will produce about 3 pages.
The "-s" flag means preprocess with
.IR soelim (1),
and the "-t" flag means preprocess with
.IR tbl (1).
.br
.ne 1i
.SH GETTING HELP
If you need assistance with the
.I tardy
program,
please do not hesitate to contact the author at
.RS
Peter Miller <pmiller@opensource.org.au>
.RE
Any and all feedback is welcome.
.PP
When reporting problems,
please include the version number
given by the
.RS
.ft CW
.nf
% tardy -version
tardy version \fIa.b.cccc\fP
\&.\&.\&.
%
.fi
.ft R
.RE
command.
.br
.ne 1i
.SH LICENSE
The
.I tardy
program is free software;
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation;
either version 3 of the License,
or (at your option) any later version.
.PP
The
.I tardy
program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.
.PP
It should be in the
.I LICENSE
file included in this distribution.
.br
.ne 1i
.SH AUTHOR
.TS
tab(;);
l r l.
Peter Miller;EMail:;pmiller@opensource.org.au
\f(CW/\e/\e*\fR;WWW:;http://miller.emu.id.au/pmiller/
.TE
.\" vim: set ts=8 sw=4 et :
