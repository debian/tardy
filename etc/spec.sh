#!/bin/sh
#
#       tardy - a tar post-processor
#       Copyright (C) 1998, 2000, 2001, 2004, 2008 Peter Miller
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program. If not, see
#       <http://www.gnu.org/licenses/>.
#
version=${version-0.0.0}
echo 'Summary: a tar post-processor'
echo 'Name: tardy'
echo "Version: ${version}"
echo 'Release: 1'
echo 'Copyright: GPL'
echo 'Group: System/Tape'
echo "Source: http://tardy.sourceforge.net/tardy-${version}.tar.gz"
echo 'URL: http://tardy.sourceforge.net/'
echo 'BuildRoot: /tmp/tardy-build-root'

prefix=/usr
#
# RPM only has install-time relocatable packages.  It has no support for
# build-time relocatable packages.  Therefore, we must NOT specify a Spec
# prefix, or the installed locations will not match the built locations.
#
#echo "Prefix: $prefix"
echo ''

cat << 'fubar'
%description
The tardy program is a tar(1) post-processor.  It may be used to
manipulate the file headers tar(5) archive files in various ways.

The reason the tardy program was written was because the author wanted
to "spruce up" tar files before posting them to the net, mostly to
remove artefacts of the development environment, without introducing more.

The tardy program was designed to allow you to alter certain atrributes
of files after they have been included in the tar file.  Among them are:

  * change file owner (by number or name)

  * change file group (by number or name)

  * add directory prefix (for example, dot)

  * change file protections (for example, from 600 to 644)

Note that all of these affect ALL files in the archive.

%prep
fubar

#
# set the prefix here
#
echo '%setup'
echo "./configure --prefix=$prefix"
echo ''
echo '%build'
echo 'make'
echo ''
echo '%install'
echo 'make RPM_BUILD_ROOT=$RPM_BUILD_ROOT install'

#
# remember things for the %files section
#
files=
psdocs=
dvidocs=

remember_prog()
{
        if eval "test \"\${prog_${1}-no}\" != yes"
        then
                eval "prog_${1}=yes"
                files="$files $prefix/bin/${1}"
        fi
}

for file in $*
do
        case $file in

        common/*)
                ;;
        etc/*)
                ;;

        */main.cc)
                dir=`echo $file | sed 's|/.*||'`
                remember_prog $dir
                ;;

        test/*/*)
                ;;

        lib/*/LC_MESSAGES/common.po)
                ;;

        lib/*.po)
                stem=`echo $file | sed 's|^lib/\(.*\)\.po$|\1|'`
                dst="$prefix/lib/tardy/$stem.mo"
                files="$files $dst"
                ;;

        man[0-9]/*.[0-9])
                files="$files $prefix/man/${file}*"
                ;;

        *)
                ;;
        esac
done

echo ''
echo '%files'
for file in $files
do
        echo "$file"
done | sort
