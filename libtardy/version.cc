//
// tardy - a tar post-processor
// Copyright (C) 1991-1999, 2001, 2002, 2004, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/stdio.h>
#include <libtardy/ac/stdlib.h>
#include <libexplain/output.h>

#include <libtardy/arglex.h>
#include <libtardy/rcstring.h>
#include <libtardy/roff.h>
#include <libtardy/trace.h>


static void
version_copyright(void)
{
    static const char *text[] =
    {
        ".so cr",
    };
    roff().interpret(text, SIZEOF(text));
}


void
version_license(void)
{
    static const char *text[] =
    {
#include <man/man1/tardy_license.h>
    };

    roff().interpret(text, SIZEOF(text));
}


struct vsn_table_t
{
    const char *name;
    void (*func)(void);
};


static vsn_table_t vsn_table[] =
{
    { "Copyright", version_copyright },
    { "License", version_license },
};


void
arglex::version_main(void)
{
    void (*func)(void) = 0;
    trace(("version_main()\n{\n"));
    const char *name = 0;
    while (token != token_eoln)
    {
        switch (token)
        {
        default:
            generic_argument();
            continue;

        case token_string:
            if (name)
            {
                explain_output_error_and_die
                (
                    "too many version information names given"
                );
            }
            name = value.string;
            break;
        }
        lex();
    }

    if (name)
    {
        int nhit = 0;
        const vsn_table_t *hit[SIZEOF(vsn_table)];
        for (const vsn_table_t *tp = vsn_table; tp < ENDOF(vsn_table); ++tp)
        {
            if (compare(tp->name, name))
                hit[nhit++] = tp;
        }
        switch (nhit)
        {
        case 0:
            explain_output_error_and_die
            (
                "version information name %s unknown",
                rcstring(name).quote_c().c_str()
            );

        case 1:
            break;

        default:
            rcstring s(hit[0]->name);
            for (int j = 1; j < nhit; ++j)
            {
                s = s + ", " + hit[j]->name;
            }
            explain_output_error_and_die
            (
                "version information name %s ambiguous (%s)",
                rcstring(name).quote_c().c_str(),
                s.c_str()
            );
        }
        lex();
        func = hit[0]->func;
    }
    else
        func = version_copyright;

    func();
    trace(("}\n"));
}


void
arglex::version_help(void)
{
    static const char *text[] =
    {
".TH \"\\*(n) -VERSion\" 1 \\*(N)",
".SH NAME",
"\\*(n) -VERSion \\- give version information",
".SH SYNOPSIS",
".B \\*(n)",
".B -VERSion",
"[",
".IR info-name",
"]",
".br",
".B \\*(n)",
".B -VERSion",
".B -Help",
".SH DESCRIPTION",
"The",
".I \\*(n)",
".I -VERSion",
"command is used to",
"give version information",
"and conditions of use.",
".PP",
"There are a number of possible",
".IR info-name s,",
"as follow (abbreviations as for command line options):",
".TP 8n",
"Copyright",
"The copyright notice for the \\*(n) program.",
"Version information will also be printed.",
"This is the default.",
".TP 8n",
"Redistribution",
"Print the conditions of use and redistribution.",
".TP 8n",
"Warranty",
"Print the limited warranty.",
".SH OPTIONS",
"The following options are understood:",
".so o_help.so",
".so o__rules.so",
".SH RECOMMENDED ALIAS",
"The recommended alias for this command is",
".nf",
".ta 8n 16n",
"csh%\talias aev '\\*(n) -vers \\e!*'",
"sh$\taev(){\\*(n) -vers $*}",
".fi",
".SH ERRORS",
"It is an error if",
"the ",
".I info-name",
"given is unknown.",
".so z_exit.so",
".so z_cr.so",
    };

    help_f(text, SIZEOF(text));
}


void
arglex::version(void)
{
    trace(("version()\n{\n"));
    switch (lex())
    {
    default:
        version_main();
        break;

    case token_help:
        version_help();
        break;
    }
    trace(("}\n"));
}
