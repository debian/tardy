//
// tardy - a tar post-processor
// Copyright (C) 2002, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_FILENAMELIST_H
#define LIBTARDY_FILENAMELIST_H

#include <libtardy/config.h>
#include <boost/shared_ptr.hpp>

#include <libtardy/rcstring.h>

/**
  * The filenamelist class is used to represent a list of file names,
  * usually but not always read from a file.
  */
class filenamelist
{
public:
    typedef boost::shared_ptr<filenamelist> pointer;

    /**
      * The destructor.
      */
    virtual ~filenamelist();

    /**
      * The read_one_line method is used to read a file name from
      * the list of file names.
      */
    virtual bool read_one_line(rcstring &result) = 0;

    /**
      * The `filename' method is used to get the name of the input
      * filename containing the list fo file names.
      */
    virtual rcstring filename(void) const = 0;

protected:
    /**
      * The default constructor.
      * For use by derived classes only.
      */
    filenamelist();

private:
    /**
      * The copy constructor.  Do not use.
      */
    filenamelist(const filenamelist &);

    /**
      * The assignment operator.  Do not use.
      */
    filenamelist &operator=(const filenamelist &);
};

#endif // LIBTARDY_FILENAMELIST_H
