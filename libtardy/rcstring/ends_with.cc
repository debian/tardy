//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/string.h>

#include <libtardy/rcstring.h>


bool
rcstring::ends_with(const rcstring &suffix)
    const
{
    if (!p)
        return (!suffix.p);
    if (!suffix.p)
        return true;

    size_t haystack_len = p->str_length;
    size_t needle_len = suffix.p->str_length;
    if (haystack_len < needle_len)
        return false;
    const char *haystack = p->str_text;
    const char *needle = suffix.p->str_text;
    return !memcmp(haystack + haystack_len - needle_len, needle, needle_len);
}
