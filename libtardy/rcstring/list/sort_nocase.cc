//
// tardy - a tar post-processor
// Copyright (C) 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/string.h>
#include <algorithm>

#include <libtardy/rcstring/list.h>


static bool
cmp(const rcstring &a, const rcstring &b)
{
    // C locale
    return (strcasecmp(a.c_str(), b.c_str()) < 0);
}


void
rcstring_list::sort_nocase(void)
{
    std::sort(contents.begin(), contents.end(), cmp);
}


// vim: set ts=8 sw=4 et :
