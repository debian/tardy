//
// tardy - a tar post-processor
// Copyright (C) 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/rcstring/list.h>


void
rcstring_list::remove(const rcstring &w)
{
    contents_t tmp;
    contents_t::iterator it = contents.begin();
    while (it != contents.end())
    {
        if (*it == w)
        {
            ++it;
            break;
        }
        tmp.push_back(*it);
        ++it;
    }
    while (it != contents.end())
    {
        tmp.push_back(*it);
        ++it;
    }
    contents = tmp;
}


void
rcstring_list::remove(size_t j)
{
    if (j >= size())
        return;
    contents_t tmp;
    for (size_t pos = 0; pos < contents.size(); ++pos)
    {
        if (j != pos)
            tmp.push_back(contents[pos]);
    }
    contents = tmp;
}


// vim: set ts=8 sw=4 et :
