//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/ctype.h>

#include <libtardy/rcstring/accumulator.h>


rcstring
rcstring::hexdump(const void *data, size_t data_size)
{
    rcstring_accumulator ac;
    const unsigned char *p = (const unsigned char *)data;
    for (size_t j = 0; j < data_size; j += 16)
    {
        ac.printf("%08lX:", (unsigned long)j);
        for (size_t k = 0; k < 16; ++k)
        {
            if (j + k < data_size)
            {
                unsigned char c = p[j + k];
                ac.printf(" %02X", c);
            }
            else
                ac.push_back("   ");
        }
        ac.push_back("  ");
        for (size_t k = 0; k < 16 && j + k < data_size; ++k)
        {
            unsigned char c = p[j + k];
            c &= 0x7F;
            if (!isprint(c))
                c = '.';
            ac.push_back(c);
        }
        ac.push_back('\n');
    }
    return ac.mkstr();
}
