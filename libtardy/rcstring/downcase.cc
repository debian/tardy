//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/ctype.h>

#include <libtardy/rcstring.h>


rcstring
rcstring::downcase(void)
    const
{
    if (!p)
        return rcstring();

    static char *tmp = 0;
    static size_t tmplen = 0;
    size_t tmp_size = p->str_length;
    if (tmp_size < 16)
        tmp_size = 16;
    if (tmp_size > tmplen)
    {
        delete [] tmp;
        tmp = new char [tmp_size];
    }

    const char *cp1 = p->str_text;
    char *cp2 = tmp;
    for (;;)
    {
        unsigned char c = *cp1++;
        if (!c)
            break;
        if (isupper(c))
            c = tolower(c);
        *cp2++ = c;
    }
    return rcstring(tmp, p->str_length);
}
