//
//      tardy - a tar post-processor
//      Copyright (C) 1993, 1998, 1999, 2004, 2008, 2009, 2011 Peter Miller
//
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program. If not, see
//      <http://www.gnu.org/licenses/>.
//

#ifndef MAIN_H
#define MAIN_H

//
// array manipulation
//
#define SIZEOF(a) (sizeof(a) / sizeof(a[0]))
#define ENDOF(a) ((a) + SIZEOF(a))

//
// Printf formatting attributes.
//
#ifdef __GNUC__
#define ATTR_PRINTF(x, y) __attribute__((__format__(printf, x, y)))
#define DEPRECATED __attribute__((__deprecated__))
#else
#define ATTR_PRINTF(x, y)
#define DEPRECATED
#endif

//
// Take the define out of comments to
// enable the debugging the functionality.
//
// #define DEBUG
//

#endif // MAIN_H
