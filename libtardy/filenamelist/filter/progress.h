//
// tardy - a tar post-processor
// Copyright (C) 2002, 2008, 2009, 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_FILENAMELIST_FILTER_PROGRESS_H
#define LIBTARDY_FILENAMELIST_FILTER_PROGRESS_H

#include <libtardy/ac/time.h>
#include <deque>
#include <libtardy/filenamelist/filter.h>

/**
  * The filenamelist_filter_progress class is used to filter a name
  * list source, not to modify the name list in any way, but to report
  * progress against itfilter a name list source, not to modify the name
  * list in any way, but to report progress against it.
  */
class filenamelist_filter_progress:
    public filenamelist_filter
{
public:
    /**
      * The destructor.
      */
    virtual ~filenamelist_filter_progress();

    /**
      * The create class method iss used to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *     The file name list to be filtered.
      */
    static pointer create(const filenamelist::pointer &deeper);

protected:
    // see base class for documentation
    bool read_one_line(rcstring &result);

private:
    /**
      * The constructor.
      * It is private on purpose, use the #create class method instead.
      *
      * @param deeper
      *     The file name list to be filtered.
      */
    filenamelist_filter_progress(const filenamelist::pointer &deeper);

    enum state_t
    {
        state_begin,
        state_middle,
        state_end
    };

    /**
      * The state instance variable is used to remember what state
      * we are up to.
      */
    state_t state;

    struct item_t
    {
        ~item_t();
        item_t(const rcstring &name, off_t size);
        item_t(const item_t &rhs);
        item_t &operator=(const item_t &rhs);

        rcstring name;
        off_t size;
    };

    typedef std::deque<item_t> items_t;

    /**
      * The items instance variable is used to keep track of the
      * name and size of each file.
      */
    items_t items;

    /**
      * The position instance variable is used to remember where
      * we are up to working through the list.
      */
    size_t position;

    /**
      * The start_time instance variable is used to remember
      * when we started to transfer data (well, file names, anyway)
      * to the classes upstream of us.  This is used to predict the ETA.
      */
    time_t start_time;

    /**
      * The next_time instance variable is used to remember when
      * we should next emit another progress report.
      */
    time_t next_time;

    /**
      * The size_total instance variable is used to remember the
      * total size of all the files.  This is used to predict the ETA.
      */
    off_t size_total;

    /**
      * The size_position instance variable is used to remember
      * how much data has been transferred so far.  This is used to
      * predict the ETA.
      */
    off_t size_position;

    /**
      * The spinner_state instance variable is used to remember
      * what position the spinner is in.
      */
    int spinner_state;

    /**
      * The show instance variable is used to remember whether
      * or not we are showing progress.  It is set to true if the
      * standard error stream is a tty, false otherwise.
      */
    bool show;

    /**
      * The spinner method is used to draw the next spinner indicator
      * character.
      */
    char spinner(void);

    /**
      * The show_start_status method is used to show how many fiels
      * have been read in to date.
      */
    void show_start_status(void);

    /**
      * The spinner method is used to show how far along with readin
      * gthe file list we are.  This indirectly indicates how long
      * it will be before we finish (ETA).
      */
    void show_middle_status(void);

    /**
      * The show_end_status method is used to shwo the number of
      * files, number of bytes, effective data transfer rate, and
      * elapsed time.
      */
    void show_end_status(void);

    /**
      * The default constructor.  Do not use.
      */
    filenamelist_filter_progress();

    /**
      * The copy constructor.  Do not use.
      */
    filenamelist_filter_progress(const filenamelist_filter_progress &);

    /**
      * The assignment operator.  Do not use.
      */
    filenamelist_filter_progress &operator=(
        const filenamelist_filter_progress &);
};

// vim: set ts=8 sw=4 et :
#endif // LIBTARDY_FILENAMELIST_FILTER_PROGRESS_H
