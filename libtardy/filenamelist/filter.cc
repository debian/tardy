//
// tardy - a tar post-processor
// Copyright (C) 2002, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/filenamelist/filter.h>


filenamelist_filter::~filenamelist_filter()
{
}


filenamelist_filter::filenamelist_filter(
    const filenamelist::pointer &a_deeper
) :
    deeper(a_deeper)
{
}


bool
filenamelist_filter::read_one_deeper(rcstring &result)
{
    return deeper->read_one_line(result);
}


rcstring
filenamelist_filter::filename(void)
    const
{
    return deeper->filename();
}
