//
// tardy - a tar post-processor
// Copyright (C) 2002, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_FILENAMELIST_FILTER_H
#define LIBTARDY_FILENAMELIST_FILTER_H

#include <libtardy/filenamelist.h>

/**
  * The filenamelist_filter class is used to represent an abstract
  * filtering operation on a list of file names.
  */
class filenamelist_filter:
    public filenamelist
{
public:
    /**
      * The destructor.
      */
    virtual ~filenamelist_filter();

    // see base class for documentation
    rcstring filename(void) const;

protected:
    /**
      * The constructor.
      * For use by derived classes only.
      *
      * @param deeper
      *     The list of file names tor be filtered.
      */
    filenamelist_filter(const filenamelist::pointer &deeper);

    /**
      * The read_one_deeper method is used to read one line from
      * the deeper file name list.
      */
    bool read_one_deeper(rcstring &result);

private:
    /**
      * The deeper instance variable is used to remember the source
      * fo the filenamelist being filtered.
      */
    filenamelist::pointer deeper;

    /**
      * The default constructor.  Do not use.
      */
    filenamelist_filter();

    /**
      * The copy constructor.  Do not use.
      */
    filenamelist_filter(const filenamelist_filter &);

    /**
      * The assignment operator.  Do not use.
      */
    filenamelist_filter &operator=(const filenamelist_filter &);
};

#endif // LIBTARDY_FILENAMELIST_FILTER_H
