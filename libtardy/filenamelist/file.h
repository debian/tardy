//
// tardy - a tar post-processor
// Copyright (C) 2002, 2008, 2009, 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_FILENAMELIST_FILE_H
#define LIBTARDY_FILENAMELIST_FILE_H

#include <libtardy/filenamelist.h>
#include <libtardy/file/input.h>

/**
  * The filenamelist_file class is used to represent a file name list
  * sourced from a file, one file name per line.
  */
class filenamelist_file:
    public filenamelist
{
public:
    /**
      * The destructor.
      */
    virtual ~filenamelist_file();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param ifp
      *     The file to be read for file names, one per line.
      */
    static pointer create(const file_input::pointer &ifp);

protected:
    // see base class for documentation
    bool read_one_line(rcstring &result);

    // see base class for documentation
    rcstring filename(void) const;

private:
    /**
      * The constructor.
      * It is private on purpose, use the #create class method instead.
      *
      * @param ifp
      *     The file to be read for file names, one per line.
      */
    filenamelist_file(const file_input::pointer &ifp);

private:
    /**
      * The source instance variable is used to remember where to
      * get the input from.
      */
    file_input::pointer source;

    /**
      * The position instance variable is used to remember the
      * current position within the #buffer.
      */
    size_t position;

    /**
      * The length instance variable is used to remember the number
      * of valid characters within the #buffer.
      */
    size_t length;

    /**
      * The buffer instance variable is used to remember data read
      * from the source.  The source is buffered for better efficiency.
      */
    char buffer[512];

    /**
      * The read_one_char method is used to read one character
      * from the input source, buffered using the the #buffer,
      * #position and #length instance variables.
      */
    inline int read_one_char(void);

    /**
      * The default constructor.  Do not use.
      */
    filenamelist_file();

    /**
      * The copy constructor.  Do not use.
      */
    filenamelist_file(const filenamelist_file &);

    /**
      * The assignment operator.  Do not use.
      */
    filenamelist_file &operator=(const filenamelist_file &);
};

// vim: set ts=8 sw=4 et :
#endif // LIBTARDY_FILENAMELIST_FILE_H
