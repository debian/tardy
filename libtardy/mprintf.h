//
// tardy - a tar post-processor
// Copyright (C) 1994-1998, 1999, 2004, 2008, 2009, 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_MPRINTF_H
#define LIBTARDY_MPRINTF_H

#include <libtardy/ac/stdarg.h>
#include <libtardy/main.h>
#include <libtardy/rcstring.h>

/**
  * The mprintf function is used to format a string into memory.
  *
  * @param fmt
  *     The mesaage format to use.  See printf(3) for documentation of
  *     the format and its arguments.
  * @returns
  *     Pointer to NUL terminated string in synamic memory.
  *     <b>Do not</b> use mem_free when you are done with it.
  */
char *mprintf(const char *fmt, ...) ATTR_PRINTF(1, 2);

/**
  * The mprintf_errok function is used to format a string into memory.
  * Safe to call from error reporting functions.
  *
  * @param fmt
  *     The mesaage format to use.  See printf(3) for documentation of
  *     the format and its arguments.
  * @returns
  *     Pointer to NUL terminated string in synamic memory.
  *     <b>Do not</b> use mem_free when you are done with it.
  */
char *mprintf_errok(const char *fmt, ...) ATTR_PRINTF(1, 2);

/**
  * The mprintf function is used to format a string into memory.
  *
  * @param fmt
  *     The mesaage format to use.  See vprintf(3) for documentation of
  *     the format and its arguments.
  * @param arglist
  *     The arguments to be used for the format.
  * @returns
  *     Pointer to NUL terminated string in synamic memory.
  *     <b>Do not</b> use mem_free when you are done with it.
  */
char *vmprintf(const char *fmt, va_list arglist);


/**
  * The mprintf function is used to format a string into memory.
  * Safe to call from error reporting functions.
  *
  * @param fmt
  *     The mesaage format to use.  See vprintf(3) for documentation of
  *     the format and its arguments.
  * @param arglist
  *     The arguments to be used for the format.
  * @returns
  *     Pointer to NUL terminated string in synamic memory.
  *     <b>Do not</b> use mem_free when you are done with it.
  */
char *vmprintf_errok(const char *fmt, va_list arglist);

/**
  * The vmprintf_str function is just like the vmprintf function, except
  * that it constructs a string_ty value from it.
  */
rcstring vmprintf_str(const char *fmt, va_list);

// vim: set ts=8 sw=4 et :
#endif // LIBTARDY_MPRINTF_H
