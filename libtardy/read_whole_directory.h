//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_READ_WHOLE_DIRECTORY_H
#define LIBTARDY_READ_WHOLE_DIRECTORY_H

#include <libtardy/rcstring/list.h>

/**
  * The read_whole_directory function is used to read all of the file
  * names in a given directory, all in one go.
  *
  * @param path
  *     The path opf the directory to read.
  * @param result
  *     where to place the names of the files in the directory (omitting
  *     "." and "..")
  */
void read_whole_directory(const rcstring &path, rcstring_list &result);

#endif // LIBTARDY_READ_WHOLE_DIRECTORY_H
