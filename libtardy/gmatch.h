//
// tardy - a tar post-processor
// Copyright (C) 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_GMATCH_H
#define LIBTARDY_GMATCH_H

#include <libtardy/rcstring.h>

/**
  * The gmatch function is used to match a file name agains a filename
  * "glob" pattern.
  *
  * @param formal
  *     The file name pattern, the usual shell patterns.
  * @param actual
  *     The actual strng to be matched against the pattern.
  * @returns
  *     bool; true of matches, false if does not.
  *     Reports a fatal error and exits of there is a bug in the pattern.
  */
bool gmatch(const rcstring &formal, const rcstring &actual);

/**
  * The gmatch function is used to match a file name agains a filename
  * "glob" pattern.
  *
  * @param formal
  *     The file name pattern, the usual shell patterns.
  * @param actual
  *     The actual strng to be matched against the pattern.
  * @returns
  *     bool; true of matches, false if does not.
  *     Reports a fatal error and exits of there is a bug in the pattern.
  */
bool gmatch(const char *formal, const char *actual);

/**
  * The gmatch function is used to match a file name agains a filename
  * "glob" pattern.  This function is for matching sub-patterns.
  *
  * @param formal
  *     The file name pattern, the usual shell patterns.
  * @param formal_end
  *     The end of the file name pattern.
  * @param actual
  *     The actual strng to be matched against the pattern.
  * @returns
  *     bool; true of matches, false if does not.
  *     Reports a fatal error and exits of there is a bug in the pattern.
  */
bool gmatch2(const char *formal, const char *formal_end, const char *actual);

#endif // LIBTARDY_GMATCH_H
