//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_FORMAT_FAMILY_H
#define LIBTARDY_FORMAT_FAMILY_H

#include <libtardy/rcstring.h>

/**
  * A format family is a broad indication as to which archive format is
  * in use.
  */
enum format_family_t
{
    format_family_tar,
    format_family_cpio,
    format_family_ar,
    format_family_other,
};

/**
  * The format_family_from_filename function is used to guess a file's
  * format family from the file's extension.
  *
  * @param filename
  *     The file name to sniff at.
  * @returns
  *     The corresponding fformat familiy, or fornat_family_other if it
  *     can't be determined.
  */
format_family_t format_family_from_filename(const rcstring &filename);

#endif // LIBTARDY_FORMAT_FAMILY_H
