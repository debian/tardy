//
// tardy - a tar post-processor
// Copyright (C) 1994, 1995, 1999, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_AC_TERMIOS_H
#define LIBTARDY_AC_TERMIOS_H

#include <libtardy/config.h>

#ifdef HAVE_winsize_SYS_IOCTL_H
#include <sys/ioctl.h>
#else
#ifdef HAVE_winsize_TERMIOS_H
#include <termios.h>
#endif // HAVE_winsize_TERMIOS_H
#endif // !HAVE_winsize_SYS_IOCTL_H

#endif // LIBTARDY_AC_TERMIOS_H
