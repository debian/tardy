//
//      tardy - a tar post-processor
//      Copyright (C) 2002, 2008, 2009 Peter Miller
//
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program. If not, see
//      <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/stdio.h>
#include <libtardy/ac/stdarg.h>


#ifndef HAVE_VSNPRINTF

int
vsnprintf(char *buffer, size_t, const char *fmt, va_list ap)
{
    return vsprintf(buffer, fmt, ap);
}

#endif

#ifndef HAVE_SNPRINTF

int
snprintf(char *buffer, size_t bufsize, const char *fmt, ...)
{
    va_liat ap;
    va_start(ap, fmt);
    int n = vsnprintf(buffer, bufsize, fmt, ap);
    va_end(ap);
    return n;
}

#endif
