//
// tardy - a tar post-processor
// Copyright (C) 2003, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_AC_ZLIB_H
#define LIBTARDY_AC_ZLIB_H

#include <libtardy/config.h>

#ifdef HAVE_ZLIB_H
#include <zlib.h>
#else
#error "You must have zlib installed in order to build Tardy."
#endif

// there is ERR_MSG in zutil.h, but we aren't allowed to see it.
const char *z_error(int);

#ifndef DEF_MEM_LEVEL
#define DEF_MEM_LEVEL 8 // in zutil.h, but we aren't allowed to see it.
#endif


#ifndef Z_BUFSIZE
#  ifdef MAXSEG_64K
#    define Z_BUFSIZE 4096 // minimize memory usage for 16-bit DOS
#  else
#    define Z_BUFSIZE 16384
#  endif
#endif

// gzip flag byte
#define Z_FLAG_ASCII        0x01 // bit 0 set: file probably ascii text
#define Z_FLAG_HEAD_CRC     0x02 // bit 1 set: header CRC present
#define Z_FLAG_EXTRA_FIELD  0x04 // bit 2 set: extra field present
#define Z_FLAG_ORIG_NAME    0x08 // bit 3 set: original file name present
#define Z_FLAG_COMMENT      0x10 // bit 4 set: file comment present
#define Z_FLAG_RESERVED     0xE0 // bits 5..7: reserved

#endif // LIBTARDY_AC_ZLIB_H
