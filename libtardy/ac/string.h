//
// tardy - a tar post-processor
// Copyright (C) 1994, 1995, 1999, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_AC_STRING_H
#define LIBTARDY_AC_STRING_H

#include <libtardy/config.h>

#if STDC_HEADERS || HAVE_STRING_H
#  include <string.h>
   // An ANSI string.h and pre-ANSI memory.h might conflict.
#  if !STDC_HEADERS && HAVE_MEMORY_H
#    include <memory.h>
#  endif
#else
   // memory.h and strings.h conflict on some systems.
#  include <strings.h>
#endif

#if !HAVE_DECL_STRCASECMP
int strcasecmp(const char *, const char *);
#endif

#if !HAVE_DECL_STRVERSCMP
int strverscmp(const char *lhs, const char *rhs);
#endif

#if !HAVE_DECL_MEMMEM
void *memmem(const void *haystack, size_t haystack_size,
    const void *needle, size_t needle_size);
#endif

#endif // LIBTARDY_AC_STRING_H
