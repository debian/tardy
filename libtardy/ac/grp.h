//
// tardy - a tar post-processor
// Copyright (C) 2003, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_AC_GRP_H
#define LIBTARDY_AC_GRP_H

#include <libtardy/ac/stdio.h>  // need for grp.h on OSF/1
#include <sys/types.h> // need for grp.h on FreeBSD
#include <grp.h>

#endif // LIBTARDY_AC_GRP_H
