//
// tardy - a tar post-processor
// Copyright (C) 1998, 2002, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_AC_STDIO_H
#define LIBTARDY_AC_STDIO_H

//
// Need to define _POSIX_SOURCE on Linux, in order to get the fdopen,
// fileno, popen and pclose function prototypes.
//
// Need to define _GNU_SOURCE on Linux, in order to get snprintf.
//
#ifdef __linux__
#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE 1
#endif
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#endif

#include <stdio.h>

//
// Ancient pre-ANSI-C systems (e.g. SunOS 4.1.2) fail to define this.
//
#ifndef SEEK_SET
#define SEEK_SET 0
#endif

#endif // LIBTARDY_AC_STDIO_H
