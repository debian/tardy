//
// tardy - a tar post-processor
// Copyright (C) 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/stdio.h>
#include <libtardy/ac/lzma.h>


const char *
lzma_strerror(int x)
{
    switch (x)
    {
    case LZMA_OK:
        return "success (LZMA_OK)";

    case LZMA_STREAM_END:
        return "End of stream was reached (LZMA_STREAM_END)";

    case LZMA_NO_CHECK:
        return "Input stream has no integrity check (LZMA_NO_CHECK)";

    case LZMA_UNSUPPORTED_CHECK:
        return "Cannot calculate the integrity check (LZMA_UNSUPPORTED_CHECK)";

    case LZMA_GET_CHECK:
        return "Integrity check type is now available (LZMA_GET_CHECK)";

    case LZMA_MEM_ERROR:
        return "Cannot allocate memory (LZMA_MEM_ERROR)";

    case LZMA_MEMLIMIT_ERROR:
        return "Memory usage limit was reached (LZMA_MEMLIMIT_ERROR)";

    case LZMA_FORMAT_ERROR:
        return "File format not recognized (LZMA_FORMAT_ERROR)";

    case LZMA_OPTIONS_ERROR:
        return "Invalid or unsupported options (LZMA_OPTIONS_ERROR)";

    case LZMA_DATA_ERROR:
        return "Data is corrupt (LZMA_DATA_ERROR)";

    case LZMA_BUF_ERROR:
        return "No progress is possible (LZMA_BUF_ERROR)";

    case LZMA_PROG_ERROR:
        return "Programming error (LZMA_PROG_ERROR)";
    }
    static char buf[30];
    snprintf(buf, sizeof(buf), "Unknown lzma error (%d)", x);
    return buf;
}


// vim: set ts=8 sw=4 et :
