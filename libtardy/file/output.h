//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2003, 2004, 2008, 2009, 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_FILE_OUTPUT_H
#define LIBTARDY_FILE_OUTPUT_H

#include <libtardy/config.h>
#include <boost/shared_ptr.hpp>

#include <libtardy/rcstring.h>

/**
  * The file_output class is used to represent an abstract form of binary
  * file output.  Shall be derived from before it can be used.
  */
class file_output
{
public:
    typedef boost::shared_ptr<file_output> pointer;

    /**
      * The destructor.
      */
    virtual ~file_output();

    /**
      * The factory method is used to create a new file_output instance,
      * based on the file name.
      *
      * @param filename
      *     The name of the file to open for writing.  If the NULL
      *     pointer is upplied, or the empty string is upplied, or the
      *     special name "-" is upplied, it will write to the standard
      *     output; otherwise, it will write to the named file.
      */
    static pointer factory(const char *filename);

    /**
      * The fatal method is used to report fatal errors occurring when
      * operating on this output stream.  The program name and the file
      * name are prepended.  It exits with an exit status of one.  It
      * does not return.
      *
      * @param fmt
      *     The message format to print.  See printf(3) for a
      *     description of the format and its arguments.
      */
    void fatal(const char *fmt, ...) const ATTR_PRINTF(2, 3);

    /**
      * The filename method is used to obtain the name of the file
      * being written.
      */
    virtual rcstring filename(void) const = 0;

    /**
      * The write method is used to write data to the output stream.
      *
      * @param data
      *     This argument points to a data structure to be written.
      *     Usually an array of bytes.
      * @param data_size
      *     This argument indicates the number fo bytes to be written.
      */
    virtual void write(const void *data, size_t data_size) = 0;

protected:
    /**
      * The default constructor.
      * May only be used by derived classes.
      */
    file_output();

protected:
    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    file_output(const file_output &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the assignment.
      */
    file_output &operator=(const file_output &rhs);
};

// vim: set ts=8 sw=4 et :
#endif // LIBTARDY_FILE_OUTPUT_H
