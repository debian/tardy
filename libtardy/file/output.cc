//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2001-2003, 2008, 2009, 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/assert.h>
#include <libtardy/ac/errno.h>
#include <libtardy/ac/stdarg.h>
#include <libtardy/ac/stdio.h>
#include <libexplain/output.h>

#include <libtardy/file/output.h>


file_output::~file_output()
{
    // nothing to do
}


file_output::file_output()
{
    // nothing to do
}


file_output::file_output(const file_output &)
{
    // nothing to do
}


file_output &
file_output::operator=(const file_output &)
{
    // nothing to do
    return *this;
}


void
file_output::fatal(const char *fmt, ...)
    const
{
    va_list ap;
    va_start(ap, fmt);
    char buffer[2000];
    vsnprintf(buffer, sizeof(buffer), fmt, ap);
    va_end(ap);
    explain_output_error_and_die("%s: %s", filename().c_str(), buffer);
}


// vim: set ts=8 sw=4 et :
