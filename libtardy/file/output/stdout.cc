//
//      tardy - a tar post-processor
//      Copyright (C) 1998, 1999, 2003, 2008, 2009, 2011, 2012 Peter Miller
//
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program. If not, see
//      <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/unistd.h>
#include <libexplain/write.h>

#include <libtardy/file/output/stdout.h>


file_output_stdout::~file_output_stdout()
{
    // nothing to do
}


file_output_stdout::file_output_stdout()
{
    // nothing to do
}


file_output::pointer
file_output_stdout::create(void)
{
    return pointer(new file_output_stdout());
}


void
file_output_stdout::write(const void *data, size_t data_size)
{
    int fd = 1;
    ssize_t count = explain_write_or_die(fd, data, data_size);
    assert(count >= 0);
    if ((size_t)count != data_size)
    {
        fatal
        (
            "short write (gave %lu, got %lu)",
            (unsigned long)data_size,
            (unsigned long)count
        );
    }
}


rcstring
file_output_stdout::filename(void)
    const
{
    return "standard output";
}


// vim: set ts=8 sw=4 et :
