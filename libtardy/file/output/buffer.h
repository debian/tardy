//
// tardy - a tar post-processor
// Copyright (C) 2003, 2004, 2008, 2009, 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_FILE_OUTPUT_BUFFER_H
#define LIBTARDY_FILE_OUTPUT_BUFFER_H

#include <libtardy/file/output.h>

/**
  * The file_output_buffer class is used to represent a buffered output
  * stream.  The data is passed through unchanged, however it is
  * buffered before delivery to the deeper output stream.
  */
class file_output_buffer:
    public file_output
{
public:
    /**
      * The destructor.
      */
    virtual ~file_output_buffer();

    /**
      * The create class method is used to create new dynmically
      * allocated instances of this class.
      *
      * @param deeper
      *     This argument is used to specify the output stream to write
      *     the data to once it has been buffered.
      * @param blocksize
      *     This argument my be used to specify the block size, in bytes.
      *     It defaults to 512 bytes if zero.
      */
    static pointer create(const file_output::pointer &deeper,
        size_t blocksize = 0);

protected:
    // See base class for documentation.
    rcstring filename(void) const;

    // See base class for documentation.
    void write(const void *data, size_t data_size);

private:
    /**
      * The constructor.
      * It is private on purpose, use the #create class method instead.
      *
      * @param deeper
      *     This argument is used to specify the output stream to write
      *     the data to once it has been buffered.
      * @param blocksize
      *     This argument my be used to specify the block size, in bytes.
      */
    file_output_buffer(const file_output::pointer &deeper, int blocksize);

    /**
      * The deeper instance variable is used to remember the output file
      * to write the output to, once it has been buffered.
      */
    file_output::pointer deeper;

    /**
      * The blocksize instance variable is used to remember the size of
      * blocks to be wrutten to the deeper output.
      */
    size_t blocksize;

    /**
      * The buffer instance variable is used to remember whe the buffer
      * of blocksize byte has been allocated in the heap.
      */
    char *buffer;

    /**
      * The pos instance variable is used to remember how many bytes
      * have been buffered into `buffer'.
      *
      * Invariant: assert(pos >= 0 && pos < blocksize);
      */
    size_t pos;

    /**
      * The default constructor.  Do not use.
      */
    file_output_buffer();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    file_output_buffer(const file_output_buffer &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the assignment.
      */
    file_output_buffer &operator=(const file_output_buffer &rhs);
};

// vim: set ts=8 sw=4 et :
#endif // LIBTARDY_FILE_OUTPUT_BUFFER_H
