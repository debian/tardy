//
// tardy - a tar post-processor
// Copyright (C) 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/file/input/xz.h>
#include <libtardy/file/output/xz.h>


bool
file_output_xz::candidate(const rcstring &filename)
{
    return (filename != file_input_xz::remove_filename_suffix(filename));
}


file_output_xz::pointer
file_output_xz::create_if_candidate(const file_output::pointer &a_deeper)
{
    if (!candidate(a_deeper->filename()))
        return a_deeper;
    return create(a_deeper);
}


// vim: set ts=8 sw=4 et :
