//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2002-2004, 2008, 2009, 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_FILE_OUTPUT_NORMAL_H
#define LIBTARDY_FILE_OUTPUT_NORMAL_H

#include <libtardy/file/output.h>
#include <libtardy/rcstring.h>

/**
  * The file_output_normal class is used to represent an output stream
  * writing to a normal file.
  */
class file_output_normal:
    public file_output
{
public:
    /**
      * The destructor.
      */
    virtual ~file_output_normal();

    /**
      * The create class method is used to create new dynmically
      * allocated instances of this class.
      *
      * @param filename
      *     The name of the file to write the output to.
      */
    static pointer create(const char *filename);

protected:
    // See base class for documentation.
    virtual void write(const void *data, size_t data_size);

    // See base class for documentation.
    rcstring filename(void) const;

private:
    /**
      * The constructor.
      * It is private on purpose, use the #create class method instead.
      *
      * @param filename
      *     The name of the file to write the output to.
      */
    file_output_normal(const char *filename);

    /**
      * The fn instance variable is used to remember the name of the
      * file being written.
      */
    rcstring fn;

    /**
      * The fd instance variable is used to remember the file descriptor
      * of the open file.
      */
    int fd;

    /**
      * The default constructor.  Do not use.
      */
    file_output_normal();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    file_output_normal(const file_output_normal &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the assignment.
      */
    file_output_normal &operator = (const file_output_normal &rhs);
};

// vim: set ts=8 sw=4 et :
#endif // LIBTARDY_FILE_OUTPUT_NORMAL_H
