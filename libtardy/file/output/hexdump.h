//
// tardy - a tar post-processor
// Copyright (C) 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_FILE_OUTPUT_HEXDUMP_H
#define LIBTARDY_FILE_OUTPUT_HEXDUMP_H

#include <libtardy/file/output.h>

/**
  * The file_output_hexdump class is used to represent a filter that
  * converts binary data into a text hexadecimal hex dump.
  * This of most use when debugging and testing.
  */
class file_output_hexdump:
    public file_output
{
public:
    typedef boost::shared_ptr<file_output_hexdump> pointer;

    /**
      * The destructor.
      */
    virtual ~file_output_hexdump();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *     The deeper output to receive our output.
      */
    static pointer create(const file_output::pointer &deeper);

protected:
    // See base class for documentation.
    void write(const void *data, size_t data_size);

    // See base class for documentation.
    rcstring filename(void) const;

private:
    /**
      * The constructor.
      * It is private on purpose, use a #create class method instead.
      *
      * @param deeper
      *     The deeper output to receive our output.
      */
    file_output_hexdump(const file_output::pointer &deeper);

    /**
      * The deeper instance variable is used to remember the deeper
      * output stream to receive our output.
      */
    file_output::pointer deeper;

    /**
      * The address instance variable is used to remember the current
      * position within the file.  This controls when and how the
      * #buffer is constructed and printed.
      * It could be greater than 2GB, which is why we must use off_t.
      */
    off_t address;

    /**
      * The buffer instance variable is used to remember the
      * output line being constructed.
      */
    char buffer[3 * 16 + 2 + 16 + 1];

    /**
      * The default constructor.  Do not use.
      */
    file_output_hexdump();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    file_output_hexdump(const file_output_hexdump &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    file_output_hexdump &operator=(const file_output_hexdump &rhs);
};

// vim: set ts=8 sw=4 et :
#endif // LIBTARDY_FILE_OUTPUT_HEXDUMP_H
