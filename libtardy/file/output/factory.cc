//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/file/output/normal.h>
#include <libtardy/file/output/stdout.h>


file_output::pointer
file_output::factory(const char *filename)
{
    if
    (
        !filename
    ||
        *filename == '\0'
    ||
        (filename[0] == '-' && filename[1] == '\0')
    )
        return file_output_stdout::create();
    return file_output_normal::create(filename);
}
