//
// tardy - a tar post-processor
// Copyright (C) 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_FILE_OUTPUT_XZ_H
#define LIBTARDY_FILE_OUTPUT_XZ_H

#include <libtardy/ac/lzma.h>

#include <libtardy/file/output.h>

/**
  * The file_output_xz class is used to represent the processing required to
  * compress output using the "xz" compression format (see liblzma-dev for more
  * info) before writing it to the deeper output stream.
  *
  * https://en.wikipedia.org/wiki/LZMA
  */
class file_output_xz:
    public file_output
{
public:
    /**
      * The destructor.
      */
    virtual ~file_output_xz();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *     the output stream this filter will write its output to.
      */
    static pointer create(const file_output::pointer &deeper);

    /**
      * The candidate class method is used to examine the given file
      * name to determine whether or not it looks like a xz file.
      *
      * @param filename
      *     The file name to be examined.
      * @returns
      *     true if looks like a xz filename, false if not
      */
    static bool candidate(const rcstring &filename);

    /**
      * The create_if_candidate class method is used to create new
      * dynamically allocated instances of this class, if the deeper
      * file's name looks like a xz candidate, otherwite the deeper
      * file is simply passed through.
      *
      * @param deeper
      *     the output stream this filter will write its output to.
      */
    static pointer create_if_candidate(const file_output::pointer &deeper);

protected:
    // See base class for documentation.
    rcstring filename(void) const;

    // See base class for documentation.
    void write(const void *data, size_t data_size);

private:
    /**
      * The constructor.  It is private on purpose, use the #create
      * class method instead.
      *
      * @param deeper
      *     the output stream this filter will write its output to.
      */
    file_output_xz(const file_output::pointer &deeper);

    /**
      * The deeper instance variable is used to remember the output
      * stream this filter will write its output to.
      */
    file_output::pointer deeper;

    lzma_stream strm;

    void drop_dead(const char *caption, lzma_ret);

    /**
      * The default constructor.  Do not use.
     */
    file_output_xz();

    /**
      * The copy constructor.  Do not use.
      */
    file_output_xz(const file_output_xz &rhs);

    /**
      * The assignment operator.  Do not use.
      */
    file_output_xz &operator=(const file_output_xz &rhs);
};

// vim: set ts=8 sw=4 et :
#endif // LIBTARDY_FILE_OUTPUT_XZ_H
