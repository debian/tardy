//
// tardy - a tar post-processor
// Copyright (C) 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/ctype.h>
#include <libtardy/ac/limits.h>
#include <libtardy/ac/string.h>

#include <libtardy/file/output/hexdump.h>


file_output_hexdump::~file_output_hexdump()
{
    if (address & 15)
    {
        deeper->write(buffer, sizeof(buffer));
        address = 0;
    }
}


file_output_hexdump::file_output_hexdump(const file_output::pointer &a_deeper) :
    deeper(a_deeper),
    address(0)
{
}


file_output_hexdump::pointer
file_output_hexdump::create(const file_output::pointer &a_deeper)
{
    return pointer(new file_output_hexdump(a_deeper));
}


static void
hex(char *buffer, unsigned long value, size_t buffer_size)
{
    while (buffer_size > 0)
    {
        --buffer_size;
        buffer[buffer_size] = "0123456789ABCDEF"[value & 15];
        value >>= 4;
    }
}


void
file_output_hexdump::write(const void *vdata, size_t data_size)
{
    unsigned char *data = (unsigned char*)vdata;
    while (data_size > 0)
    {
        unsigned char c = *data;
        ++data;
        --data_size;

        unsigned pos = address & 15;
        if (pos == 0)
        {
            if
            (
                sizeof(off_t) * CHAR_BIT > 32
            &&
                address >= ((off_t)1 << 32)
            )
            {
                assert(sizeof(buffer) >= 18);
                hex(buffer, address >> 32, 8);
                buffer[8] = ' ';
                hex(buffer + 9, address & 0xFFFFFFFFuL, 8);
                buffer[17] = ':';
                deeper->write(buffer, 18);
            }
            else
            {
                assert(sizeof(buffer) >= 9);
                hex(buffer, address, 8);
                buffer[8] = ':';
                deeper->write(buffer, 9);
            }

            memset(buffer, ' ', sizeof(buffer) - 1);
            buffer[sizeof(buffer) - 1] = '\n';
        }
        hex(buffer + 1 + 3 * pos, c, 2);
        c &= 0x7F;
        if (!isprint(c))
            c = '.';
        buffer[50 + pos] = c;
        if (pos == 15)
            deeper->write(buffer, sizeof(buffer));
        ++address;
    }
}


rcstring
file_output_hexdump::filename(void)
    const
{
    return deeper->filename();
}


// vim: set ts=8 sw=4 et :
