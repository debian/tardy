//
// tardy - a tar post-processor
// Copyright (C) 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_FILE_OUTPUT_GZIP_H
#define LIBTARDY_FILE_OUTPUT_GZIP_H

#include <libtardy/ac/zlib.h>

#include <libtardy/file/output.h>

/**
  * The file_output_gzip class is used to represent output filter which
  * compresses the data (using gzip algorithm) before writing it to the
  * deeper output stream.
  */
class file_output_gzip:
    public file_output
{
public:
    /**
      * The destructor.
      */
    virtual ~file_output_gzip();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *     the output stream this filter will write its output to.
      */
    static pointer create(const file_output::pointer &deeper);

    /**
      * The candidate class method is used to examine the given file
      * name to determine whether or not it looks like a gzip file.
      *
      * @param filename
      *     The file name to be examined.
      * @returns
      *     true if looks like a gzip filename, false if not
      */
    static bool candidate(const rcstring &filename);

    /**
      * The create_if_candidate class method is used to create new
      * dynamically allocated instances of this class, if the deeper
      * file's name looks like a gzip candidate, otherwite the deeper
      * file is simple passed through.
      *
      * @param deeper
      *     the output stream this filter will write its output to.
      */
    static pointer create_if_candidate(const file_output::pointer &deeper);

protected:
    // See base class for documentation.
    rcstring filename(void) const;

    // See base class for documentation.
    void write(const void *data, size_t data_size);

private:
    /**
      * The constructor.  It is private on purpose, use the #create
      * class method instead.
      *
      * @param deeper
      *     the output stream this filter will write its output to.
      */
    file_output_gzip(const file_output::pointer &deeper);

    /**
      * The deeper instance variable is used to remember the output
      * stream this filter will write its output to.
      */
    file_output::pointer deeper;

    /**
      * The stream instance variable is used to remember somthign that
      * the gzip code wants to remember.  It is opaque to us.
      */
    z_stream stream;

    /**
      * The outbuf instance variable is used to remember the buffered
      * output data.
      */
    Byte *outbuf;

    /**
      * The crc instance variable is used to remember the crc32 of the
      * uncompressed data.
      */
    uLong crc;

    /**
      * The drop_dead method is used to report a fatal error from the
      * gzip engine.
      */
    void drop_dead(int err);

    void put4(unsigned long value);

    /**
      * The default constructor.  Do not use.
     */
    file_output_gzip();

    /**
      * The copy constructor.  Do not use.
      */
    file_output_gzip(const file_output_gzip &);

    /**
      * The assignment operator.  Do not use.
      */
    file_output_gzip &operator=(const file_output_gzip &);
};

// vim: set ts=8 sw=4 et :
#endif // LIBTARDY_FILE_OUTPUT_GZIP_H
