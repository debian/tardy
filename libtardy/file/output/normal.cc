//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2003, 2008, 2009, 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/assert.h>
#include <libtardy/ac/fcntl.h>
#include <libtardy/ac/unistd.h>
#include <libexplain/close.h>
#include <libexplain/creat.h>
#include <libexplain/write.h>

#include <libtardy/file/output/normal.h>


file_output_normal::~file_output_normal()
{
    if (fd >= 0)
        explain_close_or_die(fd);
}


file_output_normal::file_output_normal(const char *a_fn) :
    fn(a_fn),
    fd(-1)
{
    fd = explain_creat_or_die(fn.c_str(), 0666);
}


file_output::pointer
file_output_normal::create(const char *a_fn)
{
    return pointer(new file_output_normal(a_fn));
}


void
file_output_normal::write(const void *buffer, size_t nbytes)
{
    ssize_t count = explain_write_or_die(fd, buffer, nbytes);
    assert(count >= 0);
    // it would be nice to use %zu but backwards compatibility prevents
    if ((size_t)count != nbytes)
    {
        fatal
        (
            "short write (gave %lu, got %lu)",
            (unsigned long)nbytes,
            (unsigned long)count
        );
    }
}

rcstring
file_output_normal::filename(void)
    const
{
    return fn;
}


// vim: set ts=8 sw=4 et :
