//
// tardy - a tar post-processor
// Copyright (C) 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/file/input/bunzip2.h>


bool
file_input_bunzip2::candidate(const file_input::pointer &a_deeper)
{
    //
    // Check for the magic number.
    //
    unsigned char buf[4];
    long n = a_deeper->peek(buf, 4);
    return
        (
            n == 4
        &&
            buf[0] == 'B'
        &&
            buf[1] == 'Z'
        &&
            buf[2] == 'h'
        &&
            buf[3] >= '1'
        &&
            buf[3] <= '9'
        );
}


// vim: set ts=8 sw=4 et :
