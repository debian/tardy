//
// tardy - a tar post-processor
// Copyright (C) 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_FILE_INPUT_POSITION_H
#define LIBTARDY_FILE_INPUT_POSITION_H

#include <libtardy/file/input.h>

/**
  * The file_input_position class is used to represent the processing
  * required to track the file position for non-seekable files (pipes,
  * for example).
  */
class file_input_position:
    public file_input
{
public:
    /**
      * The destructor.
      */
    virtual ~file_input_position();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *     The file inoput that is to be filtered.
      */
    static pointer create(const file_input::pointer &deeper);

protected:
    // See base class for documentation.
    rcstring filename(void) const;

    // See base class for documentation.
    size_t read_inner(void *data, size_t data_size);

    // See base class for documentation.
    off_t get_position_inner(void) const;

private:
    /**
      * The constructor.
      * It is private on purpose, use a #create class method instead.
      *
      * @param deeper
      *     The file inoput that is to be filtered.
      */
    file_input_position(const file_input::pointer &deeper);

    /**
      * The deeper instance variable is used to remember the file input
      * to be filtered.
      */
    file_input::pointer deeper;

    /**
      * The position instance variable is used to remember the current
      * read position within the file input being filtered.
      * It could be greater than 2GB, which is why we must use off_t.
      */
    off_t position;

    /**
      * The default constructor.  Do not use.
      */
    file_input_position();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    file_input_position(const file_input_position &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    file_input_position &operator=(const file_input_position &rhs);
};

// vim: set ts=8 sw=4 et :
#endif // LIBTARDY_FILE_INPUT_POSITION_H
