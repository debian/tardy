//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2008, 2009, 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/sys/stat.h>
#include <libtardy/ac/unistd.h>
#include <libexplain/fstat.h>
#include <libexplain/lseek.h>
#include <libexplain/read.h>

#include <libtardy/file/input/stdin.h>
#include <libtardy/file/input/position.h>


file_input_stdin::~file_input_stdin()
{
    // nothing to do
}


file_input_stdin::file_input_stdin()
{
    // nothing to do
}


file_input::pointer
file_input_stdin::create(void)
{
    typedef boost::shared_ptr<file_input_stdin> ptr;
    ptr p(new file_input_stdin());
    if (p->needs_position_filter())
        return file_input_position::create(p);
    return p;
}


bool
file_input_stdin::needs_position_filter(void)
    const
{
    int fd = 0;
    struct stat st;
    explain_fstat_or_die(fd, &st);
    if (!S_ISREG(st.st_mode))
        return true;
    return (lseek(fd, 0, SEEK_CUR) != 0);
}


size_t
file_input_stdin::read_inner(void *data, size_t data_size)
{
    int fd = 0;
    return explain_read_or_die(fd, data, data_size);
}


rcstring
file_input_stdin::filename(void)
    const
{
    return "standard input";
}


off_t
file_input_stdin::get_position_inner(void)
    const
{
    int fd = 0;
    return explain_lseek_or_die(fd, 0, SEEK_CUR);
}


// vim: set ts=8 sw=4 et :
