//
// tardy - a tar post-processor
// Copyright (C) 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_FILE_INPUT_XZ_H
#define LIBTARDY_FILE_INPUT_XZ_H

#include <libtardy/ac/lzma.h>

#include <libtardy/file/input.h>

/**
  * The file_input_xz class is used to represent an LZMA input stream
  * which is uncompressed on the fly.
  */
class file_input_xz:
    public file_input
{
public:
    /**
      * The destructor.
      */
    virtual ~file_input_xz();

    /**
      * The create class method is used to create new dynamically
      * allocated instance of this class.
      *
      * @param deeper
      *     The deeper input which this filter reads from.
      */
    static pointer create(const file_input::pointer &deeper);

    /**
      * The create_if_candidate class method is used to sniff at the
      * input file, to see if it is compressed.  If it is compressed,
      * the #create class method will be called, otherwise the deeper
      * file will be returned unfiltered.
      *
      * @param deeper
      *     The deeper input which is to be filtered (conditionally).
      */
    static pointer create_if_candidate(const file_input::pointer &deeper);

    /**
      * The remove_filename_suffix class method is used to examine a
      * filename, and if it ends with a xz-style file extension,
      * remove the file extension, otherwise return the file name
      * unaltered.
      */
    static rcstring remove_filename_suffix(const rcstring &filename);

protected:
    // See base class for documentation.
    rcstring filename(void) const;

    // See base class for documentation.
    size_t read_inner(void *data, size_t data_size);

    // See base class for documentation.
    off_t get_position_inner(void) const;

    /**
      * The candidate class method is used to check the magic number of a
      * xz'ed file.  All of the bytes read are unread before this method
      * returns.
      */
    static bool candidate(const file_input::pointer &deeper);

private:
    /**
      * The constructor.
      * It is private on purpose, use the #create class method instead.
      *
      * @param deeper
      *     The deeper input which this filter reads from.
      */
    file_input_xz(const file_input::pointer &deeper);

    /**
      * The deeper instance variable is used to remember the deeper
      * input which this filter reads from.
      */
    file_input::pointer deeper;

    /**
      * The stream instance variable is used to remember the data
      * private to the lzma library.
      */
    lzma_stream strm;

    /**
      * The end_of_file instance variable is used to remember whether or
      * not this input stream has reached the end of input.
      */
    bool end_of_file;

    /**
      * the pos instance variable is used to remember the current read
      * position, in bytes, relative to the start of this stream.
      * It could be greater than 2GB, which is why we must use off_t.
      */
    off_t pos;

    /**
      * The buf instance variable is used to remember the base address
      * of a dynamically allocated array of bytes, of size #BUFFER_SIZE
      * bytes.
      */
    char *buf;

    static const size_t BUFFER_SIZE = ((size_t)1u << 14);

    /**
      * The drop_dead method is used to report fatal error from
      * the xz engine.
      *
      * @param caption
      *     more explanatory text, usually the function name
      * @param err
      *     The error code returned by a deeper bzlib function.
      * @note
      *     This method does not return.
      */
    void drop_dead(const char *caption, lzma_ret err);

    /**
      * The deeper_at_end instance variable is used to remember whether
      * or not the deeper input stream has reached end of file.
      */
    bool deeper_at_end;

    /**
      * The default constructor.  Do not use.
      */
    file_input_xz();

    /**
      * The copy constructor.  Do not use.
      */
    file_input_xz(const file_input_xz &rhs);

    /**
      * The assignment operator.  Do not use.
      */
    file_input_xz &operator=(const file_input_xz &rhs);
};

// vim: set ts=8 sw=4 et :
#endif // LIBTARDY_FILE_INPUT_XZ_H
