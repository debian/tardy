//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2002, 2004, 2008, 2009, 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_FILE_INPUT_NORMAL_H
#define LIBTARDY_FILE_INPUT_NORMAL_H

#include <libtardy/rcstring.h>
#include <libtardy/file/input.h>

/**
  * The file_input_normal class is used to represent input from a regular
  * file.
  */
class file_input_normal:
    public file_input
{
public:
    /**
      * The destructor.
      */
    virtual ~file_input_normal();

    /**
      * Thye create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param filename
      *     The name of the file to be opened.
      */
    static pointer create(const rcstring &filename);

protected:
    // See base class for documentation.
    virtual size_t read_inner(void *data, size_t data_size);

    // See base class for documentation.
    rcstring filename(void) const;

    // See base class for documentation.
    off_t get_position_inner(void) const;

private:
    /**
      * The constructor.
      * It is private on purpose, the the #create class method instead.
      *
      * @param filename
      *     The name of the file to be opened.
      */
    file_input_normal(const rcstring &filename);

    /**
      * The fn instance variable is used to remember the name of the
      * input file.
      */
    rcstring fn;

    /**
      * The fd instance variable is used to remember the file descriptor
      * of the open input file.
      */
    int fd;

    /**
      * The needs_position_filter method is used by the #create class
      * method, to add a position filter to all files that are not regular
      * (seekable) files.  For example, pipes need the position filter.
      */
    bool needs_position_filter(void) const;

    /**
      * The default constructor.  Do not use.
      */
    file_input_normal();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    file_input_normal(const file_input_normal &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the assignment.
      */
    file_input_normal &operator=(const file_input_normal &rhs);
};

// vim: set ts=8 sw=4 et :
#endif // LIBTARDY_FILE_INPUT_NORMAL_H
