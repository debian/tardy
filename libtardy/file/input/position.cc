//
// tardy - a tar post-processor
// Copyright (C) 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/file/input/position.h>


file_input_position::~file_input_position()
{
}


file_input_position::file_input_position(const file_input::pointer &a_deeper) :
    deeper(a_deeper),
    position(0)
{
    assert(deeper);
}


file_input_position::pointer
file_input_position::create(const file_input::pointer &a_deeper)
{
    return pointer(new file_input_position(a_deeper));
}


rcstring
file_input_position::filename(void)
    const
{
    return deeper->filename();
}


size_t
file_input_position::read_inner(void *data, size_t data_size)
{
    // only ever returns on success.
    size_t nbytes = deeper->read(data, data_size);
    position += nbytes;
    return nbytes;
}


off_t
file_input_position::get_position_inner(void)
    const
{
    return position;
}


// vim: set ts=8 sw=4 et :
