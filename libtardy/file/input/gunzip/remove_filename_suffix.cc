//
// tardy - a tar post-processor
// Copyright (C) 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/file/input/gunzip.h>

//
// This method is in a separate file so that the file_output_gzip
// class's methods can access it without dragging in all of the rest of
// the file_input_gunzip class's methods.
//

rcstring
file_input_gunzip::remove_filename_suffix(const rcstring &fn)
{
    rcstring fn_lc = fn.downcase();
    if (fn_lc.ends_with(".z"))
        return fn.substr(0, fn.size() - 2);
    if (fn_lc.ends_with(".gz"))
        return fn.substr(0, fn.size() - 3);
    if (fn_lc.ends_with(".tgz"))
        return fn.substr(0, fn.size() - 4) + ".tar";
    return fn;
}


// vim: set ts=8 sw=4 et :
