//
// tardy - a tar post-processor
// Copyright (C) 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/string.h>

#include <libtardy/file/input/xz.h>


#if 0

static bool
is_lzma(const file_input::pointer &a_deeper)
{
    //
    // This brain-dead format doesn't HAVE a magic number,
    // so any random 13-byte sequence will test as OK.
    //
    unsigned char magic[13];
    size_t n = a_deeper->peek(magic, 13);
    if (n != 13)
        return false;
    lzma_filter filter;
    filter.id = LZMA_FILTER_LZMA1;
    if (LZMA_OK != lzma_properties_decode(&filter, NULL, magic, 5))
        return false;
    lzma_options_lzma *opt = (lzma_options_lzma *)filter.options;
    if (!opt)
        return false;
    bool ok = (opt->lc == 3 && opt->lp == 0 && opt->pb == 2);
    free(opt);
    return ok;
}

#endif


static bool
is_xz(const file_input::pointer &a_deeper)
{
    static unsigned char magic[6] = { 0xFD, 0x37, 0x7A, 0x58, 0x5A, 0x00 };
    unsigned char buf[sizeof(magic)];
    size_t n = a_deeper->peek(buf, sizeof(buf));
    if (n != sizeof(buf))
        return false;
    return (0 == memcmp(magic, buf, sizeof(magic)));
}


bool
file_input_xz::candidate(const file_input::pointer &a_deeper)
{
    return is_xz(a_deeper);
}


// vim: set ts=8 sw=4 et :
