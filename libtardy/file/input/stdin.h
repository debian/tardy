//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2004, 2008, 2009, 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_FILE_INPUT_STDIN_H
#define LIBTARDY_FILE_INPUT_STDIN_H

#include <libtardy/file/input.h>

/**
  * The file_input_stdin is used to represent input from the process's
  * standard input stream.
  */
class file_input_stdin:
    public file_input
{
public:
    /**
      * The destructor.
      */
    ~file_input_stdin();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      */
    static pointer create(void);

protected:
    // See base class for documentation.
    virtual size_t read_inner(void *data, size_t data_size);

    // See base class for documentation.
    rcstring filename(void) const;

    // See base class for documentation.
    off_t get_position_inner(void) const;

private:
    /**
      * The needs_position_filter method is used by the #create class
      * method, to add a position filter to all files that are not regular
      * (seekable) files.  For example, pipes need the position filter.
      */
    bool needs_position_filter(void) const;

    /**
      * The default constructor.
      * It is private on purpose, the the #create class method instead.
      */
    file_input_stdin();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    file_input_stdin(const file_input_stdin &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the assignment.
      */
    file_input_stdin &operator=(const file_input_stdin &rhs);
};

// vim: set ts=8 sw=4 et :
#endif // LIBTARDY_FILE_INPUT_STDIN_H
