//
// tardy - a tar post-processor
// Copyright (C) 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/file/input/bunzip2.h>
#include <libtardy/file/input/gunzip.h>
#include <libtardy/file/input/normal.h>
#include <libtardy/file/input/stdin.h>
#include <libtardy/file/input/xz.h>


static bool
looks_like_stdin(const char *filename)
{
    return
        (
            !filename
        ||
            *filename == '\0'
        ||
            (filename[0] == '-' && filename[1] == '\0')
        );
}


file_input::pointer
file_input::factory(const char *filename)
{
    file_input::pointer ip =
        (
            looks_like_stdin(filename)
        ?
            file_input_stdin::create()
        :
            file_input_normal::create(filename)
        );
    ip = file_input_gunzip::create_if_candidate(ip);
    ip = file_input_bunzip2::create_if_candidate(ip);
    ip = file_input_xz::create_if_candidate(ip);
    return ip;
}


// vim: set ts=8 sw=4 et :
