//
// tardy - a tar post-processor
// Copyright (C) 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_FILE_INPUT_GUNZIP_H
#define LIBTARDY_FILE_INPUT_GUNZIP_H

#include <libtardy/ac/zlib.h>

#include <libtardy/file/input.h>

/**
  * The file_input_gunzip class is used to represent an input stream which is
  * uncompressed on the fly.
  */
class file_input_gunzip:
    public file_input
{
public:
    /**
      * The destructor.
      */
    virtual ~file_input_gunzip();

    /**
      * The create class method is used to create new dynamically
      * allocated instance of this class.
      *
      * @param deeper
      *     The deeper input which this filter reads from.
      */
    static pointer create(const file_input::pointer &deeper);

    /**
      * The create_if_candidate class method is used to sniff at the
      * input file, to see if it is compressed.  If it is compressed,
      * the #create class method will be called, otherwise the deeper
      * file will be returned unfiltered.
      *
      * @param deeper
      *     The deeper input which is to be filtered (conditionally).
      */
    static pointer create_if_candidate(const file_input::pointer &deeper);

    /**
      * The remove_filename_suffix class method is used to examine a
      * filename, and if it ends with a gzip-style file extension,
      * remove the file extension, otherwise return the file name
      * unaltered.
      */
    static rcstring remove_filename_suffix(const rcstring &filename);

protected:
    // See base class for documentation.
    rcstring filename(void) const;

    // See base class for documentation.
    size_t read_inner(void *data, size_t data_size);

    // See base class for documentation.
    off_t get_position_inner(void) const;

    /**
      * The candidate class method is used to check the magic number of a
      * gzipped file.  All of the bytes read are unread before this method
      * returns.
      */
    static bool candidate(const file_input::pointer &deeper);

private:
    /**
      * The constructor.
      * It is private on purpose, use the #create class method instead.
      *
      * @param deeper
      *     The deeper input which this filter reads from.
      */
    file_input_gunzip(const file_input::pointer &deeper);

    /**
      * The deeper instance variable is used to remember the deeper
      * input which this filter reads from.
      */
    file_input::pointer deeper;

    /**
      * The steam instance variable is used to remember remmebr the
      * opaque blob of data used by the zlib1g library.
      */
    z_stream stream;

    /**
      * The z_eof instance variable is used to remember whether or not
      * we have reached the end of the compressed data stream.
      */
    bool z_eof;

    /**
      * The crc instance variable is used to remember the running
      * checksum of the decompressed data. Used for input validation at
      * the end of the file.
      */
    uLong crc;

    /**
      * The pos instance variable is used to remember the current read
      * position, in bytes, relative to the start of this stream.
      * It could be greater than 2GB, which is why we must use off_t.
      */
    off_t pos;

    /**
      * The buf instance variable is used to remember the base address
      * of a dynamically allocated array of bytes.
      */
    Byte *buf;

    /**
      * The zlib_fatal_error method is used to print errors reported by
      * the zlib1g library, and then exit.
      *
      * @param err
      *     The error reported by the zlib1g library.
      */
    void zlib_fatal_error(int err);

    /**
      * The get_long method is used to extract a 4-byte little-endian
      * value from the deeper input stream.
      */
    unsigned long get_long(void);

    /**
      * The get_short method is used to extract a 2-byte little-endian
      * value from the deeper input stream.
      */
    unsigned get_short(void);

    /**
      * The get_header method is used to read and check the GZIP file
      * header in the input stream.
      */
    void read_header(void);

    /**
      * The skip_string method is sued to advance the deeper file
      * position past a NUL-terminated string in the deeper input.
      */
    void skip_string(void);

    /**
      * The default constructor.  Do not use.
      */
    file_input_gunzip();

    /**
      * The copy constructor.  Do not use.
      */
    file_input_gunzip(const file_input_gunzip &arg);

    /**
      * The assignment operator.  Do not use.
      */
    file_input_gunzip &operator=(const file_input_gunzip &arg);
};

// vim: set ts=8 sw=4 et :
#endif // LIBTARDY_FILE_INPUT_GUNZIP_H
