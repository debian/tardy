//
//      tardy - a tar post-processor
//      Copyright (C) 1995, 1998, 1999, 2008-2010 Peter Miller
//
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program. If not, see
//      <http://www.gnu.org/licenses/>.
//

#include <libtardy/version_stmp.h>
#include <libtardy/patchlevel.h>


const char *
version_stamp(void)
{
        return PATCHLEVEL;
}


const char *
copyright_years(void)
{
        return COPYRIGHT_YEARS;
}
