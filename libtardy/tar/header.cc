//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2003, 2008, 2009 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/tar/header.h>


tar_header::~tar_header()
{
}


tar_header::tar_header() :
        atime(0),
        ctime(0),
        device_major(0),
        device_minor(0),
        group_id(0),
        group_name(),
        inode_number(0),
        link_count(1),
        linkname(),
        mode(0),
        mtime(0),
        name(),
        rdevice_major(0),
        rdevice_minor(0),
        size(0),
        type(type_normal),
        user_id(0),
        user_name()
{
}


tar_header::tar_header(const tar_header &arg) :
        atime(arg.atime),
        ctime(arg.ctime),
        device_major(arg.device_major),
        device_minor(arg.device_minor),
        group_id(arg.group_id),
        group_name(arg.group_name),
        inode_number(arg.inode_number),
        link_count(arg.link_count),
        linkname(arg.linkname),
        mode(arg.mode),
        mtime(arg.mtime),
        name(arg.name),
        rdevice_major(arg.rdevice_major),
        rdevice_minor(arg.rdevice_minor),
        size(arg.size),
        type(arg.type),
        user_id(arg.user_id),
        user_name(arg.user_name)
{
}


tar_header &
tar_header::operator = (const tar_header &arg)
{
        atime = arg.atime;
        ctime = arg.ctime;
        device_major = arg.device_major;
        device_minor = arg.device_minor;
        group_id = arg.group_id;
        group_name = arg.group_name;
        inode_number = arg.inode_number;
        link_count = arg.link_count;
        linkname = arg.linkname;
        mode = arg.mode;
        mtime = arg.mtime;
        name = arg.name;
        rdevice_major = arg.rdevice_major;
        rdevice_minor = arg.rdevice_minor;
        size = arg.size;
        type = arg.type;
        user_id = arg.user_id;
        user_name = arg.user_name;
        return *this;
}
