//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2002, 2003, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_HEADER_H
#define LIBTARDY_TAR_HEADER_H

#include <libtardy/ac/time.h>
#include <libtardy/rcstring.h>


class tar_header
{
public:
    enum type_ty
    {
        type_device_block,
        type_device_character,
        type_directory,
        type_fifo,
        type_link_hard,
        type_link_symbolic,
        type_normal,
        type_normal_contiguous,
        type_normal_gzipped,
        type_socket
    };

    time_t atime;
    time_t ctime;
    long device_major;
    long device_minor;
    long group_id;
    rcstring group_name;
    long inode_number;
    long link_count;
    rcstring linkname;
    long mode;
    time_t mtime;
    rcstring name;
    long rdevice_major;
    long rdevice_minor;
    long size;
    type_ty type;
    long user_id;
    rcstring user_name;

    ~tar_header();
    tar_header();
    tar_header(const tar_header &);
    tar_header &operator = (const tar_header &);
};

#endif // LIBTARDY_TAR_HEADER_H
