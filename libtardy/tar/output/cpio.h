//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_OUTPUT_CPIO_H
#define LIBTARDY_TAR_OUTPUT_CPIO_H

#include <libtardy/file/output.h>
#include <libtardy/tar/output.h>

/**
  * The tar_output_tar abstract class reperesents a generic CPIO
  * formatted archive output, and the current state of the output.
  * (The leading tar_ prefix is an accident of history.)
  *
  * There are several cpio(5) formats, this class shall be further derived
  * to represent each kind.
  */
class tar_output_cpio:
    public tar_output
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_output_cpio();

protected:
    /**
      * The constructor.
      * For use by derived classes only.
      *
      * @param fp
      *     The file to be written.
      * @param padding
      *     The amount of bytes of data padding to use (zero or negative
      *     mean none).
      */
    tar_output_cpio(const file_output::pointer &fp, int padding);

    // See base class for documentation.
    virtual void write_header_padding(void);

    // See base class for documentation.
    virtual void write_data(const void *data, int data_size);

    // See base class for documentation.
    virtual void write_data_padding(void);

    // See base class for documentation.
    virtual void write_archive_end(void);

    // See base class for documentation.
    rcstring filename(void) const;

    /**
      * The write_deeper method is used to derived classes to write
      * directly to the binary archive.
      *
      * @param data
      *     Pointer to the base of the array of bytes to be written.
      * @param data_size
      *     Size of the base of the array of bytes to be written.
      */
    void write_deeper(const void *data, size_t data_size);

    /**
      * The write_padding method may be used to write zero-valued bytes
      * upto the next padding boundary.
      */
    void write_padding(void);

private:
    /**
      * The fp instance variable is used to remember where to send the
      * output, usually the file to write the output to.
      */
    file_output::pointer fp;

    /**
      * The padding instance variable is used to remember the byte
      * multiple that file headers and file data are to start on.  Zero
      * means no alignment required.
      */
    unsigned padding;

    /**
      * The pos instance variable is used to remember the output file
      * position, this is used when calculating how much padding to
      * insert.
      */
    unsigned long pos;

private:
    /**
      * The default constructor.  Do not use.
      */
    tar_output_cpio();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    tar_output_cpio(const tar_output_cpio &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the assignment.
      */
    tar_output_cpio &operator = (const tar_output_cpio &rhs);
};

#endif // LIBTARDY_TAR_OUTPUT_CPIO_H
