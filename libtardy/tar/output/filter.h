//
// tardy - a tar post-processor
// Copyright (C) 2003, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_OUTPUT_FILTER_H
#define LIBTARDY_TAR_OUTPUT_FILTER_H

#include <libtardy/tar/output.h>

/**
  * The tar_output_filter class is used to represent a generic filter
  * class which modifies the file headers or data in some way.
  * The default operation of the methods of this class is to forward the
  * operation to the deepr instance; derived classes will override those
  * methods which need a different behaviour.
  */
class tar_output_filter:
    public tar_output
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_output_filter();

protected:
    /**
      * The constructor.
      * For use by derived classes only.
      *
      * @param deeper
      *     The underlying archive to be written to.
      */
    tar_output_filter(const tar_output::pointer &deeper);

    // See base class for documentation.
    void write_header(const tar_header &);

    // See base class for documentation.
    void write_header_padding(void);

    // See base class for documentation.
    void write_data(const void *, int);

    // See base class for documentation.
    void write_data_padding(void);

    // See base class for documentation.
    rcstring filename(void) const;

    // See base class for documentation.
    const char *get_format_name(void) const;

    // See base class for documentation.
    void write_archive_begin(void);

    // See base class for documentation.
    void write_archive_end(void);

    // See base class for docuemntation.
    size_t get_maximum_name_length(void) const;

private:
    /**
      * The deeper instance variable is used to remember where the
      * filtered output is to be sent.
      */
    tar_output::pointer deeper;

    /**
      * The default constructor.  Do not use.
      */
    tar_output_filter();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    tar_output_filter(const tar_output_filter &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the assignment.
      */
    tar_output_filter &operator=(const tar_output_filter &rhs);
};

#endif // LIBTARDY_TAR_OUTPUT_FILTER_H
