//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_OUTPUT_EXTRACT_H
#define LIBTARDY_TAR_OUTPUT_EXTRACT_H

#include <libtardy/tar/output.h>

/**
  * The tar_output_extract class is used to represent the processing
  * required to extract the files form an archive.
  */
class tar_output_extract:
    public tar_output
{
public:
    typedef boost::shared_ptr<tar_output_extract> pointer;

    /**
      * The destructor.
      */
    virtual ~tar_output_extract();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      */
    static pointer create(void);

protected:
    // See base class for documentation.
    void write_header(const tar_header &hdr);

    // See base class for documentation.
    void write_header_padding(void);

    // See base class for documentation.
    void write_data(const void *data, int data_size);

    // See base class for documentation.
    void write_data_padding(void);

    // See base class for documentation.
    rcstring filename(void) const;

    // See base class for documentation.
    const char *get_format_name(void) const;

    // See base class for docuemntation.
    size_t get_maximum_name_length(void) const;

private:
    int fd;

    tar_header current_header;

    /**
      * The default constructor.
      * It is private on purpose, use a #create class method instead.
      */
    tar_output_extract();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_output_extract(const tar_output_extract &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_output_extract &operator=(const tar_output_extract &rhs);
};

#endif // LIBTARDY_TAR_OUTPUT_EXTRACT_H
