//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2002, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/stdio.h>

#include <libtardy/tar/output/cpio/newascii.h>


tar_output_cpio_newascii::~tar_output_cpio_newascii()
{
}


tar_output_cpio_newascii::tar_output_cpio_newascii(
    const file_output::pointer &a_fp
) :
    tar_output_cpio(a_fp, 4)
{
}


tar_output::pointer
tar_output_cpio_newascii::create(const file_output::pointer &a_fp)
{
    return pointer(new tar_output_cpio_newascii(a_fp));
}


void
tar_output_cpio_newascii::write_header(const tar_header &h)
{
    char buffer[112];
    snprintf
    (
        buffer,
        sizeof(buffer),
        "%6s%08lx%08lx%08lx%08lx%08lx%08lx%08lx%08lx%08lx%08lx%08lx%08lx%08lx",
        "070701",
        h.inode_number,
        (long)calculate_mode(h),
        h.user_id,
        h.group_id,
        h.link_count,
        h.mtime,
        h.size,
        h.device_major,
        h.device_minor,
        h.rdevice_major,
        h.rdevice_minor,
        (long)h.name.size() + 1,
        (long)0 // no checksum, for this format
    );
    write_deeper(buffer, 110);
    write_deeper(h.name.c_str(), h.name.size() + 1);
}


const char *
tar_output_cpio_newascii::get_format_name(void)
    const
{
    return "cpio-new-ascii";
}


size_t
tar_output_cpio_newascii::get_maximum_name_length(void)
    const
{
    return ((size_t)1 << (3 * 8));
}
