//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_OUTPUT_CPIO_BINARY_H
#define LIBTARDY_TAR_OUTPUT_CPIO_BINARY_H

#include <libtardy/endian.h>
#include <libtardy/tar/output/cpio.h>

/**
  * The tar_output_cpio_binary class is used to represent writing to a
  * "old binary" formatted cpio(5) archive.
  */
class tar_output_cpio_binary:
    public tar_output_cpio
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_output_cpio_binary();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param ofp
      *     The file into which the archive is to be written.
      * @param endian
      *     The byte order to be used when writing binary values into
      *     the archive file.
      */
    static pointer create(const file_output::pointer &ofp, endian_t endian);

    /**
      * The create_le class method is used to create new dynamically
      * allocated instances of this class, in little-endian order.
      *
      * @param ofp
      *     The file into which the archive is to be written.
      */
    static pointer create_le(const file_output::pointer &ofp);

    /**
      * The create_be class method is used to create new dynamically
      * allocated instances of this class, in big-endian order.
      *
      * @param ofp
      *     The file into which the archive is to be written.
      */
    static pointer create_be(const file_output::pointer &ofp);

    /**
      * The create_native_endian class method is used to create new
      * dynamically allocated instances of this class, in native byte
      * order.
      *
      * @param ofp
      *     The file into which the archive is to be written.
      */
    static pointer create_native_endian(const file_output::pointer &ofp);

protected:
    // See base class for documentation.
    void write_header(const tar_header &hdr);

    // See base class for documentation.
    const char *get_format_name(void) const;

    // See base class for docuemntation.
    size_t get_maximum_name_length(void) const;

private:
    /**
      * The constructor.
      * It is private on purpose, use a #create class method instead.
      *
      * @param ofp
      *     The file into which the archive is to be written.
      * @param endian
      *     The byte order to be used when writing binary values into
      *     the archive file.
      */
    tar_output_cpio_binary(const file_output::pointer &ofp, endian_t endian);

    /**
      * The endian instance variable is used to remember the byte
      * ordering to be used in the output file.
      */
    endian_t endian;

    void set2(void *data, unsigned value) const;

    void set4(void *data, unsigned long value) const;

    /**
      * The default constructor.  Do not use.
      */
    tar_output_cpio_binary();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_output_cpio_binary(const tar_output_cpio_binary &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_output_cpio_binary &operator=(const tar_output_cpio_binary &rhs);
};

#endif // LIBTARDY_TAR_OUTPUT_CPIO_BINARY_H
