//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_OUTPUT_CPIO_CRC_H
#define LIBTARDY_TAR_OUTPUT_CPIO_CRC_H

#include <libtardy/rcstring/accumulator.h>
#include <libtardy/tar/output/cpio.h>

/**
  * The tar_output_cpio_newascii class is used to represent writing to a
  * "crc" formatted CPIO archive.
  *
  * This format includes a checksum in each file header.  The header
  * data ia cached, and the file data is also chached.  At the end of
  * each file, the archive header is written, with the correct checksum,
  * and then the file data.  This assumes enough memory is available.
  */
class tar_output_cpio_crc:
    public tar_output_cpio
{
public:
     /**
       * The destructor.
       */
     virtual ~tar_output_cpio_crc();

     /**
       * The create class method is sued to create new dynamically
       * allocated instances of the class.
       *
       * @param fp
       *     The file to be written.
       */
     static pointer create(const file_output::pointer &fp);

protected:
     // See base class for documentation.
     void write_header(const tar_header &);

     // See base class for documentation.
     void write_header_padding(void);

     // See base class for documentation.
     void write_data(const void *data, int data_size);

     // See base class for documentation.
     void write_data_padding(void);

     // See base class for documentation.
     const char *get_format_name(void) const;

    // See base class for docuemntation.
    size_t get_maximum_name_length(void) const;

private:
     /**
       * The constructor.
       * It is private on purpose, use the #create class method instead.
       *
       * @param fp
       *     The file to be written.
       */
     tar_output_cpio_crc(const file_output::pointer &fp);

     /**
       * The current_header instance variable is used to remember the
       * file meta-data for the file we will emit shortly.
       */
     tar_header current_header;

     /**
       * The current_data instance variable is used to remember the file
       * contents for the file we will emit shortly.
       */
     rcstring_accumulator current_data;

     /**
       * The running_checksum instance variable is used to remember the
       * file contents checksum for the file we will emit shortly.
       */
     unsigned long running_checksum;

     /**
       * The default constructor.  Do not use.
       */
     tar_output_cpio_crc();

     /**
       * The copy constructor.  Do not use.
       *
       * @param rhs
       *     The right hand side of the initialization.
       */
     tar_output_cpio_crc(const tar_output_cpio_crc &rhs);

     /**
       * The assignment operator.  Do not use.
       *
       * @param rhs
       *     The right hand side of the assignment.
       */
     tar_output_cpio_crc &operator = (const tar_output_cpio_crc &rhs);
};

#endif // LIBTARDY_TAR_OUTPUT_CPIO_CRC_H
