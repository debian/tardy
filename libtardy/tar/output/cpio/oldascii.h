//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_OUTPUT_CPIO_OLDASCII_H
#define LIBTARDY_TAR_OUTPUT_CPIO_OLDASCII_H

#include <libtardy/tar/output/cpio.h>

/**
  * The tar_output_cpio_oldascii class is used to represent writing to an
  * "old" ascii formatted CPIO archive.
  */
class tar_output_cpio_oldascii:
    public tar_output_cpio
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_output_cpio_oldascii();

    /**
      * The create class method is used to create new dynamically
      * allocated isntances of this class.
      *
      * @param fp
      *     The output file to be written.
      */
    static pointer create(const file_output::pointer &fp);

protected:
    // See base class for documentation.
    virtual void write_header(const tar_header &);

    // See base class for documentation.
    virtual void write_header_padding(void);

    // See base class for documentation.
    const char *get_format_name(void) const;

    // See base class for docuemntation.
    size_t get_maximum_name_length(void) const;

private:
    /**
      * The constructor.
      * It is private on purpose, use the #create class method instead.
      *
      * @param fp
      *     The output file to be written.
      */
    tar_output_cpio_oldascii(const file_output::pointer &fp);

public:
    /**
      * The default constructor.  Do not use.
      */
    tar_output_cpio_oldascii();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    tar_output_cpio_oldascii(const tar_output_cpio_oldascii &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the assignment.
      */
    tar_output_cpio_oldascii &operator = (const tar_output_cpio_oldascii &rhs);
};

#endif // LIBTARDY_TAR_OUTPUT_CPIO_OLDASCII_H
