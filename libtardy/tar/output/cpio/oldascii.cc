//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2002, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/stdio.h>

#include <libtardy/tar/output/cpio/oldascii.h>


tar_output_cpio_oldascii::~tar_output_cpio_oldascii()
{
}


tar_output_cpio_oldascii::tar_output_cpio_oldascii(
    const file_output::pointer &a_fp
) :
    tar_output_cpio(a_fp, 2)
{
}


tar_output::pointer
tar_output_cpio_oldascii::create(const file_output::pointer &a_fp)
{
    return pointer(new tar_output_cpio_oldascii(a_fp));
}


void
tar_output_cpio_oldascii::write_header(const tar_header &h)
{
    char buffer[78];
    int dev = ((h.device_major & 0xFF) << 8) | (h.device_minor & 0xFF);
    int rdev = ((h.rdevice_major & 0xFF) << 8) | (h.rdevice_minor & 0xFF);
    snprintf
    (
        buffer,
        sizeof(buffer),
        "%06o%06o%06lo%06lo%06lo%06lo%06lo%06o%011lo%06lo%011lo",
        070707,
        dev & 0777777,
        h.inode_number & 0x777777,
        (long)calculate_mode(h) & 07777,
        h.user_id & 0777777,
        h.group_id & 0777777,
        h.link_count & 0777777,
        rdev & 0777777,
        h.mtime & 07777777777,
        (long)h.name.size() + 1,
        h.size
    );
    write_deeper(buffer, 76);
    write_deeper(h.name.c_str(), h.name.size() + 1);
}


void
tar_output_cpio_oldascii::write_header_padding(void)
{
    // no header padding, only data padding
}


const char *
tar_output_cpio_oldascii::get_format_name(void)
    const
{
    return "cpio-old-ascii";
}


size_t
tar_output_cpio_oldascii::get_maximum_name_length(void)
    const
{
    return ((size_t)1 << (3 * 6));
}
