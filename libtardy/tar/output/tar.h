//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2004, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_OUTPUT_TAR_H
#define LIBTARDY_TAR_OUTPUT_TAR_H

#include <libtardy/file/output.h>
#include <libtardy/tar/header.h>
#include <libtardy/tar/output.h>

/**
  * The tar_output_tar abstract class reperesents a generic tar formatted
  * archive output, and the current state of the output.
  *
  * There are several tar formats, this class shall be further derived
  * to represent each kind.
  */
class tar_output_tar:
    public tar_output
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_output_tar();

protected:
    /**
      * The constructor.
      * For use by derived classes only.
      *
      * @param fp
      *     The file to be written to.
      */
    tar_output_tar(const file_output::pointer &fp);

    // See base class for documentation.
    virtual void write_data(const void *, int);

    // See base class for documentation.
    virtual void write_data_padding(void);

    // See base class for documentation.
    rcstring filename(void) const;

    // See base class for documentation.
    virtual void write_archive_end(void);

    // See base class for documentation.
    virtual void set_block_size(long nbytes);

private:
    /**
      * The fp instance variable is used to remember where to send the
      * output, usually the file to write the output to.
      */
    file_output::pointer fp;

    /**
      * The pos instance variable is used to remember the output file
      * position, this is used when calculating how much padding to
      * insert.
      */
    long pos;

    /**
      * The block_size instance variable is used to remember the size in
      * bytes of the output block size.  This is used for output padding
      * at the end of the archive.
      */
    long block_size;

    /**
      * The default constructor.  Do not use.
      */
    tar_output_tar();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    tar_output_tar(const tar_output_tar &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the assignment.
      */
    tar_output_tar &operator = (const tar_output_tar &rhs);
};

#endif // LIBTARDY_TAR_OUTPUT_TAR_H
