//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_OUTPUT_AR_BSD_H
#define LIBTARDY_TAR_OUTPUT_AR_BSD_H

#include <libtardy/tar/output/ar.h>

/**
  * The tar_output_ar_bsd class is used to represent the processing
  * required to write an ar(1) archive, in BSD format.
  */
class tar_output_ar_bsd:
    public tar_output_ar
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_output_ar_bsd();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * It uses the "/nnn" long file name method.
      *
      * @param ofp
      *     The file to be written to.
      */
    static pointer create(const file_output::pointer &ofp);

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * It uses the "#1/nnn" long file name method.
      *
      * @param ofp
      *     The file to be written to.
      */
    static pointer create_l2(const file_output::pointer &ofp);

protected:
    // See base class for docuemntation.
    void write_archive_begin();

    // See base class for docuemntation.
    void write_header(const tar_header &hdr);

    // See base class for docuemntation.
    const char *get_format_name(void) const;

    // See base class for docuemntation.
    size_t get_maximum_name_length(void) const;

private:
    /**
      * The constructor.
      * It is private on purpose, use a #create class method instead.
      *
      * @param ofp
      *     The file to be written to.
      */
    tar_output_ar_bsd(const file_output::pointer &ofp);

    /**
      * The default constructor.  Do not use.
      */
    tar_output_ar_bsd();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_output_ar_bsd(const tar_output_ar_bsd &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_output_ar_bsd &operator=(const tar_output_ar_bsd &rhs);
};

#endif // LIBTARDY_TAR_OUTPUT_AR_BSD_H
