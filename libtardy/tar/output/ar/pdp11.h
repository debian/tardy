//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_OUTPUT_AR_PDP11_H
#define LIBTARDY_TAR_OUTPUT_AR_PDP11_H

#include <libtardy/endian.h>
#include <libtardy/tar/output/ar.h>

/**
  * The tar_output_ar_pdp11 class is used to represent the processing
  * required to write an ar(1) archive, in the (probably) oldest PDP11
  * Unix format.
  */
class tar_output_ar_pdp11:
    public tar_output_ar
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_output_ar_pdp11();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param ofp
      *     Where to write the archive data.
      * @param endian
      *     The byte order to be used.
      */
    static pointer create(const file_output::pointer &ofp, endian_t endian);

    /**
      * The create_be class method is used to create new dynamically
      * allocated instances of this class, using big-endian byte order.
      *
      * @param ofp
      *     Where to write the archive data.
      */
    static pointer create_be(const file_output::pointer &ofp);

    /**
      * The create_le class method is used to create new dynamically allocated
      * instances of this class, using little-endian byte order.  The PDP11 was
      * a little endian machine, this is the authentic byte order.
      *
      * @param ofp
      *     Where to write the archive data.
      */
    static pointer create_le(const file_output::pointer &ofp);

protected:
    // See base class for documentation.
    void write_archive_begin(void);

    // See base class for documentation.
    void write_header(const tar_header &hdr);

    // See base class for documentation.
    const char *get_format_name(void) const;

    // See base class for docuemntation.
    size_t get_maximum_name_length(void) const;

private:
    /**
      * The constructor.
      * It is private on purpose, use a #create class method instead.
      *
      * @param ofp
      *     Where to write the archive data.
      * @param endian
      *     The byte order to be used.
      */
    tar_output_ar_pdp11(const file_output::pointer &ofp, endian_t endian);

    /**
      * The endian instance variable is used to remember the byte order
      * to be used.
      */
    endian_t endian;

    /**
      * The put2 method is used to insert a two byte value into a data
      * buffer.
      *
      * @param data
      *     where to insert the two bytes
      * @param value
      *     the value to be inserted
      */
    void put2(unsigned char *data, unsigned value) const;

    /**
      * The put4 method is used to insert a four byte value into a data
      * buffer.
      *
      * @param data
      *     where to insert the two bytes
      * @param value
      *     the value to be inserted
      */
    void put4(unsigned char *data, unsigned long value) const;

    /**
      * The default constructor.  Do not use.
      */
    tar_output_ar_pdp11();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_output_ar_pdp11(const tar_output_ar_pdp11 &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_output_ar_pdp11 &operator=(const tar_output_ar_pdp11 &rhs);
};

#endif // LIBTARDY_TAR_OUTPUT_AR_PDP11_H
