//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2003, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/string.h>

#include <libtardy/tar/format.h>
#include <libtardy/tar/output/tar/bsd.h>


tar_output_tar_bsd::~tar_output_tar_bsd()
{
}


tar_output_tar_bsd::tar_output_tar_bsd(const file_output::pointer &a_fp) :
    tar_output_tar(a_fp)
{
}


tar_output::pointer
tar_output_tar_bsd::create(const file_output::pointer &a_fp)
{
    return pointer(new tar_output_tar_bsd(a_fp));
}


void
tar_output_tar_bsd::write_header(const tar_header &h)
{
    char block[TBLOCK];
    memset(block, 0, sizeof(block));
    // NOTE: no magic number
    header_ty *hp = (header_ty *)block;

    if (h.name.size() < 1)
        fatal("filename \"%s\" too short", h.name.c_str());
    if (h.name.size() > sizeof(hp->name))
    {
        fatal
        (
            "filename \"%s\" too long (by %d)",
            h.name.c_str(),
            (int)(h.name.size() - sizeof(hp->name))
        );
    }
    hp->name_set(h.name);

    hp->mode_set(h.mode);
    hp->uid_set(h.user_id);
    hp->gid_set(h.group_id);
    hp->size_set(h.size);
    hp->mtime_set(h.mtime);

    hp->linkflag_set(LF_NORMAL);
    switch (h.type)
    {
    case tar_header::type_normal:
        hp->linkflag_set(LF_NORMAL);
        break;

    case tar_header::type_normal_contiguous:
        hp->linkflag_set(LF_CONTIG);
        break;

    case tar_header::type_directory:
        hp->linkflag_set(LF_DIR);
        hp->name_set(h.name + "/");
        break;

    case tar_header::type_link_hard:
        hp->linkflag_set(LF_LINK);
        break;

    case tar_header::type_link_symbolic:
        hp->linkflag_set(LF_SYMLINK);
        break;

    case tar_header::type_fifo:
        hp->linkflag_set(LF_FIFO);
        break;

    case tar_header::type_device_block:
        fatal
        (
            "\"%s\" block special devices not supported by this format",
            h.name.c_str()
        );
        break;

    case tar_header::type_device_character:
        fatal
        (
            "\"%s\" character special devices not supported by this format",
            h.name.c_str()
        );
        break;

    case tar_header::type_socket:
        fatal
        (
            "\"%s\" named sockets not supported by this format",
            h.name.c_str()
        );
        break;

    case tar_header::type_normal_gzipped:
        fatal
        (
            "\"%s\" gzipped files not supported by this format",
            h.name.c_str()
        );
        break;
    }

    if
    (
        h.type == tar_header::type_link_hard
    ||
        h.type == tar_header::type_link_symbolic
    )
    {
        if (h.linkname.size() < 1)
        {
            fatal("linkname \"%s\" too short", h.linkname.c_str());
        }
        if (h.linkname.size() > sizeof(hp->linkname))
        {
            fatal
            (
                "linkname \"%s\" too long (by %d)",
                h.linkname.c_str(),
                (int)(h.linkname.size() - sizeof(hp->linkname))
            );
        }
        hp->linkname_set(h.linkname);
    }

    hp->chksum_set(hp->calculate_checksum());

    write_data(block, sizeof(block));
}


const char *
tar_output_tar_bsd::get_format_name(void)
    const
{
    return "tar-bsd";
}


size_t
tar_output_tar_bsd::get_maximum_name_length(void)
    const
{
    return NAMSIZ;
}
