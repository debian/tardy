//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
// Copyright (C) 2011 Thomas <metaf4@users.askja.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/string.h>
#include <libexplain/output.h>

#include <libtardy/tar/format.h>
#include <libtardy/tar/output/tar/ustar.h>


tar_output_tar_ustar::~tar_output_tar_ustar()
{
}


tar_output_tar_ustar::tar_output_tar_ustar(const file_output::pointer &a_fp) :
    tar_output_tar(a_fp)
{
}


tar_output::pointer
tar_output_tar_ustar::create(const file_output::pointer &a_fp)
{
    return pointer(new tar_output_tar_ustar(a_fp));
}


void
tar_output_tar_ustar::write_header(const tar_header &h)
{
    char block[TBLOCK];
    header_ty *hp = (header_ty *)block;

    if (h.name.size() < 1)
        fatal("filename \"%s\" too short", h.name.c_str());

    if (h.name[h.name.size() - 1] == '/')
    {
        explain_output_error_and_die
        (
            "bug (%s:%d) name \"%s\" has slash",
            __FILE__,
            __LINE__,
            h.name.c_str()
        );
    }

    rcstring name = h.name;
    if (h.type == tar_header::type_directory)
        name = h.name + "/";

    if (h.type == tar_header::type_normal_gzipped)
    {
        //
        // Write a bogus header block, indicating that the following
        // data is compressed.
        //
        memset(block, 0, sizeof(block));

        // this hack makes joerg schillings star think
        // it's create by it own, in fact it's only important
        // for long links and long names a.k.a. L/K header
        memcpy(block + 508, "tar", 3);

        memcpy(hp->magic, USTAR_MAGIC, sizeof(hp->magic));
        hp->name_set("././@Gzipped");
        hp->mode_set_z(0);
        hp->uid_set_z(0);
        hp->uname_set("root");
        hp->gid_set_z(0);
        hp->gname_set("root");
        hp->size_set_z(0);
        hp->mtime_set_z(0);
        hp->linkflag_set(LF_GZIPPED);
        hp->chksum_set_z(hp->calculate_checksum());
        write_data(block, sizeof(block));
    }
    if (name.size() + 1 > sizeof(hp->name))
    {
        //
        // Write a bogus header block, indicating that the following
        // data is a long file name.
        //
        memset(block, 0, sizeof(block));

        // this hack makes joerg schillings star think
        // it's create by it own, in fact it's only important
        // for long links and long names a.k.a. L/K header
        memcpy(block + 508, "tar", 3);

        memcpy(hp->magic, USTAR_MAGIC, sizeof(hp->magic));
        hp->name_set("././@LongLink");
        hp->mode_set_z(0);
        hp->uid_set_z(0);
        hp->uname_set("root");
        hp->gid_set_z(0);
        hp->gname_set("root");
        hp->size_set_z(name.size() + 1);
        hp->mtime_set_z(0);
        hp->linkflag_set(LF_LONGNAME);
        hp->chksum_set_z(hp->calculate_checksum());
        write_data(block, sizeof(block));

        //
        // Write a bogus data block with the file name in it.
        //
        write_data(name.c_str(), name.size() + 1);
        write_data_padding();
    }
    if
    (
        (
            h.type == tar_header::type_link_hard
        ||
            h.type == tar_header::type_link_symbolic
        )
    &&
        h.linkname.size() + 1 > sizeof(hp->linkname)
    )
    {
        //
        // Write a bogus header block, indicating that the following
        // data is a long file name.
        //
        memset(block, 0, sizeof(block));

        // this hack makes joerg schillings star think
        // it's create by it own, in fact it's only important
        // for long links and long names a.k.a. L/K header
        memcpy(block + 508, "tar", 3);

        memcpy(hp->magic, USTAR_MAGIC, sizeof(hp->magic));
        hp->name_set("././@LongLink");
        hp->mode_set_z(0);
        hp->uid_set_z(0);
        hp->uname_set("root");
        hp->gid_set_z(0);
        hp->gname_set("root");
        hp->size_set_z(name.size() + 1);
        hp->mtime_set_z(0);
        hp->linkflag_set(LF_LONGLINK);
        hp->chksum_set_z(hp->calculate_checksum());
        write_data(block, sizeof(block));

        //
        // Write a bogus data block with the file name in it.
        //
        write_data(h.linkname.c_str(), h.linkname.size() + 1);
        write_data_padding();
    }

    //
    // Write the file header.
    //
    memset(block, 0, sizeof(block));

    // This hack makes joerg schillings star think
    // it's create by it own, in fact it's only important
    // for long links and long names a.k.a. L/K header
    memcpy(block + 508, "tar", 3);

    memcpy(hp->magic, USTAR_MAGIC, sizeof(hp->magic));
    hp->name_set(name);

    hp->mode_set_z(h.mode);
    hp->uid_set_z(h.user_id);
    hp->gid_set_z(h.group_id);
    hp->size_set_z(h.size);
    hp->mtime_set_z(h.mtime);
    hp->uname_set(h.user_name);
    hp->gname_set(h.group_name);

    hp->linkflag_set(LF_NORMAL);
    switch (h.type)
    {
    case tar_header::type_normal:
    case tar_header::type_normal_gzipped:
        // already set
        break;

    case tar_header::type_normal_contiguous:
        hp->linkflag_set(LF_CONTIG);
        break;

    case tar_header::type_directory:
        hp->linkflag_set(LF_DIR);
        break;

    case tar_header::type_link_hard:
        hp->linkflag_set(LF_LINK);
        break;

    case tar_header::type_link_symbolic:
        hp->linkflag_set(LF_SYMLINK);
        break;

    case tar_header::type_fifo:
        hp->linkflag_set(LF_FIFO);
        break;

    case tar_header::type_device_block:
        hp->linkflag_set(LF_BLK);
        break;

    case tar_header::type_device_character:
        hp->linkflag_set(LF_CHR);
        break;

    case tar_header::type_socket:
        fatal
        (
            "\"%s\" named sockets not supported by this format",
            h.name.c_str()
        );
        break;
    }

    if
    (
        h.type == tar_header::type_link_hard
    ||
        h.type == tar_header::type_link_symbolic
    )
    {
        if (h.linkname.size() < 1)
        {
            fatal("linkname \"%s\" too short", h.linkname.c_str());
        }
        hp->linkname_set(h.linkname);
    }

    if
    (
        h.type == tar_header::type_device_block
    ||
        h.type == tar_header::type_device_character
    )
    {
        hp->devmajor_set_z(h.device_major);
        hp->devminor_set_z(h.device_minor);
    }

    hp->chksum_set_z(hp->calculate_checksum());

    write_data(block, sizeof(block));
}


const char *
tar_output_tar_ustar::get_format_name(void)
    const
{
    return "ustar";
}


size_t
tar_output_tar_ustar::get_maximum_name_length(void)
    const
{
    return NAMSIZ;
}
