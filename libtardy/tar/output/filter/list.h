//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_OUTPUT_FILTER_LIST_H
#define LIBTARDY_TAR_OUTPUT_FILTER_LIST_H

#include <libtardy/tar/output/filter.h>

/**
  * The tar_output_filter_list class is used to print a running
  * commentary about file being written to the output archive.
  * The file data and meta-data is left unchanged.
  */
class tar_output_filter_list:
    public tar_output_filter
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_output_filter_list();

    /**
      * The create class method is sued to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *     The actual archive to be written to.
      */
    static pointer create(const tar_output::pointer &deeper);

protected:
    // See base class for documentation.
    virtual void write_header(const tar_header &hdr);

private:
    /**
      * The constructor.
      * It is private on purpose, use the #create class method instead.
      *
      * @param deeper
      *     The actual archive to be written to.
      */
    tar_output_filter_list(const tar_output::pointer &deeper);

    /**
      * The default constructor.  Do not use.
      */
    tar_output_filter_list();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    tar_output_filter_list(const tar_output_filter_list &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the assignment.
      */
    tar_output_filter_list &operator = (const tar_output_filter_list &rhs);
};

#endif // LIBTARDY_TAR_OUTPUT_FILTER_LIST_H
