//
// tardy - a tar post-processor
// Copyright (C) 2003, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_OUTPUT_FILTER_GZIP_H
#define LIBTARDY_TAR_OUTPUT_FILTER_GZIP_H

#include <libtardy/ac/zlib.h>
#include <libtardy/tar/output/filter.h>

/**
  * The tar_output_filter_gzip class is used to represent an output
  * stream which compresses (via gzip) the contents of file data.
  *
  * By compressing file data, rather than the whole archive, when tape
  * blocks go bad, you can still get at the file data for later files.
  * If you compress the whole archive, you lose all files after the
  * bad block.
  */
class tar_output_filter_gzip:
    public tar_output_filter
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_output_filter_gzip();

    /**
      * The create class method is used to create new dynamically
      * allocated instance of this class.
      *
      * @param deeper
      *     The underlying archive to be written to.
      */
    static pointer create(const tar_output::pointer &deeper);

protected:
    // See base class for documentation.
    void write_header(const tar_header &hdr);

    // See base class for documentation.
    void write_header_padding(void);

    // See base class for documentation.
    void write_data(const void *data, int data_size);

    // See base class for documentation.
    void write_data_padding(void);

private:
    /**
      * The constructor.
      *
      * @param deeper
      *     The underlying archive to be written to.
      */
    tar_output_filter_gzip(const tar_output::pointer &deeper);

    /**
      * The pass_through instance variable is used to remember whether
      * we are using raw data (true) or compressing the data (false).
      */
    bool pass_through;

    /**
      * The stream instance variable is used to remember the internal
      * state of the zlib library.
      */
    z_stream stream;

    /**
      * The outbuf instance variable is used to remember data to be sent
      * to the compressed stream.
      */
    Byte *outbuf;

    /**
      * The crc instance variable is used to remember the running CRC
      * of the uncompressed data.
      */
    uLong crc;

    /**
      * The hdr instance variable is used to remember original file
      * header.  This is needed after the file has been compressed to
      * send the modified headers.
      */
    tar_header hdr;

    /**
      * The temp_fp instance variable is used to remember the file
      * pointer for the temporary file.
      */
    void *temp_fp;

    /**
      * The temp_filename instance variable is used to remember the name
      * of the temporary file.
      */
    rcstring temp_filename;

    /**
      * The drop_dead method is used to report fatal errors from zlib.
      */
    void drop_dead(int err);

    /**
      * The default constructor.  Do not use.
      */
    tar_output_filter_gzip();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    tar_output_filter_gzip(const tar_output_filter_gzip &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    tar_output_filter_gzip &operator=(const tar_output_filter_gzip &rhs);
};

#endif // LIBTARDY_TAR_OUTPUT_FILTER_GZIP_H
