//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/tar/output/filter/basename.h>


tar_output_filter_basename::~tar_output_filter_basename()
{
}


tar_output_filter_basename::tar_output_filter_basename(
    const pointer &a_deeper
) :
    tar_output_filter(a_deeper)
{
}


tar_output_filter_basename::pointer
tar_output_filter_basename::create(const pointer &a_deeper)
{
    return pointer(new tar_output_filter_basename(a_deeper));
}


void
tar_output_filter_basename::write_header(const tar_header &hdr)
{
    tar_header h2 = hdr;
    h2.name = h2.name.basename();
    if (h2.name.empty())
        h2.name = ".";
    tar_output_filter::write_header(h2);
}
