//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2002, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/stdio.h>

#include <libtardy/tar/output/filter/list.h>


tar_output_filter_list::~tar_output_filter_list()
{
}


tar_output_filter_list::tar_output_filter_list(
    const tar_output::pointer &a_deeper
) :
    tar_output_filter(a_deeper)
{
}


tar_output::pointer
tar_output_filter_list::create(const tar_output::pointer &a_deeper)
{
    return pointer(new tar_output_filter_list(a_deeper));
}


void
tar_output_filter_list::write_header(const tar_header &h)
{
    rcstring name = h.name;
    if (h.type == tar_header::type_directory)
        name += "/";
    fprintf
    (
        stderr,
        "%04lo %3ld %3ld %5ld %s\n",
        h.mode,
        h.user_id,
        h.group_id,
        h.size,
        name.c_str()
    );
    tar_output_filter::write_header(h);
}
