//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_OUTPUT_FILTER_AR_LONG_NAMES_H
#define LIBTARDY_TAR_OUTPUT_FILTER_AR_LONG_NAMES_H

#include <libtardy/config.h>
#include <list>

#include <libtardy/rcstring/accumulator.h>
#include <libtardy/tar/output/filter.h>

/**
  * The tar_output_filter_ar_long_names class is used to represent
  * writing an ar(10 archive, using the first form of long name mapping,
  * the use of an initial index file.
  *
  * Because the index file has to be written first, we are going to need
  * to spools the entire contents, and then write the index file, and
  * then write the archive contents.
  */
class tar_output_filter_ar_long_names:
    public tar_output_filter
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_output_filter_ar_long_names();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *     The underlying archive to be written to.
      */
    static pointer create(const pointer &deeper);

protected:
    // See base class for documentation.
    void write_header(const tar_header &hdr);

    // See base class for documentation.
    void write_header_padding(void);

    // See base class for documentation.
    void write_data(const void *data, int data_size);

    // See base class for documentation.
    void write_data_padding(void);

    // See base class for documentation.
    void write_archive_end(void);

    // See base class for documentation.
    size_t get_maximum_name_length(void) const;

private:
    /**
      * The default constructor.
      * It is private on purpose, use a #create class method instead.
      *
      * @param deeper
      *     The underlying archive to be written to.
      */
    tar_output_filter_ar_long_names(const pointer &deeper);

    typedef std::list<std::pair<tar_header, rcstring_accumulator> > contents_t;

    /**
      * The contents instance variable is used to remember each
      * individual archive member, because we can't write the first
      * archive member (the name map) until we have seen all of the
      * archive members.
      */
    contents_t contents;

    /**
      * The current_header instance variable is used to remember the
      * meta-data for the archive member being processed at the moment.
      */
    tar_header current_header;

    /**
      * The current_data instance variable is used to remember the file
      * data for the archive member being processed at the moment.
      */
    rcstring_accumulator current_data;

    /**
      * The default constructor.  Do not use.
      */
    tar_output_filter_ar_long_names();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_output_filter_ar_long_names(const tar_output_filter_ar_long_names &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_output_filter_ar_long_names &operator=(
        const tar_output_filter_ar_long_names &rhs);
};

#endif // LIBTARDY_TAR_OUTPUT_FILTER_AR_LONG_NAMES_H
