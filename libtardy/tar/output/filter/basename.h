//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_OUTPUT_FILTER_BASENAME_H
#define LIBTARDY_TAR_OUTPUT_FILTER_BASENAME_H

#include <libtardy/tar/output/filter.h>

/**
  * The tar_output_filter_basename class is used to represent
  * the removal of all but the final path conponent from file names.
  *
  * This is used by the ar(1) classes, as they do not store directory
  * components in their names (almost always they have very little room
  * for file names).
  */
class tar_output_filter_basename:
    public tar_output_filter
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_output_filter_basename();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *     The underlying archive to be written to.
      */
    static pointer create(const pointer &deeper);

protected:
    // See base class for documentation.
    void write_header(const tar_header &);

private:
    /**
      * The default constructor.
      * It is private on purpose, use a #create class method instead.
      *
      * @param deeper
      *     The underlying archive to be written to.
      */
    tar_output_filter_basename(const pointer &deeper);

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_output_filter_basename(const tar_output_filter_basename &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_output_filter_basename &operator=(
        const tar_output_filter_basename &rhs);
};

#endif // LIBTARDY_TAR_OUTPUT_FILTER_BASENAME_H
