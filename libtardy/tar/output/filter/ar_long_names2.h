//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_OUTPUT_FILTER_AR_LONG_NAMES2_H
#define LIBTARDY_TAR_OUTPUT_FILTER_AR_LONG_NAMES2_H

#include <libtardy/tar/output/filter.h>

/**
  * The tar_output_filter_ar_long_names2 class is used to represent
  * a filter for adding long archive member names, by inserting the
  * name in the start of the data.  You can recognise this format byte
  * "#1/nnn" file names.
  *
  * The advantage of this method over the other (see the
  * #tar_output_filter_ar_long_names class) is that you can do it as
  * the archive members stream past, and there is no need to build and
  * remember a name mapping pseudo-file.
  *
  *   "If any file name is more than 16 characters in length or contains an
  *   embedded space, the string "#1/" followed by the ASCII length of the
  *   name is written in the name field.  The file size (stored in the archive
  *   header) is incremented by the length of the name.  The name is then
  *   written immediately following the archive header."
  */
class tar_output_filter_ar_long_names2:
    public tar_output_filter
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_output_filter_ar_long_names2();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *     The underlying archive to be written to.
      */
    static pointer create(const pointer &deeper);

protected:
    // See base class for docuemntation.
    void write_header(const tar_header &hdr);

    // See base class for docuemntation.
    void write_header_padding(void);

    // See base class for docuemntation.
    size_t get_maximum_name_length(void) const;

private:
    /**
      * The default constructor.
      * It is private on purpose, use a #create class method instead.
      *
      * @param deeper
      *     The underlying archive to be written to.
      */
    tar_output_filter_ar_long_names2(const pointer &deeper);

    /**
      * The name_is_long instance variable is used to remember whether
      * or not a long name is being written into the archive.
      */
    bool name_is_long;

    /**
      * The name_cache instance variable is used to remember the long
      * name to be written into the archive (if #name_is_long is true).
      */
    rcstring name_cache;

    /**
      * The default constructor.  Do not use.
      */
    tar_output_filter_ar_long_names2();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_output_filter_ar_long_names2(
        const tar_output_filter_ar_long_names2 &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_output_filter_ar_long_names2 &operator=(
        const tar_output_filter_ar_long_names2 &rhs);
};

#endif // LIBTARDY_TAR_OUTPUT_FILTER_AR_LONG_NAMES2_H
