//
// tardy - a tar post-processor
// Copyright (C) 2003, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/tar/output/filter.h>


tar_output_filter::~tar_output_filter()
{
}


tar_output_filter::tar_output_filter(const tar_output::pointer &a_deeper) :
    deeper(a_deeper)
{
}


void
tar_output_filter::write_header(const tar_header &arg)
{
    deeper->write_header(arg);
}


void
tar_output_filter::write_header_padding(void)
{
    deeper->write_header_padding();
}


void
tar_output_filter::write_data(const void *arg1, int arg2)
{
    deeper->write_data(arg1, arg2);
}


void
tar_output_filter::write_data_padding(void)
{
    deeper->write_data_padding();
}


rcstring
tar_output_filter::filename(void)
    const
{
    return deeper->filename();
}


void
tar_output_filter::write_archive_begin(void)
{
    deeper->write_archive_begin();
}


void
tar_output_filter::write_archive_end(void)
{
    deeper->write_archive_end();
}


const char *
tar_output_filter::get_format_name(void)
    const
{
    return deeper->get_format_name();
}


size_t
tar_output_filter::get_maximum_name_length(void)
    const
{
    return deeper->get_maximum_name_length();
}
