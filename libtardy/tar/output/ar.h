//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_OUTPUT_AR_H
#define LIBTARDY_TAR_OUTPUT_AR_H

#include <libtardy/file/output.h>
#include <libtardy/tar/output.h>

/**
  * The tar_output_ar base class is used to represent the processing
  * required to write an ar(1) archive.  There are several of these,
  * each will derive from this base class.
  *
  * The bdf/archive.c file in the sources of the GNU Binutils package is
  * most instructive.
  */
class tar_output_ar:
    public tar_output
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_output_ar();

protected:
    /**
      * The constructor.
      * For use by derived classes only.
      *
      * @param ofp
      *     The file to be written to.
      * @param padding
      *     The byte multiple for padding.  Zero means none required.
      */
    tar_output_ar(const file_output::pointer &ofp, int padding);

    // See base class for documentation.
    void write_header_padding(void);

    // See base class for documentation.
    void write_data(const void *data, int data_size);

    // See base class for documentation.
    void write_data_padding(void);

    // See base class for documentation.
    rcstring filename(void) const;

    /**
      * The write_deeper method is used by derived classes to write data
      * to the output file.
      */
    void write_deeper(const void *data, size_t data_size);

    /**
      * The dec method is used to insert an ascii representation of a
      * numeric value, in decimal, into a data field.  It will be padded
      * with spaces on the right, if necessary.
      *
      * @param data
      *     Pointer to the base of an array to be filled with the number.
      * @param value
      *     The value to be inserted.
      * @param data_size
      *     The size of the field, in bytes.
      */
    static void dec(char *data, unsigned long value, size_t data_size);

    /**
      * The oct method is used to insert an ascii representation of a
      * numeric value, in octal, into a data field.  It will be padded
      * with spaces on the right, if necessary.
      *
      * @param data
      *     Pointer to the base of an array to be filled with the number.
      * @param value
      *     The value to be inserted.
      * @param data_size
      *     The size of the field, in bytes.
      */
    static void oct(char *data, unsigned long value, size_t data_size);

private:
    /**
      * The ofp instance variable isused to remember the binary file in
      * which to write the archive contents.
      */
    file_output::pointer ofp;

    /**
      * The padding instance variable is used to remember the byte
      * multiple that file headers and file data are to start on.  Zero
      * means no alignment required.
      */
    unsigned padding;

    /**
      * The position instance variable is used to remember the output
      * file position, this is used when calculating how much padding to
      * insert.
      */
    unsigned long position;

    /**
      * The write_padding method may be used to write zero-valued bytes
      * upto the next padding boundary.
      */
    void write_padding(void);

    /**
      * The default constructor.  Do not use.
      */
    tar_output_ar();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_output_ar(const tar_output_ar &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_output_ar &operator=(const tar_output_ar &rhs);
};

#endif // LIBTARDY_TAR_OUTPUT_AR_H
