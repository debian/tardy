//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_INPUT_CPIO_BINARY_H
#define LIBTARDY_TAR_INPUT_CPIO_BINARY_H

#include <libtardy/endian.h>
#include <libtardy/tar/input/cpio.h>

/**
  * The tar_input_cpio_binary class is used to represent the processing
  * required to read and parse an archive in the old binary cpio(5) format.
  */
class tar_input_cpio_binary:
    public tar_input_cpio
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_input_cpio_binary();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param ifp
      *     The file to be read and parsed.
      */
    static pointer create(const file_input::pointer &ifp);

    /**
      * The candidate class method is used to sniff at an input file
      * (without moving the file position) to see if it looks like a
      * file in the old binary cpio(5) format.
      *
      * @param ifp
      *     The input file to read and verify.
      */
    static bool candidate(const file_input::pointer &ifp);

protected:
    // See base class for documentation.
    bool read_header(tar_header &hdr);

    // See base class for documentation.
    const char *get_format_name(void) const;

    // See base class for documentation.
    tar_output::pointer tar_output_factory(const file_output::pointer &ofp)
        const;

    // See base class for documentation.
    size_t get_maximum_name_length(void) const;

private:
    /**
      * The constructor.
      * It is private on purpose, use a #create class method instead.
      *
      * @param ifp
      *     The file to be read and parsed.
      */
    tar_input_cpio_binary(const file_input::pointer &ifp);

    /**
      * The endian instance variable is used to remember the byte
      * ordering used in the input file.
      */
    endian_t endian;

    unsigned get2(const void *data) const;

    unsigned long get4(const void *data) const;

    /**
      * The default constructor.  Do not use.
      */
    tar_input_cpio_binary();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_input_cpio_binary(const tar_input_cpio_binary &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_input_cpio_binary &operator=(const tar_input_cpio_binary &rhs);
};

#endif // LIBTARDY_TAR_INPUT_CPIO_BINARY_H
