//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/tar/input/cpio/binary.h>
#include <libtardy/tar/input/cpio/crc.h>
#include <libtardy/tar/input/cpio/new_ascii.h>
#include <libtardy/tar/input/cpio/old_ascii.h>


tar_input::pointer
tar_input_cpio::factory(const file_input::pointer &ifp)
{
    if (tar_input_cpio_binary::candidate(ifp))
        return tar_input_cpio_binary::create(ifp);
    if (tar_input_cpio_old_ascii::candidate(ifp))
        return tar_input_cpio_old_ascii::create(ifp);
    if (tar_input_cpio_crc::candidate(ifp))
        return tar_input_cpio_crc::create(ifp);
    return tar_input_cpio_new_ascii::create(ifp);
}


bool
tar_input_cpio::candidate(const file_input::pointer &ifp)
{
    return
        (
            tar_input_cpio_binary::candidate(ifp)
        ||
            tar_input_cpio_old_ascii::candidate(ifp)
        ||
            tar_input_cpio_crc::candidate(ifp)
        ||
            tar_input_cpio_new_ascii::candidate(ifp)
        );
}
