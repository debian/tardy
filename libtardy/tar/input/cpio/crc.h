//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_INPUT_CPIO_CRC_H
#define LIBTARDY_TAR_INPUT_CPIO_CRC_H

#include <libtardy/tar/input/cpio.h>

/**
  * The tar_input_cpio_crc class is used to represent the processing
  * required to read and parse a "crc" format cpio(5) archive.
  *
  * @note
  *     This format is misnamed, and this is acknowledged in the cpio(5)
  *     documentation.  It uses a simple checksum, not a CRC.
  */
class tar_input_cpio_crc:
    public tar_input_cpio
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_input_cpio_crc();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param ifp
      *     The input file to read and parse into archive members.
      */
    static pointer create(const file_input::pointer &ifp);

    /**
      * The candidate class method is used to sniff at an input file
      * (without moving the file position) to see if it looks like a
      * file in new-ascii cpio(1) format.
      *
      * @param ifp
      *     The input file to read and verify.
      */
    static bool candidate(const file_input::pointer &ifp);

protected:
    // See base class for documentation.
    bool read_header(tar_header &hdr);

    // See base class for documentation.
    int read_data(void *data, int data_size);

    // See base class for documentation.
    void read_data_padding(void);

    // See base class for documentation.
    tar_output::pointer tar_output_factory(const file_output::pointer &ofp)
        const;

    // See base class for documentation.
    const char *get_format_name(void) const;

private:
    /**
      * The constructor.
      * It is private on purpose, use a #create class method instead.
      *
      * @param ifp
      *     The input file to read and parse into archive members.
      */
    tar_input_cpio_crc(const file_input::pointer &ifp);

    unsigned long checksum_expected;

    unsigned long running_checksum;

    /**
      * The default constructor.  Do not use.
      */
    tar_input_cpio_crc();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_input_cpio_crc(const tar_input_cpio_crc &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_input_cpio_crc &operator=(const tar_input_cpio_crc &rhs);

    /**
      * The cpio_header class is used to represent the set of
      * information that may be found in a New ASCII Format cpio(1) file
      * header.  This is typically overlayed (type punned, aliased) onto
      * a "unsigned char []".
      *
      * <b>DO NOT</b> add any virtual method to this class, or it will
      * cease to be useful for overlaying.
      */
    class cpio_header
    {
    public:
        char magic[6];
        rcstring get_magic(void) const;

        char ino[8];
        unsigned long ino_get(void) const;

        char mode[8];
        unsigned long mode_get(void) const;

        char uid[8];
        unsigned long uid_get(void) const;

        char gid[8];
        unsigned long gid_get(void) const;

        char nlink[8];
        unsigned long nlink_get(void) const;

        char mtime[8];
        unsigned long mtime_get(void) const;

        char filesize[8];
        unsigned long filesize_get(void) const;

        char devmajor[8];
        unsigned long devmajor_get(void) const;

        char devminor[8];
        unsigned long devminor_get(void) const;

        char rdevmajor[8];
        unsigned long rdevmajor_get(void) const;

        char rdevminor[8];
        unsigned long rdevminor_get(void) const;

        char namesize[8];
        unsigned long namesize_get(void) const;

        char check[8];
        unsigned long check_get(void) const;

    private:
        static unsigned long hex(const char *data, size_t data_size);
    };
};

#endif // LIBTARDY_TAR_INPUT_CPIO_CRC_H
