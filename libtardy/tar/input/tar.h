//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2002, 2003, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_INPUT_TAR_H
#define LIBTARDY_TAR_INPUT_TAR_H

#include <libtardy/file/input.h>
#include <libtardy/tar/input.h>


/**
  * The tar_input_tar class is used to represent the process of parsing
  * an input file, in tar(1) format, into a series of archive contents.
  */
class tar_input_tar:
    public tar_input
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_input_tar();

    /**
      * The factory class method is used to create new dynamically
      * allocated instances of this class.
      *
      * The file provided is non-destructively examined to determine
      * which tar format is actually present, and then the appropriate
      * class' create method is called.
      *
      * @param fp
      *     The file to be read.
      */
    static pointer factory(const file_input::pointer &fp);

protected:
    /**
      * The constructor.
      * For use by derived classes only.
      *
      * @param fp
      *     The file to be read.
      */
    tar_input_tar(const file_input::pointer &fp);

    // See base class for documentation.
    virtual tar_output::pointer tar_output_factory(
        const file_output::pointer &ofp) const = 0;

    // See base class for documentation.
    virtual int read_data(void *data, int data_size);

    // See base class for documentation.
    virtual void read_data_padding(void);

    // See base class for documentation.
    virtual bool read_header(tar_header &hdr);

    // See base class for documentation.
    virtual void read_header_padding(void);

    // See base class for documentation.
    rcstring filename(void) const;

    // See base class for documentation.
    format_family_t get_format_family(void) const;

private:
    /**
      * The read_data_strict method is used to read in data from the
      * input file, insisting on exactly the right number of bytes.
      * Short reads not acceptable.
      */
    int read_data_strict(void *data, int data_size);

    /**
      * The fp instance variable is used to remember where we are
      * getting out input from.
      */
    file_input::pointer fp;

    /**
      * The state instance variable is used to remember
      * where we are up to in processing a single file.
      *     0 = haven't read a header yet
      *     1 = have read the header (read header padding next)
      *     2 = have read the header padding (read data next)
      *     3 = have read some data
      */
    int state;

    /**
      * The check_state method is used to make sure we are in the
      * correct state for the method being called.
      */
    void check_state(int, int = -1);

    /**
      * The read_header_longname method is used to read a long
      * file name (out of file data) and then read the next header,
      * replacing the file name with the one read.
      */
    bool read_header_longname(int, tar_header &);

    /**
      * The read_header_longlink method is used to read a long
      * link name (out of file data) and then read the next header,
      * replacing the link name with the one read.
      */
    bool read_header_longlink(int, tar_header &);

    /**
      * The read_header_gzipped method is used to read a gzip flag
      * header.  The unzipped name in in the file data.  Then read
      * the next header, replacing the file name with the one read,
      * and set the gzipped flag.
      */
    bool read_header_gzipped(int, tar_header &);

    /**
      * The default constructor.  Do not use.
      */
    tar_input_tar();

    /**
      * The copy constructor.  Do not use.
      */
    tar_input_tar(const tar_input_tar &);

    /**
      * The assignment operator.  Do not use.
      */
    tar_input_tar &operator=(const tar_input_tar &);
};

#endif // LIBTARDY_TAR_INPUT_TAR_H
