//
// tardy - a tar post-processor
// Copyright (C) 2002, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_INPUT_FILENAMELIST_H
#define LIBTARDY_TAR_INPUT_FILENAMELIST_H

#include <libtardy/file/input.h>
#include <libtardy/filenamelist.h>
#include <libtardy/tar/input.h>

/**
  * The tar_input_filenamelist class is used to represent a file name
  * list as a source of tar input.
  */
class tar_input_filenamelist:
    public tar_input
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_input_filenamelist();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param fnl
      *     The list of file names to be archived.
      */
    static pointer create(const file_input::pointer &fnl);

    /**
      * The issue_progress_reports class method is used to request
      * (usually from the command line) that progress reports be issused
      * from time to time.
      *
      * @note
      *     The issue_progress_reports class method must be called
      *     before the #create class method, or it will have no effect.
      */
    static void issue_progress_reports(void);

protected:
    // See base class for documentation.
    int read_data(void *data, int data_size);

    // See base class for documentation.
    bool read_header(tar_header &hdr);

    // See base class for documentation.
    rcstring filename(void) const;

    // See base class for documentation.
    const char *get_format_name(void) const;

    // see base class for documentation
    format_family_t get_format_family(void) const;

    // See base class for docuemntation.
    size_t get_maximum_name_length(void) const;

private:
    /**
      * The constructor.
      * It is private on purpose, use the #create class method instead.
      *
      * @param fnl
      *     The list of file names to be archived.
      */
    tar_input_filenamelist(const filenamelist::pointer &fnl);

    /**
      * The default constructor.  Do not use.
      */
    tar_input_filenamelist();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    tar_input_filenamelist(const tar_input_filenamelist &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the assignment.
      */
    tar_input_filenamelist &operator=(const tar_input_filenamelist &rhs);

    /**
      * The source instance variable is used to remember where we
      * are getting out file names from.
      */
    filenamelist::pointer source;

    /**
      * The singleton instance variable is used to remember the
      * deeper input source.
      */
    mutable tar_input::pointer singleton;

    /**
      * The progress_flag class variable is used to remember whether or
      * not to include progress messages as the file list is traversed.
      */
    static bool progress_flag;

    /**
      * The get_singleton method is used to instantiate the
      * singleton it it is null, by reading the next file name from
      * the file name list and opening it.
      */
    tar_input::pointer get_singleton(void) const;
};

#endif // LIBTARDY_TAR_INPUT_FILENAMELIST_H
