//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/tar/input/ar.h>


tar_input_ar::~tar_input_ar()
{
}


tar_input_ar::tar_input_ar(const file_input::pointer &a_ifp, int a_padding) :
    ifp(a_ifp),
    padding(a_padding >= 2 ? a_padding : 0)
{
    assert(ifp);
}


format_family_t
tar_input_ar::get_format_family(void)
    const
{
    return format_family_ar;
}


rcstring
tar_input_ar::filename(void)
    const
{
    return ifp->filename();
}


int
tar_input_ar::read_data(void *data, int data_size)
{
    assert(data_size >= 0);
    return read_deeper(data, data_size);
}


void
tar_input_ar::read_header_padding(void)
{
    read_padding();
}


size_t
tar_input_ar::read_deeper(void *data, size_t data_size)
{
    return ifp->read(data, data_size);
}


void
tar_input_ar::read_data_padding(void)
{
    read_padding();
}


void
tar_input_ar::read_padding(void)
{
    if (padding <= 0)
        return;
    unsigned n = ifp->get_position() % padding;
    if (!n)
        return;
    ifp->skip(padding - n);
}
