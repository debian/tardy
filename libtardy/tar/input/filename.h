//
// tardy - a tar post-processor
// Copyright (C) 2002, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_INPUT_FILENAME_H
#define LIBTARDY_TAR_INPUT_FILENAME_H

#include <libtardy/file/input.h>
#include <libtardy/tar/input.h>

/**
  * The tar_input_filename class is used to represent tar input from a
  * single file.
  */
class tar_input_filename:
    public tar_input
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_input_filename();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param fn
      *     The name of the file.
      */
    static pointer create(const rcstring &fn);

protected:
    // see base class for documentation
    int read_data(void *data, int data_size);

    // see base class for documentation
    bool read_header(tar_header &hdr);

    // see base class for documentation
    rcstring filename(void) const;

    // see base class for documentation
    const char *get_format_name(void) const;

    // see base class for documentation
    format_family_t get_format_family(void) const;

    // See base class for docuemntation.
    size_t get_maximum_name_length(void) const;

private:
    /**
      * The constructor.
      * It is private on purpose, use the #create class method instead.
      *
      * @param fn
      *     The name of the file.
      */
    tar_input_filename(const rcstring &fn);

    /**
      * The name instance variable is used to remember the name
      * of the file being read.
      */
    rcstring name;

    /**
      * The length instance variable is used to remember how many
      * bytes of input are to be read from the file.
      */
    int length;

    /**
      * The source instance variable is used to remember the input
      * file state (position, file descriptor, etc).
      */
    file_input::pointer source;

    /**
      * The default constructor.
      */
    tar_input_filename();

    /**
      * The copy constructor.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    tar_input_filename(const tar_input_filename &rhs);

    /**
      * The assignment operator.
      *
      * @param rhs
      *     The right hand side of the assignment.
      */
    tar_input_filename &operator=(const tar_input_filename &rhs);
};

#endif // LIBTARDY_TAR_INPUT_FILENAME_H
