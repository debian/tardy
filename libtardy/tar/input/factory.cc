//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/tar/input/ar.h>
#include <libtardy/tar/input/cpio.h>
#include <libtardy/tar/input/tar.h>


tar_input::pointer
tar_input::factory(const file_input::pointer &ifp)
{
    if (tar_input_ar::candidate(ifp))
        return tar_input_ar::factory(ifp);
    if (tar_input_cpio::candidate(ifp))
        return tar_input_cpio::factory(ifp);
    return tar_input_tar::factory(ifp);
}
