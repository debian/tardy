//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/string.h>

#include <libtardy/tar/format.h>
#include <libtardy/tar/input/tar/bsd.h>
#include <libtardy/tar/output/tar/bsd.h>


tar_input_tar_bsd::~tar_input_tar_bsd()
{
}


tar_input_tar_bsd::tar_input_tar_bsd(const file_input::pointer &a_ifp) :
    tar_input_tar(a_ifp)
{
}


tar_input_tar_bsd::pointer
tar_input_tar_bsd::create(const file_input::pointer &a_ifp)
{
    return pointer(new tar_input_tar_bsd(a_ifp));
}


bool
tar_input_tar_bsd::candidate(const file_input::pointer &ifp)
{
    char buf[512];
    int nbytes = ifp->peek(buf, sizeof(buf));
    if (nbytes != sizeof(buf))
        return false;
    header_ty *hp = (header_ty *)buf;

    //
    // We may have to get smarter than this: the way to distinguish
    // V7 tarballs from BSD tarballs, is that BSD uses LF_NORMAL for
    // regular files while V7 uses LF_OLDNORMAL.
    //
    // But that may require skipping a few directories (or other no-data
    // files) first.
    //
    // BSD tarballs don't have user names or group names, either.
    //
    return (hp->magic[0] == '\0');
}


tar_output::pointer
tar_input_tar_bsd::tar_output_factory(const file_output::pointer &ofp)
    const
{
    return tar_output_tar_bsd::create(ofp);
}


const char *
tar_input_tar_bsd::get_format_name(void)
    const
{
    return "tar-bsd";
}
