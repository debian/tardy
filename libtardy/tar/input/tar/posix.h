//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_INPUT_TAR_POSIX_H
#define LIBTARDY_TAR_INPUT_TAR_POSIX_H

#include <libtardy/tar/input/tar.h>

/**
  * The tar_input_tar_posix class is used to represent the process of parsing
  * an input file, in POSIX tar(1) format, into a series of archive contents.
  */
class tar_input_tar_posix:
    public tar_input_tar
{
public:
    typedef boost::shared_ptr<tar_input_tar_posix> pointer;

    /**
      * The destructor.
      */
    virtual ~tar_input_tar_posix();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param ifp
      *     The file to read for the binary archive contents.
      */
    static pointer create(const file_input::pointer &ifp);

protected:
    // See base class for documentation.
    tar_output::pointer tar_output_factory(const file_output::pointer &ofp)
        const;

     // See base class for documentation.
     const char *get_format_name(void) const;

private:
    /**
      * The constructor.
      * It is private on purpose, use a #create class method instead.
      *
      * @param ifp
      *     The file to read for the binary archive contents.
      */
    tar_input_tar_posix(const file_input::pointer &ifp);

    /**
      * The default constructor.  Do not use.
      */
    tar_input_tar_posix();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_input_tar_posix(const tar_input_tar_posix &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_input_tar_posix &operator=(const tar_input_tar_posix &rhs);
};

#endif // LIBTARDY_TAR_INPUT_TAR_POSIX_H
