//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/string.h>

#include <libtardy/tar/input/tar/ustar.h>
#include <libtardy/tar/output/tar/ustar.h>


tar_input_tar_ustar::~tar_input_tar_ustar()
{
}


tar_input_tar_ustar::tar_input_tar_ustar(const file_input::pointer &a_ifp) :
    tar_input_tar(a_ifp)
{
}


tar_input_tar_ustar::pointer
tar_input_tar_ustar::create(const file_input::pointer &a_ifp)
{
    return pointer(new tar_input_tar_ustar(a_ifp));
}


bool
tar_input_tar_ustar::candidate(const file_input::pointer &ifp)
{
    char buf[512];
    int nbytes = ifp->peek(buf, sizeof(buf));
    if (nbytes != sizeof(buf))
        return false;
    return (0 == memcmp(buf + 508, "tar", 3));
}


tar_output::pointer
tar_input_tar_ustar::tar_output_factory(const file_output::pointer &ofp)
    const
{
    return tar_output_tar_ustar::create(ofp);
}


const char *
tar_input_tar_ustar::get_format_name(void)
    const
{
    return "ustar";
}
