//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_INPUT_FILTER_H
#define LIBTARDY_TAR_INPUT_FILTER_H

#include <libtardy/tar/input.h>

/**
  * The tar_input_filter class is used to represent the state of an
  * input filter.  It remembers the origin of the input, and implements
  * transparent methods by default.  Specific filters will want to
  * override some or all of these defaults.
  */
class tar_input_filter:
    public tar_input
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_input_filter();

protected:
    /**
      * The constructor.
      * For use by derived classes only.
      *
      * @param deeper
      *     The input archive from which to read our input.
      */
    tar_input_filter(const tar_input::pointer &deeper);

    // See base class for documentation.
    void read_archive_begin(void);

    // See base class for documentation.
    virtual bool read_header(tar_header &hdr);

    // See base class for documentation.
    virtual void read_header_padding(void);

    // See base class for documentation.
    virtual int read_data(void *data, int data_size);

    // See base class for documentation.
    virtual void read_data_padding(void);

    // See base class for documentation.
    void read_archive_end(void);

    // See base class for documentation.
    rcstring filename(void) const;

    // See base class for documentation.
    const char *get_format_name(void) const;

    // See base class for documentation.
    format_family_t get_format_family(void) const;

   // See base class for documentation.
   size_t get_maximum_name_length(void) const;

private:
    /**
      * The deeper instance variable is used to remember the deeper
      * archive from which to read our input.
      */
    tar_input::pointer deeper;

    /**
      * Default constructor.  Do not use.
      */
    tar_input_filter();

    /**
      * Copy constructor.  Do not use.
      */
    tar_input_filter(const tar_input_filter &);

    /**
      * Assignment operator.  Do not use.
      */
    tar_input_filter &operator=(const tar_input_filter &);
};

#endif // LIBTARDY_TAR_INPUT_FILTER_H
