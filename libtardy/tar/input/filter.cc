//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2002, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/ctype.h>
#include <libtardy/ac/string.h>

#include <libtardy/tar/input/filter.h>


tar_input_filter::~tar_input_filter()
{
}


tar_input_filter::tar_input_filter(const tar_input::pointer &a_deeper) :
    deeper(a_deeper)
{
    assert(deeper);
}


void
tar_input_filter::read_archive_begin(void)
{
    deeper->read_archive_begin();
}


bool
tar_input_filter::read_header(tar_header &h)
{
    return deeper->read_header(h);
}


void
tar_input_filter::read_header_padding(void)
{
    deeper->read_header_padding();
}


int
tar_input_filter::read_data(void *data, int data_size)
{
    return deeper->read_data(data, data_size);
}


void
tar_input_filter::read_data_padding(void)
{
    deeper->read_data_padding();
}


void
tar_input_filter::read_archive_end(void)
{
    deeper->read_archive_end();
}


rcstring
tar_input_filter::filename(void)
    const
{
    return deeper->filename();
}


const char *
tar_input_filter::get_format_name(void)
    const
{
    return deeper->get_format_name();
}


format_family_t
tar_input_filter::get_format_family(void)
    const
{
    return deeper->get_format_family();
}


size_t
tar_input_filter::get_maximum_name_length(void)
    const
{
    return deeper->get_maximum_name_length();
}
