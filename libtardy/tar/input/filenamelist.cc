//
// tardy - a tar post-processor
// Copyright (C) 2002, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/sys/param.h>

#include <libtardy/filenamelist/file.h>
#include <libtardy/filenamelist/filter/progress.h>
#include <libtardy/tar/input/filename.h>
#include <libtardy/tar/input/filenamelist.h>
#include <libtardy/tar/input/filenamelist.h>


bool tar_input_filenamelist::progress_flag = false;


tar_input_filenamelist::~tar_input_filenamelist()
{
}


tar_input_filenamelist::tar_input_filenamelist(
    const filenamelist::pointer &a_source
) :
    source(a_source)
{
}


tar_input::pointer
tar_input_filenamelist::create(const file_input::pointer &fp)
{
    filenamelist::pointer fnl = filenamelist_file::create(fp);
    if (progress_flag)
        fnl = filenamelist_filter_progress::create(fnl);
    return pointer(new tar_input_filenamelist(fnl));
}


tar_input::pointer
tar_input_filenamelist::get_singleton(void)
    const
{
    if (!singleton)
    {
        rcstring line;
        if (!source->read_one_line(line))
            return pointer();
        singleton = tar_input_filename::create(line);
    }
    return singleton;
}


bool
tar_input_filenamelist::read_header(tar_header &result)
{
    if (singleton)
        singleton.reset();
    tar_input::pointer fp = get_singleton();
    return (fp ? fp->read_header(result) : false);
}


int
tar_input_filenamelist::read_data(void *buffer, int buflen)
{
    tar_input::pointer fp = get_singleton();
    if (!fp)
        return 0;
    return fp->read_data(buffer, buflen);
}


rcstring
tar_input_filenamelist::filename(void)
    const
{
    return source->filename();
}


void
tar_input_filenamelist::issue_progress_reports(void)
{
    progress_flag = true;
}


const char *
tar_input_filenamelist::get_format_name(void)
    const
{
    return "list";
}


format_family_t
tar_input_filenamelist::get_format_family(void)
    const
{
    return format_family_other;
}


size_t
tar_input_filenamelist::get_maximum_name_length(void)
    const
{
    return PATH_MAX;
}
