//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_INPUT_AR_V7_H
#define LIBTARDY_TAR_INPUT_AR_V7_H

#include <libtardy/endian.h>
#include <libtardy/tar/input/ar.h>

/**
  * The tar_input_ar_v7 class is used to represent the processing
  * required to read and parse an ar(1) archive in the ancient Unix
  * Version7 format.
  */
class tar_input_ar_v7:
    public tar_input_ar
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_input_ar_v7();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param ifp
      *     The file to be read and parsed.
      *     The byte order will be determined by sniffing at the file.
      */
    static pointer create(const file_input::pointer &ifp);

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param ifp
      *     The file to be read and parsed.
      * @param endian
      *     The byte order to be used.
      */
    static pointer create(const file_input::pointer &ifp, endian_t endian);

    /**
      * The create_be class method is used to create new dynamically
      * allocated instances of this class, using big-endian byte order.
      *
      * @param ifp
      *     The file to be read and parsed.
      */
    static pointer create_be(const file_input::pointer &ifp);

    /**
      * The create_le class method is used to create new dynamically
      * allocated instances of this class, using little-endian byte order.
      *
      * @param ifp
      *     The file to be read and parsed.
      */
    static pointer create_le(const file_input::pointer &ifp);

    /**
      * The candidate class method is used to sniff at an input file
      * (without moving the file position) to see if it looks like a
      * ar(1) archive, in V7 format.
      *
      * @param ifp
      *     The file to be examined.
      * @returns
      *     true if the #create class method could open it, false otherwise.
      */
    static bool candidate(const file_input::pointer &ifp);

protected:
    // See base class for documentation.
    void read_archive_begin(void);

    // See base class for documentation.
    bool read_header(tar_header &hdr);

    // See base class for documentation.
    const char *get_format_name(void) const;

    // See base class for documentation.
    tar_output::pointer tar_output_factory(const file_output::pointer &ofp)
        const;

    // See base class for documentation.
    size_t get_maximum_name_length(void) const;

private:
    /**
      * The constructor.
      * It is private on purpose, use a #create class method instead.
      *
      * @param ifp
      *     The file to be read and parsed.
      * @param endian
      *     The byte order to be used.  V7 Unix was developed on a
      *     PDP11, in little endian byte order.
      */
    tar_input_ar_v7(const file_input::pointer &ifp, endian_t endian);

private:
    /**
      * The default constructor.  Do not use.
      */
    tar_input_ar_v7();

    /**
      * The endian instance variable is used to remember the byte order
      * to use.
      */
    endian_t endian;

    /**
      * The get2 method is used to convert a byte array (length 2) into
      * an unsigned value, using the current byte ordering.
      *
      * @param data
      *     Pointer to the array of two bytes to be converted.
      */
    unsigned get2(unsigned char *data) const;

    /**
      * The get4 method is used to convert a byte array (length 4) into
      * an unsigned long value, using the current byte ordering.
      *
      * @param data
      *     Pointer to the array of four bytes to be converted.
      */
    unsigned long get4(unsigned char *data) const;

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_input_ar_v7(const tar_input_ar_v7 &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_input_ar_v7 &operator=(const tar_input_ar_v7 &rhs);
};

#endif // LIBTARDY_TAR_INPUT_AR_V7_H
