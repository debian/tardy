//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/tar/input/ar/bsd.h>
#include <libtardy/tar/input/ar/pdp11.h>
#include <libtardy/tar/input/ar/v7.h>


tar_input_ar::pointer
tar_input_ar::factory(const file_input::pointer &a_ifp)
{
    if (tar_input_ar_pdp11::candidate(a_ifp))
        return tar_input_ar_pdp11::create(a_ifp);
    if (tar_input_ar_v7::candidate(a_ifp))
        return tar_input_ar_v7::create(a_ifp);
    return tar_input_ar_bsd::create(a_ifp);
}


bool
tar_input_ar::candidate(const file_input::pointer &a_ifp)
{
    return
        (
            tar_input_ar_pdp11::candidate(a_ifp)
        ||
            tar_input_ar_v7::candidate(a_ifp)
        ||
            tar_input_ar_bsd::candidate(a_ifp)
        );
}
