//
// tardy - a tar post-processor
// Copyright (C) 2004, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_INPUT_FILTER_REMOVE_PREFIX_COUNT_H
#define LIBTARDY_TAR_INPUT_FILTER_REMOVE_PREFIX_COUNT_H

#include <libtardy/tar/input/filter.h>

/**
  * The tar_input_filter_remove_prefix_count class is used to represent
  * an input filter which remove leading path prefixes, specifying how
  * many path prefixes to be removed.
  */
class tar_input_filter_remove_prefix_count:
    public tar_input_filter
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_input_filter_remove_prefix_count();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *     The input source to be filtered.
      * @param count
      *     The number of leading directories to be removed.
      */
    static pointer create(const tar_input::pointer &deeper,
        int count);

protected:
    // See base class for documentation.
    bool read_header(tar_header &hdr);

private:
    /**
      * The constructor.
      * It is private on purpose, use the #create class method instead.
      *
      * @param deeper
      *     The input source to be filtered.
      * @param count
      *     The number of leading directories to be removed.
      */
    tar_input_filter_remove_prefix_count(const tar_input::pointer &deeper,
        int count);

    /**
      * The count instance variable is used to remember how many levels
      * of leading directories are to be removed.
      */
    int count;

    /**
      * The default constructor.  Do not use.
      */
    tar_input_filter_remove_prefix_count();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    tar_input_filter_remove_prefix_count(
        const tar_input_filter_remove_prefix_count &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the assignment.
      */
    tar_input_filter_remove_prefix_count &operator=(
        const tar_input_filter_remove_prefix_count &rhs);
};

#endif // LIBTARDY_TAR_INPUT_FILTER_REMOVE_PREFIX_COUNT_H
