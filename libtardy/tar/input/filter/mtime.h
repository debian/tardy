//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_INPUT_MTIME_H
#define LIBTARDY_TAR_INPUT_MTIME_H

#include <libtardy/tar/input/filter.h>

/**
  * The tar_input_filter_mtime class is used to represent a filter which
  * sets the file last modifiied time in tar archive file headers.
  */
class tar_input_filter_mtime:
    public tar_input_filter
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_input_filter_mtime();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *     The input stream to apply the filter to.
      * @param when
      *     The time to set the file, as if returned by the time(2)
      *     system call).
      */
    static pointer create(const tar_input::pointer &deeper, time_t when);

protected:
    // See base class for documentation.
    virtual bool read_header(tar_header &hdr);

private:
    /**
      * The constructor.
      * It is private on purpose, use the #create class method instead.
      *
      * @param deeper
      *     The input stream to apply the filter to.
      * @param when
      *     The time to set the file, as if returned by the time(2)
      *     system call).
      */
    tar_input_filter_mtime(const tar_input::pointer &deeper, time_t when);

    /**
      * The when instance variable is used to remember the last time
      * modified to be set in each file header in the tar archive.
      */
    time_t when;

    /**
      * The default constructor.  Do not use.
      */
    tar_input_filter_mtime();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    tar_input_filter_mtime(const tar_input_filter_mtime &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the assignment.
      */
    tar_input_filter_mtime &operator=(const tar_input_filter_mtime &rhs);
};

#endif // LIBTARDY_TAR_INPUT_MTIME_H
