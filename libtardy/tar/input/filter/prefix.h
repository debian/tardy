//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2002, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_INPUT_PREFIX_H
#define LIBTARDY_TAR_INPUT_PREFIX_H

#include <libtardy/rcstring.h>
#include <libtardy/tar/input/filter.h>

/**
  * The tar_input_filter_mode_clear class is used to represent a filter
  * which adds a directory prefix to each file name in tar archive
  * file headers.
  */
class tar_input_filter_prefix:
    public tar_input_filter
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_input_filter_prefix();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *     The input stream to apply the filter to.
      * @param prefix
      *     The directory prefix to be applied to file names.
      */
    static pointer create(const tar_input::pointer &deeper,
        const rcstring &prefix);

protected:
    // See base classfor documentation.
    virtual bool read_header(tar_header &hdr);

private:
    /**
      * The constructor.
      * It is private on purpose, use the #create class method instead.
      *
      * @param deeper
      *     The input stream to apply the filter to.
      * @param prefix
      *     The directory prefix to be applied to file names.
      */
    tar_input_filter_prefix(const tar_input::pointer &deeper,
        const rcstring &prefix);

    /**
      * The prefix instance variable is used to remember the directory
      * prefix to be applied to each file name in the tar archive.
      */
    rcstring prefix;

    /**
      * The default constructor.  Do not use.
      */
    tar_input_filter_prefix();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    tar_input_filter_prefix(const tar_input_filter_prefix &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the assignment.
      */
    tar_input_filter_prefix &operator=(const tar_input_filter_prefix &rhs);
};

#endif // LIBTARDY_TAR_INPUT_PREFIX_H
