//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_INPUT_FILTER_AR_LONG_NAMES_H
#define LIBTARDY_TAR_INPUT_FILTER_AR_LONG_NAMES_H

#include <libtardy/config.h>
#include <map>

#include <libtardy/tar/input/filter.h>

/**
  * The tar_input_filter_ar_long_names class is used to represent
  * filtering ar(5) archives with long name re-mapping.
  *
  * This applies to archives that use "/" or "//" for the lookup map,
  * and the "/[0-9]+" to request an indirection through that map.
  */
class tar_input_filter_ar_long_names:
    public tar_input_filter
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_input_filter_ar_long_names();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *     The input archive from which to read our input.
      */
    static pointer create(const pointer &deeper);

protected:
    // See base class for documentation.
    virtual bool read_header(tar_header &hdr);

    // See base class for documentation.
    virtual size_t get_maximum_name_length(void) const;

private:
    /**
      * The default constructor.
      * It is private on purpose, use a #create class method instead.
      *
      * @param deeper
      *     The input archive from which to read our input.
      */
    tar_input_filter_ar_long_names(const pointer &deeper);

    /**
      * The first instance variable is used to remember whether or not
      * the first file in the archive has been processed.  The first
      * file contains the name mapping, if a mapping is present.
      */
    bool first;

    typedef std::map<rcstring, rcstring> names_t;

    /**
      * The names instance variable is used to remember the name mapping
      * from the short archive name (first) toi the longer expanded file
      * name (second).
      */
    names_t names;

    /**
      * The read_and_process_name_map method is used to read the special
      * name map file at the start of the archive, and populate the
      * #names instance variable.
      *
      * @param map_file_size
      *     The size of the map file, in bytes.
      */
    void read_and_process_name_map(size_t map_file_size);

    /**
      * The default constructor.  Do not use.
      */
    tar_input_filter_ar_long_names();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_input_filter_ar_long_names(const tar_input_filter_ar_long_names &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_input_filter_ar_long_names &operator=(
        const tar_input_filter_ar_long_names &rhs);
};

#endif // LIBTARDY_TAR_INPUT_FILTER_AR_LONG_NAMES_H
