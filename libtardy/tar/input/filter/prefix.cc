//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2002, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/cannonical.h>
#include <libtardy/tar/input/filter/prefix.h>


tar_input_filter_prefix::~tar_input_filter_prefix()
{
}


tar_input_filter_prefix::tar_input_filter_prefix(
    const tar_input::pointer &a_deeper,
    const rcstring &a_prefix
) :
    tar_input_filter(a_deeper),
    prefix(a_prefix)
{
}


tar_input::pointer
tar_input_filter_prefix::create(const tar_input::pointer &a_deeper,
    const rcstring &a_prefix)
{
    return pointer(new tar_input_filter_prefix(a_deeper, a_prefix));
}


static rcstring
apply_prefix(const rcstring &prefix, const rcstring &name)
{
    return cannonicalize(prefix + "/" + name);
}


bool
tar_input_filter_prefix::read_header(tar_header &h)
{
    bool ok = tar_input_filter::read_header(h);
    if (ok)
    {
        h.name = apply_prefix(prefix, h.name);
        if (h.type == tar_header::type_link_hard)
            h.linkname = apply_prefix(prefix, h.linkname);
    }
    return ok;
}
