//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/time.h>

#include <libtardy/tar/input/filter/mtime.h>


tar_input_filter_mtime::~tar_input_filter_mtime()
{
}


tar_input_filter_mtime::tar_input_filter_mtime(
    const tar_input::pointer &a_deeper,
    time_t a_when
) :
    tar_input_filter(a_deeper),
    when(a_when)
{
}


tar_input::pointer
tar_input_filter_mtime::create(const tar_input::pointer &a_deeper,
    time_t a_when)
{
    return pointer(new tar_input_filter_mtime(a_deeper, a_when));
}


bool
tar_input_filter_mtime::read_header(tar_header &h)
{
    bool ok = tar_input_filter::read_header(h);
    if (ok)
        h.mtime = when;
    return ok;
}
