//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2003, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_INPUT_SUPPR_DIREC_H
#define LIBTARDY_TAR_INPUT_SUPPR_DIREC_H

#include <libtardy/tar/input/filter.h>

/**
  * The tar_input_filter_suppress_directories class is used to represent a
  * filter which removes records of directories from tar archive files.
  * All other type of files are passed through.  No file names are
  * altered in any way.
  */
class tar_input_filter_suppress_directories:
    public tar_input_filter
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_input_filter_suppress_directories();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *     The input tream to be filtered.
     */
    static pointer create(const tar_input::pointer &deeper);

protected:
    // See base class for documentation.
    virtual bool read_header(tar_header &hdr);

private:
    /**
      * The constructor.
      * It is private on purpose, use the #create class method instead.
      *
      * @param deeper
      *     The input tream to be filtered.
      */
    tar_input_filter_suppress_directories(const tar_input::pointer &deeper);

    /**
      * The default constructor.  Do not use.
      */
    tar_input_filter_suppress_directories();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    tar_input_filter_suppress_directories(
        const tar_input_filter_suppress_directories &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the assignment.
      */
    tar_input_filter_suppress_directories &operator=(
        const tar_input_filter_suppress_directories &rhs);
};

#endif // LIBTARDY_TAR_INPUT_SUPPR_DIREC_H
