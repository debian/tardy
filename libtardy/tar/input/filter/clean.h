//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_INPUT_FILTER_CLEAN_H
#define LIBTARDY_TAR_INPUT_FILTER_CLEAN_H

#include <libtardy/tar/input/filter.h>

/**
  * The tar_input_filter_clean class is used to represent a filter which
  * cleans filenames in tar archive headers.  You can clean out upper
  * case, lower case, shite space, shell meta characters and unprintable
  * characters.
  */
class tar_input_filter_clean:
    public tar_input_filter
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_input_filter_clean();

    enum
    {
        flag_down =  1 << 0,
        flag_print = 1 << 1,
        flag_shell = 1 << 2,
        flag_space = 1 << 3,
        flag_up =    1 << 4
    };

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *    The deeper input is the input to be filtered.
      * @param flags
      *    The flags indicate what is to be cleaned.  See the enum above
      *    for details.
      */
    static pointer create(const tar_input::pointer &deeper, int flags);

protected:
    // See base class for documentation.
    virtual bool read_header(tar_header &hdr);

private:
    /**
      * The constructor.
      * It is private on purpose, use the #create class method instead.
      *
      * @param deeper
      *    The deeper input is the input to be filtered.
      * @param flags
      *    The flags indicate what is to be cleaned.  See the enum above
      *    for details.
      */
    tar_input_filter_clean(const tar_input::pointer &deeper, int flags);

    /**
      * The flag instance variable is used to remember what to clean.
      */
    int flag;

    /**
      * The default constructor.  Do not use.
      */
    tar_input_filter_clean();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    tar_input_filter_clean(const tar_input_filter_clean &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the assignment.
      */
    tar_input_filter_clean &operator=(const tar_input_filter_clean &rhs);
};

#endif // LIBTARDY_TAR_INPUT_FILTER_CLEAN_H
