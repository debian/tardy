//
// tardy - a tar post-processor
// Copyright (C) 2004, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/assert.h>
#include <libtardy/ac/string.h>

#include <libtardy/cannonical.h>
#include <libtardy/tar/input/filter/remove_prefix_count.h>


tar_input_filter_remove_prefix_count::~tar_input_filter_remove_prefix_count()
{
}


tar_input_filter_remove_prefix_count::tar_input_filter_remove_prefix_count(
    const tar_input::pointer &a_deeper,
    int a_count
) :
    tar_input_filter(a_deeper),
    count(a_count)
{
}


tar_input::pointer
tar_input_filter_remove_prefix_count::create(const tar_input::pointer &a_deeper,
    int a_count)
{
    return pointer(new tar_input_filter_remove_prefix_count(a_deeper, a_count));
}


static rcstring
apply_remove_prefix_count(const rcstring &name, int count, bool isdir)
{
    const char *cp = name.c_str();
    while (count > 0)
    {
        const char *ep = strchr(cp, '/');
        if (!ep)
        {
            if (isdir)
                return ".";
            return (*cp ? cp : ".");
        }
        cp = ep + 1;
        --count;
    }
    return (*cp ? cp : ".");
}


bool
tar_input_filter_remove_prefix_count::read_header(tar_header &h)
{
    for (;;)
    {
        bool ok = tar_input_filter::read_header(h);
        if (!ok)
            return false;
        bool isdir = (h.type == tar_header::type_directory);
        h.name = apply_remove_prefix_count(h.name, count, isdir);
        if (isdir && h.name == ".")
            continue;
        if (h.type == tar_header::type_link_hard)
            h.linkname = apply_remove_prefix_count(h.linkname, count, false);
        return true;
    }
}
