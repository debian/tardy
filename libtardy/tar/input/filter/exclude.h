//
// tardy - a tar post-processor
// Copyright (C) 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_INPUT_FILTER_EXCLUDE_H
#define LIBTARDY_TAR_INPUT_FILTER_EXCLUDE_H

#include <libtardy/tar/input/filter.h>

/**
  * The tar_input_filter_exclude class is used to represent a filter
  * that removes file that match a glob pattern from the archive.
  */
class tar_input_filter_exclude:
    public tar_input_filter
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_input_filter_exclude();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *     The input source to be filtered.
      * @param glob
      *     The file name pattern to be matched.
      */
    static pointer create(const tar_input::pointer &deeper,
        const rcstring &glob);

protected:
    // See base class for documentation.
    virtual bool read_header(tar_header &hdr);

private:
    /**
      * The constructor.
      * It is private on purpose, use the #create class method instead.
      *
      * @param deeper
      *     The input source to be filtered.
      * @param glob
      *     The file name pattern to be matched.
      */
    tar_input_filter_exclude(const tar_input::pointer &deeper,
        const rcstring &glob);

    /**
      * The glob instance variable is used to remember the file name
      * pattern for the excluded files.
      */
    rcstring glob;

    /**
      * The default constructor.  Do not use.
      */
    tar_input_filter_exclude();

    /**
      * The copy constructor.  Do not use.
      */
    tar_input_filter_exclude(const tar_input_filter_exclude &);

    /**
      * The assignment operator.  Do not use.
      */
    tar_input_filter_exclude &operator=(const tar_input_filter_exclude &);
};

#endif // LIBTARDY_TAR_INPUT_FILTER_EXCLUDE_H
