//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2002, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/tar/input/filter/user_name.h>


tar_input_filter_user_name::~tar_input_filter_user_name()
{
}


tar_input_filter_user_name::tar_input_filter_user_name(
    const tar_input::pointer &a_deeper,
    const rcstring &a_name
) :
    tar_input_filter(a_deeper),
    name(a_name)
{
}


tar_input::pointer
tar_input_filter_user_name::create(const tar_input::pointer &a_deeper,
    const rcstring &a_name)
{
    return pointer(new tar_input_filter_user_name(a_deeper, a_name));
}


bool
tar_input_filter_user_name::read_header(tar_header &h)
{
    bool ok = tar_input_filter::read_header(h);
    if (ok)
        h.user_name = name;
    return ok;
}
