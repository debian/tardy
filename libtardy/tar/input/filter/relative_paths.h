//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_INPUT_FILTER_RELATIVE_PATHS_H
#define LIBTARDY_TAR_INPUT_FILTER_RELATIVE_PATHS_H

#include <libtardy/tar/input/filter.h>

/**
  * The tar_input_filter_relative_paths class is used to represent a
  * filter that re-writes file names in the archive if they are absolute
  * paths (if they start with a '/' slash character) to be relative paths.
  */
class tar_input_filter_relative_paths:
    public tar_input_filter
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_input_filter_relative_paths();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *     The archive to be filtered, the files we return are read
      *     from this file, and this modified (if necessary).
      */
    static pointer create(const tar_input::pointer &deeper);

protected:
     // See base class for documentation.
     bool read_header(tar_header &hdr);

private:
    /**
      * The constructor.
      * It is private on purpose, use a #create class method instead.
      *
      * @param deeper
      *     The archive to be filtered, the files we return are read
      *     from this file, and this modified (if necessary).
      */
    tar_input_filter_relative_paths(const tar_input::pointer &deeper);

    /**
      * The default constructor.  Do not use.
      */
    tar_input_filter_relative_paths();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_input_filter_relative_paths(const tar_input_filter_relative_paths &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_input_filter_relative_paths &operator=(
        const tar_input_filter_relative_paths &rhs);
};

#endif // LIBTARDY_TAR_INPUT_FILTER_RELATIVE_PATHS_H
