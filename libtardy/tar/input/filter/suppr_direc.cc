//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2002, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/assert.h>

#include <libtardy/tar/input/filter/suppr_direc.h>


tar_input_filter_suppress_directories::~tar_input_filter_suppress_directories()
{
}


tar_input_filter_suppress_directories::tar_input_filter_suppress_directories(
    const tar_input::pointer &a_deeper
) :
    tar_input_filter(a_deeper)
{
}


tar_input::pointer
tar_input_filter_suppress_directories::create(
    const tar_input::pointer &a_deeper)
{
    return pointer(new tar_input_filter_suppress_directories(a_deeper));
}


bool
tar_input_filter_suppress_directories::read_header(tar_header &h)
{
    for (;;)
    {
        if (!tar_input_filter::read_header(h))
            return false;
        if (h.type != tar_header::type_directory)
            return true;
        read_header_padding();
        //
        // We would need to skip the data, if directories *had*
        // data, but they don't.
        //
        assert(h.size == 0);
        read_data_padding();
    }
}
