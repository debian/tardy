//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/rcstring/list.h>
#include <libtardy/tar/input/filter/relative_paths.h>


tar_input_filter_relative_paths::~tar_input_filter_relative_paths()
{
}


tar_input_filter_relative_paths::tar_input_filter_relative_paths(
    const tar_input::pointer &a_deeper
) :
    tar_input_filter(a_deeper)
{
}


tar_input_filter_relative_paths::pointer
tar_input_filter_relative_paths::create(const tar_input::pointer &a_deeper)
{
    return pointer(new tar_input_filter_relative_paths(a_deeper));
}


static rcstring
clean(const rcstring &fn)
{
    rcstring_list components;
    components.split(fn, "/", false);
    rcstring_list result;
    for (size_t j = 0; j < components.size(); ++j)
    {
        rcstring component = components[j];
        if (component.empty())
            continue;
        if (component == ".")
            continue;
        if (component == ".." && !result.empty() && result.back() != "..")
        {
            result.pop_back();
            continue;
        }
        result.push_back(component);
    }
    if (result.empty())
        return ".";
    rcstring retval = result.unsplit("/");
    return retval;
}


bool
tar_input_filter_relative_paths::read_header(tar_header &hdr)
{
    if (!tar_input_filter::read_header(hdr))
        return false;
    hdr.name = clean(hdr.name);
    return true;
}
