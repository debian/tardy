//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_INPUT_MODE_SET_H
#define LIBTARDY_TAR_INPUT_MODE_SET_H

#include <libtardy/tar/input/filter.h>

/**
  * The tar_input_filter_mode_set class is used to represent a filter
  * which set bits in the file protection mode in tar archive file
  * headers.
  */
class tar_input_filter_mode_set:
    public tar_input_filter
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_input_filter_mode_set();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *     The input stream to apoply the filter to.
      * @param bits
      *     The bits to be set in the file mode.
      */
    static pointer create(const tar_input::pointer &deeper, long bits);

protected:
    // See base class for documentation.
    virtual bool read_header(tar_header &hdr);

private:
    /**
      * The constructor.
      * It is private on purpose, use the #create class method instead.
      *
      * @param deeper
      *     The input stream to apoply the filter to.
      * @param bits
      *     The bits to be set in the file mode.
      */
    tar_input_filter_mode_set(const tar_input::pointer &deeper, long bits);

    /**
      * The bits instance variable is used to remember which bits are
      * to be set in each file header in the tar archive.
      */
    long bits;

    /**
      * The default constructor.  Do not use.
      */
    tar_input_filter_mode_set();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialization.
      */
    tar_input_filter_mode_set(const tar_input_filter_mode_set &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the assignment.
      */
    tar_input_filter_mode_set &operator=(const tar_input_filter_mode_set &rhs);
};

#endif // LIBTARDY_TAR_INPUT_MODE_SET_H
