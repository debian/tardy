//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/tar/input/filter/mode_set.h>


tar_input_filter_mode_set::~tar_input_filter_mode_set()
{
}


tar_input_filter_mode_set::tar_input_filter_mode_set(
    const tar_input::pointer &a_deeper,
    long a_bits
) :
    tar_input_filter(a_deeper),
    bits(a_bits & 07777)
{
}


tar_input::pointer
tar_input_filter_mode_set::create(const tar_input::pointer &a_deeper,
    long a_bits)
{
    return pointer(new tar_input_filter_mode_set(a_deeper, a_bits));
}


bool
tar_input_filter_mode_set::read_header(tar_header &h)
{
    bool ok = tar_input_filter::read_header(h);
    if (ok)
    {
        if (h.mode & 0444)
            h.mode |= 0444;
        if (h.mode & 0111)
            h.mode |= 0111;
        h.mode |= bits;
        if (h.type == tar_header::type_link_symbolic)
            h.mode |= 0777;
    }
    return ok;
}
