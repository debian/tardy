//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2001, 2002, 2004, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/assert.h>
#include <libtardy/ac/string.h>

#include <libtardy/cannonical.h>
#include <libtardy/tar/input/filter/remov_prefi.h>


tar_input_filter_remove_prefix::~tar_input_filter_remove_prefix()
{
}


static rcstring
cannonicalize_plus_slash(const rcstring &arg)
{
    rcstring tmp = cannonicalize(arg);
    if (tmp.empty() || tmp[tmp.size() - 1] != '/')
        tmp += "/";
    return tmp;
}


tar_input_filter_remove_prefix::tar_input_filter_remove_prefix(
    const tar_input::pointer &a_deeper,
    const rcstring &a_prefix
) :
    tar_input_filter(a_deeper),
    prefix(cannonicalize_plus_slash(a_prefix))
{
}


tar_input::pointer
tar_input_filter_remove_prefix::create(const tar_input::pointer &a_deeper,
    const rcstring &a_prefix)
{
    return pointer(new tar_input_filter_remove_prefix(a_deeper, a_prefix));
}


static rcstring
remove_prefix_inner(const rcstring &prfx, const rcstring &name)
{
    //
    // The name and the prefix have both been cannonicalized at this
    // point (i.e. no extra slashes, no extra dot dirs).  The prefix
    // is also guaranteed to have a '/' on the end.
    //
    int len_p = prfx.size();
    assert(len_p >= 1);
    assert(prfx[len_p - 1] == '/');

    //
    // if the prefix matches the name exactly,
    // replace it with "./"
    //
    // Note that the prefix has been canonicalized to have a slash, and
    // the name as been canonicalized NOT to have a slash.
    //
    int len_n = name.size();
    if
    (
        len_p == len_n + 1
    &&
        0 == memcmp(prfx.c_str(), name.c_str(), len_n)
    )
    {
        return ".";
    }

    //
    // if the prefix is longer than the name,
    // it can't be a prefix
    //
    if (len_p > len_n)
        return name;

    //
    // if it is a leading prefix,
    // remove it from the name
    //
    if (0 == memcmp(prfx.c_str(), name.c_str(), len_p))
        return rcstring(name.c_str() + len_p, len_n - len_p);

    //
    // prefix does not match the start of the name
    //
    return name;
}


static rcstring
apply_remove_prefix(const rcstring &prefix, const rcstring &name)
{
    return remove_prefix_inner(prefix, cannonicalize(name));
}


bool
tar_input_filter_remove_prefix::read_header(tar_header &h)
{
    bool ok = tar_input_filter::read_header(h);
    if (ok)
    {
        h.name = apply_remove_prefix(prefix, h.name);
        if (h.type == tar_header::type_link_hard)
            h.linkname = apply_remove_prefix(prefix, h.linkname);
    }
    return ok;
}
