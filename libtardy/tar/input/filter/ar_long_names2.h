//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_TAR_INPUT_FILTER_AR_LONG_NAMES2_H
#define LIBTARDY_TAR_INPUT_FILTER_AR_LONG_NAMES2_H

#include <libtardy/tar/input/filter.h>

/**
  * The tar_input_filter_ar_long_names2 class is used to represent the
  * second form of long file name support in ar(1) archives.  In this
  * form, filenames take the form of "#1/nnn" wher ennn is the length of
  * the filename, and the actual filename is inserted into the start of
  * the data (and the data size increased to match).
  */
class tar_input_filter_ar_long_names2:
    public tar_input_filter
{
public:
    /**
      * The destructor.
      */
    virtual ~tar_input_filter_ar_long_names2();

    /**
      * The create class method is used to create new dynamically
      * allocated instances of this class.
      *
      * @param deeper
      *     The input archive from which to read our input.
      */
    static pointer create(const pointer &deeper);

protected:
    // See base class for documentation.
    bool read_header(tar_header &hdr);

    // See base class for documentation.
    void read_header_padding(void);

    // See base class for documentation.
    size_t get_maximum_name_length(void) const;

private:
    /**
      * The default constructor.
      * It is private on purpose, use a #create class method instead.
      *
      * @param deeper
      *     The input archive from which to read our input.
      */
    tar_input_filter_ar_long_names2(const pointer &deeper);

    /**
      * The do_header_padding instance variable is used to remember
      * whether or not the read_header_padding method should be a no-op.
      * It will be a no-op when the read_header method has already done
      * it, for access to the start of the file data, containing the
      * file name.
      */
    bool do_header_padding;

    /**
      * The default constructor.  Do not use.
      */
    tar_input_filter_ar_long_names2();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_input_filter_ar_long_names2(const tar_input_filter_ar_long_names2 &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    tar_input_filter_ar_long_names2 &operator=(
        const tar_input_filter_ar_long_names2 &rhs);
};

#endif // LIBTARDY_TAR_INPUT_FILTER_AR_LONG_NAMES2_H
