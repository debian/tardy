//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2002, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/stdarg.h>
#include <libtardy/ac/stdio.h>
#include <libexplain/output.h>

#include <libtardy/tar/input.h>


tar_input::tar_input()
{
}


tar_input::~tar_input()
{
}


void
tar_input::fatal(const char *fmt, ...)
    const
{
    va_list ap;
    va_start(ap, fmt);
    char buffer[2000];
    vsnprintf(buffer, sizeof(buffer), fmt, ap);
    va_end(ap);
    explain_output_error_and_die
    (
        "%s: %s {%s}",
        filename().c_str(),
        buffer,
        get_format_name()
    );
}


void
tar_input::warning(const char *fmt, ...)
    const
{
    va_list ap;
    va_start(ap, fmt);
    char buffer[2000];
    vsnprintf(buffer, sizeof(buffer), fmt, ap);
    va_end(ap);
    explain_output_error
    (
        "%s: warning: %s {%s}",
        filename().c_str(),
        buffer,
        get_format_name()
    );
}


void
tar_input::read_data_padding(void)
{
    // default is none
}


void
tar_input::read_header_padding(void)
{
    read_data_padding();
}


tar_header::type_ty
tar_input::type_from_mode(int mode)
{
    //
    // Don't use the native UNIX defintions, because
    // (a) this may not be UNIX, and
    // (b) the vendor may have gratuitously changed things.
    //
    switch (mode >> 12)
    {
    case 014:
        return tar_header::type_socket;

    case 012:
        return tar_header::type_link_symbolic;

    case 010:
        return tar_header::type_normal;

    case 006:
        return tar_header::type_device_block;

    case 004:
        return tar_header::type_directory;

    case 002:
        return tar_header::type_device_character;

    case 001:
        return tar_header::type_fifo;

    default:
        return tar_header::type_normal;
    }
}


void
tar_input::read_archive_begin(void)
{
}


void
tar_input::read_archive_end(void)
{
}


size_t
tar_input::get_maximum_name_length(void)
    const
{
    return 0;
}
