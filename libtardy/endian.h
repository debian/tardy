//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef LIBTARDY_ENDIAN_H
#define LIBTARDY_ENDIAN_H

/**
  * The endian_t type is used to represent a byte ordering.
  */
enum endian_t
{
    /**
      * Least significant byte first.
      */
    endian_little,

    /**
      * Most significant byte first.
      */
    endian_big
};

/**
  * The endian_name function may be used to obtain a human-readable name
  * from a byte ordering value.
  */
const char *endian_name(endian_t value);

/**
  * The endian_native function i sused to obtain the byte order of the
  * host executing this program.
  */
endian_t endian_native(void);

/**
  * The endian_get2 function is used to decode two bytes of binary into
  * a 16-bit value, using the byte order given.
  *
  * @param data
  *     Pointer to the base of an array of two bytes to be decoded.
  * @param endian
  *     The byte order to be used.
  */
unsigned endian_get2(const void *data, endian_t endian);

/**
  * The endian_get2le function is used to decode two bytes of binary
  * into a 16-bit value, using little-endian byte order.
  *
  * @param data
  *     Pointer to the base of an array of two bytes to be decoded.
  */
unsigned endian_get2le(const void *data);

/**
  * The endian_get2be function is used to decode two bytes of binary
  * into a 16-bit value, using big-endian byte order.
  *
  * @param data
  *     Pointer to the base of an array of two bytes to be decoded.
  */
unsigned endian_get2be(const void *data);

/**
  * The endian_set2 function is used to encode 16-bit value into two
  * bytes of binary, using the byte order given.
  *
  * @param data
  *     Pointer to the base of an array of two bytes to be encoded.
  * @param value
  *     The value to be encoded.
  * @param endian
  *     The byte order to be used.
  */
void endian_set2(void *data, unsigned value, endian_t endian);

/**
  * The endian_set2le function is used to encode 16-bit value into two
  * bytes of binary, using the little-endian byte order.
  *
  * @param data
  *     Pointer to the base of an array of two bytes to be encoded.
  * @param value
  *     The value to be encoded.
  */
void endian_set2le(void *data, unsigned value);


/**
  * The endian_set2be function is used to encode 16-bit value into two
  * bytes of binary, using the big-endian byte order.
  *
  * @param data
  *     Pointer to the base of an array of two bytes to be encoded.
  * @param value
  *     The value to be encoded.
  */
void endian_set2be(void *data, unsigned value);

/**
  * The endian_get4 function is used to decode four bytes of binary into
  * a 32-bit value, using the byte order given.
  *
  * @param data
  *     Pointer to the base of an array of four bytes to be decoded.
  * @param endian
  *     The byte order to be used.
  */
unsigned long endian_get4(const void *data, endian_t endian);

/**
  * The endian_get4le function is used to decode four bytes of binary
  * into a 32-bit value, using little-endian byte order.
  *
  * @param data
  *     Pointer to the base of an array of four bytes to be decoded.
  */
unsigned long endian_get4le(const void *data);

/**
  * The endian_get4be function is used to decode four bytes of binary
  * into a 32-bit value, using big-endian byte order.
  *
  * @param data
  *     Pointer to the base of an array of four bytes to be decoded.
  */
unsigned long endian_get4be(const void *data);

/**
  * The endian_set4 function is used to encode 32-bit value into four
  * bytes of binary, using the byte order given.
  *
  * @param data
  *     Pointer to the base of an array of four bytes to be encoded.
  * @param value
  *     The value to be encoded.
  * @param endian
  *     The byte order to be used.
  */
void endian_set4(void *data, unsigned long value, endian_t endian);

/**
  * The endian_set4le function is used to encode 32-bit value into four
  * bytes of binary, using little-endian byte order.
  *
  * @param data
  *     Pointer to the base of an array of four bytes to be encoded.
  * @param value
  *     The value to be encoded.
  */
void endian_set4le(void *data, unsigned long value);

/**
  * The endian_set4le function is used to encode 32-bit value into four
  * bytes of binary, using big-endian byte order.
  *
  * @param data
  *     Pointer to the base of an array of four bytes to be encoded.
  * @param value
  *     The value to be encoded.
  */
void endian_set4be(void *data, unsigned long value);

#endif // LIBTARDY_ENDIAN_H
