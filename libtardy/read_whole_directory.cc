//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/dirent.h>
#include <libexplain/closedir.h>
#include <libexplain/opendir.h>
#include <libexplain/readdir.h>

#include <libtardy/read_whole_directory.h>


void
read_whole_directory(const rcstring &path, rcstring_list &result)
{
    DIR *dp = explain_opendir_or_die(path.c_str());
    for (;;)
    {
        struct dirent *dep = explain_readdir_or_die(dp);
        if (!dep)
            break;
        const char *name = dep->d_name;
        if
        (
            !(
                name[0] == '.'
            &&
                (name[1] == '\0' || (name[1] == '.' && name[2] == '\0'))
            )
        )
            result.push_back(name);
    }
    explain_closedir_or_die(dp);
}
