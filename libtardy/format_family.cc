//
// tardy - a tar post-processor
// Copyright (C) 2011, 2012 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/format_family.h>
#include <libtardy/file/input/gunzip.h>
#include <libtardy/file/input/bunzip2.h>


format_family_t
format_family_from_filename(const rcstring &filename)
{
    if (filename.empty() || filename == "standard output")
        return format_family_other;
    rcstring fn2 = filename.basename().downcase();
    fn2 = file_input_gunzip::remove_filename_suffix(fn2);
    fn2 = file_input_bunzip2::remove_filename_suffix(fn2);
    if (fn2.ends_with(".tar"))
        return format_family_tar;
    if (fn2.ends_with(".cpio"))
        return format_family_cpio;
    if (fn2.ends_with(".ar") || fn2.ends_with(".a") || fn2.ends_with(".deb"))
        return format_family_ar;
    return format_family_other;
}
// vim: set ts=8 sw=4 et :
