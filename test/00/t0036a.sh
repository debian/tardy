#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="write ar-bsd"
. test_prelude

sed 's|X$||' > test.ok << 'fubar'
00000000: 21 3C 61 72 63 68 3E 0A 61 62 63 2F 20 20 20 20  !<arch>.abc/    X
00000010: 20 20 20 20 20 20 20 20 38 36 34 30 30 20 20 20          86400   X
00000020: 20 20 20 20 30 20 20 20 20 20 30 20 20 20 20 20      0     0     X
00000030: 31 30 30 36 34 34 20 20 34 20 20 20 20 20 20 20  100644  4       X
00000040: 20 20 60 0A 61 62 63 0A                            `.abc.        X
fubar
if test $? -ne 0 ; then no_result; fi

mkdir junk
if test $? -ne 0 ; then no_result; fi
echo abc > junk/abc
if test $? -ne 0 ; then no_result; fi
tar cf junk.tar junk
if test $? -ne 0 ; then no_result; fi

# run the command
tardy -auto-test junk.tar -ofmt ar-bsd test.out -hexdump
if test $? -ne 0 ; then fail; fi

# make sure we get the expected results
diff test.ok test.out || fail

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
