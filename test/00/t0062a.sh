#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="write ar(1) long names, first kind"
. test_prelude

sed 's|$$||' > test.ok << 'fubar'
00000000: 21 3C 61 72 63 68 3E 0A 2F 2F 20 20 20 20 20 20  !<arch>.//      $
00000010: 20 20 20 20 20 20 20 20 30 20 20 20 20 20 20 20          0       $
00000020: 20 20 20 20 30 20 20 20 20 20 30 20 20 20 20 20      0     0     $
00000030: 31 30 30 30 30 30 20 20 34 37 20 20 20 20 20 20  100000  47      $
00000040: 20 20 60 0A 61 62 63 64 65 66 67 68 69 6A 6B 6C    `.abcdefghijkl$
00000050: 6D 6E 6F 70 71 72 73 74 75 76 77 78 79 7A 2F 0A  mnopqrstuvwxyz/.$
00000060: 6A 6B 6C 6D 6E 6F 70 71 72 73 74 75 76 77 78 79  jklmnopqrstuvwxy$
00000070: 7A 2F 0A 0A 2F 30 20 20 20 20 20 20 20 20 20 20  z/../0          $
00000080: 20 20 20 20 38 36 34 30 30 20 20 20 20 20 20 20      86400       $
00000090: 30 20 20 20 20 20 30 20 20 20 20 20 31 30 30 36  0     0     1006$
000000A0: 34 34 20 20 32 37 20 20 20 20 20 20 20 20 60 0A  44  27        `.$
000000B0: 61 62 63 64 65 66 67 68 69 6A 6B 6C 6D 6E 6F 70  abcdefghijklmnop$
000000C0: 71 72 73 74 75 76 77 78 79 7A 0A 0A 2F 32 38 20  qrstuvwxyz../28 $
000000D0: 20 20 20 20 20 20 20 20 20 20 20 20 38 36 34 30              8640$
000000E0: 30 20 20 20 20 20 20 20 30 20 20 20 20 20 30 20  0       0     0 $
000000F0: 20 20 20 20 31 30 30 36 34 34 20 20 31 38 20 20      100644  18  $
00000100: 20 20 20 20 20 20 60 0A 6A 6B 6C 6D 6E 6F 70 71        `.jklmnopq$
00000110: 72 73 74 75 76 77 78 79 7A 0A 78 79 7A 2F 20 20  rstuvwxyz.xyz/  $
00000120: 20 20 20 20 20 20 20 20 20 20 38 36 34 30 30 20            86400 $
00000130: 20 20 20 20 20 20 30 20 20 20 20 20 30 20 20 20        0     0   $
00000140: 20 20 31 30 30 36 34 34 20 20 34 20 20 20 20 20    100644  4     $
00000150: 20 20 20 20 60 0A 78 79 7A 0A                        `.xyz.      $
fubar
if test $? -ne 0 ; then no_result; fi

# make a tarball with files with mostly long names
mkdir junk
if test $? -ne 0 ; then no_result; fi
echo xyz > junk/xyz
if test $? -ne 0 ; then no_result; fi
echo jklmnopqrstuvwxyz > junk/jklmnopqrstuvwxyz
if test $? -ne 0 ; then no_result; fi
echo abcdefghijklmnopqrstuvwxyz > junk/abcdefghijklmnopqrstuvwxyz
if test $? -ne 0 ; then no_result; fi
tar cf junk.tar junk/*
if test $? -ne 0 ; then no_result; fi

# run the command
tardy -auto-test junk.tar -ofmt=ar-bsd test.out -hexdump
if test $? -ne 0 ; then fail; fi

# make sure we get the expected results
diff test.ok test.out || fail

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
