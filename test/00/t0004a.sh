#! /bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 1993, 1995, 1999, 2008, 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="path prefix"
. test_prelude

#
# sed script to orthoganalize the columns
#
cat > sf << 'fubar'
s/^........./& /
s/\// /
fubar
if test $? -ne 0 ; then fail; fi

#
# awk script to pass the uid and name
#       (mode uid gid size name)
#
cat > af << 'fubar'
{ print $5 }
fubar
if test $? -ne 0 ; then fail; fi

#
# create a tar file
#
mkdir foo
if test $? -ne 0 ; then fail; fi
cp af foo
if test $? -ne 0 ; then fail; fi
cd foo
if test $? -ne 0 ; then fail; fi
tar cf ../test.in .
if test $? -ne 0 ; then fail; fi
cp ../sf .
if test $? -ne 0 ; then fail; fi
tar rf ../test.in sf
if test $? -ne 0 ; then fail; fi
cd ..
if test $? -ne 0 ; then fail; fi

#
# create the expected output
#
cat > test.ok << 'fubar'
prefix/
prefix/af
prefix/sf
fubar
if test $? -ne 0 ; then fail; fi

#
# put your test here
#
tardy -prefix prefix < test.in > test.out1
if test $? -ne 0 ; then fail; fi

tardy -list test.out1 /dev/null 2> test.out2
if test $? -ne 0 ; then fail; fi

awk -f af < test.out2 > test.out4
if test $? -ne 0 ; then fail; fi

diff test.ok test.out4
if test $? -ne 0 ; then fail; fi

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
