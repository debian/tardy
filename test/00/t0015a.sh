#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 2002, 2008, 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="input as a list of files"
. test_prelude

#
# test the long file name functionality
#
path=longfilename
echo $path/ > ok
if test $? -ne 0 ; then no_result; fi
for n in a b c d e f g h i j k l m
do
    path=$path/$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n
    mkdir -p $path
    if test $? -ne 0 ; then no_result; fi
    echo $path/ >> ok
    if test $? -ne 0 ; then no_result; fi
    date > $path/yup
    if test $? -ne 0 ; then no_result; fi
    echo $path/yup >> ok
    if test $? -ne 0 ; then no_result; fi
done

tardy -ifmt=list ok test.out
if test $? -ne 0 ; then fail; fi

tar tf test.out > test.out2
if test $? -ne 0 ; then no_result; fi

diff ok test.out2
if test $? -ne 0 ; then fail; fi

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
