#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 1993, 1995, 1999, 2002, 2008, 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="no such file error"
. test_prelude

cat > test.ok << 'fubar'
tardy: NoSuchFile: cannot open: No such file or directory
fubar
if test $? -ne 0 ; then fail; fi

cat > test.ok2 << 'fubar'
tardy: open(pathname = "NoSuchFile", flags = O_RDONLY) failed, No such file
    or directory (2, ENOENT) because there is no "NoSuchFile" regular file
    in the current directory
fubar
if test $? -ne 0 ; then fail; fi

#
# put your test here
#
tardy NoSuchFile ignore 2> test.out
if test $? -ne 1 ; then fail; fi

diff test.ok test.out > /dev/null 2>&1 && pass

diff test.ok2 test.out
if test $? -ne 0 ; then fail; fi

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
