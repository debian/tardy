#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="extract"
. test_prelude

mkdir junk || no_result
echo abc > junk/abc || no_result
tar cf junk.tar junk || no_result

# run the command
tardy junk.tar -prefix more -extract ignore.output || fail

if test ! -f more/junk/abc
then
    echo "expected more/junk/abc to exist"
    fail
fi

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
