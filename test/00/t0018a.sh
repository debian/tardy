#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 2004, 2008, 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="tardy -remove-prefix filter"
. test_prelude

#
# awk script to elide all but the name
#       (mode uid/gid size name)
#
cat > five.awk << 'fubar'
{ print $4 }
fubar
if test $? -ne 0 ; then no_result; fi

cat > test.ok << 'fubar'
./
one
three
two
fubar
if test $? -ne 0 ; then no_result; fi

#
# test the tardy -remprefix functionality
#
mkdir prefix
if test $? -ne 0 ; then no_result; fi
echo one > prefix/one
if test $? -ne 0 ; then no_result; fi
echo two > prefix/two
if test $? -ne 0 ; then no_result; fi
echo three > prefix/three
if test $? -ne 0 ; then no_result; fi

tar cf prefix.tar prefix
if test $? -ne 0 ; then no_result; fi

tardy -rp prefix prefix.tar noprefix.tar
if test $? -ne 0 ; then fail; fi

tar tf noprefix.tar > test.out.unsorted
if test $? -ne 0 ; then no_result; fi

sort test.out.unsorted > test.out
if test $? -ne 0 ; then no_result; fi

diff test.ok test.out
if test $? -ne 0 ; then fail; fi

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
