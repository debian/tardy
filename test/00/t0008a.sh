#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 1995, 1999, 2008, 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="tardy -no-dir option"
. test_prelude

#
# test the -nodir functionality
#
mkdir a a/b
if test $? -ne 0 ; then fail; fi
cat > a/b/c << 'fubar'
something short
fubar
if test $? -ne 0 ; then fail; fi

tar cf test1 a
if test $? -ne 0 ; then fail; fi

tardy -nodir test1 test2
if test $? -ne 0 ; then fail; fi

tardy -list test2 /dev/null 2> test3
if test $? -ne 0 ; then fail; fi

awk '{print $4 " " $5 }' < test3 > test.out
if test $? -ne 0 ; then fail; fi

cat > test.ok << 'fubar'
16 a/b/c
fubar
if test $? -ne 0 ; then fail; fi

diff test.ok test.out
if test $? -ne 0 ; then fail; fi

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
