#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="write cpio(5) crc format"
. test_prelude

sed 's|X$||' > test.ok << 'fubar'
00000000: 30 37 30 37 30 32 30 30 30 30 30 30 30 31 30 30  0707020000000100
00000010: 30 30 34 31 65 64 30 30 30 30 30 30 30 30 30 30  0041ed0000000000
00000020: 30 30 30 30 30 30 30 30 30 30 30 30 30 32 30 30  0000000000000200
00000030: 30 31 35 31 38 30 30 30 30 30 30 30 30 30 30 30  0151800000000000
00000040: 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30  0000000000000000
00000050: 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30  0000000000000000
00000060: 30 30 30 30 30 35 30 30 30 30 30 30 30 30 6A 75  00000500000000ju
00000070: 6E 6B 00 00 30 37 30 37 30 32 30 30 30 30 30 30  nk..070702000000
00000080: 30 32 30 30 30 30 38 31 61 34 30 30 30 30 30 30  02000081a4000000
00000090: 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30  0000000000000000
000000A0: 30 31 30 30 30 31 35 31 38 30 30 30 30 30 30 30  0100015180000000
000000B0: 30 34 30 30 30 30 30 30 30 30 30 30 30 30 30 30  0400000000000000
000000C0: 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30  0000000000000000
000000D0: 30 30 30 30 30 30 30 30 30 39 30 30 30 30 30 31  0000000009000001
000000E0: 33 30 6A 75 6E 6B 2F 61 62 63 00 00 61 62 63 0A  30junk/abc..abc.
000000F0: 30 37 30 37 30 32 30 30 30 30 30 30 30 30 30 30  0707020000000000
00000100: 30 30 38 30 30 30 30 30 30 30 30 30 30 30 30 30  0080000000000000
00000110: 30 30 30 30 30 30 30 30 30 30 30 30 30 31 30 30  0000000000000100
00000120: 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30  0000000000000000
00000130: 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30  0000000000000000
00000140: 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30  0000000000000000
00000150: 30 30 30 30 30 62 30 30 30 30 30 30 30 30 54 52  00000b00000000TR
00000160: 41 49 4C 45 52 21 21 21 00 00 00 00              AILER!!!....    X
fubar
if test $? -ne 0 ; then no_result; fi

mkdir junk
if test $? -ne 0 ; then no_result; fi
echo abc > junk/abc
if test $? -ne 0 ; then no_result; fi
tar cf junk.tar junk
if test $? -ne 0 ; then no_result; fi

# run the command
tardy -auto-test junk.tar -ofmt cpio-crc test.out -hexdump
if test $? -ne 0 ; then fail; fi

# make sure we get the expected results
diff test.ok test.out || fail

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
