#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="recursive directory input"
. test_prelude

cat > test.ok << 'fubar'
0755   0   0     0 junk/
0644   0   0     2 junk/a
0755   0   0     0 junk/rubbish/
0644   0   0     2 junk/rubbish/b
0755   0   0     0 junk/rubbish/crap/
0644   0   0     2 junk/rubbish/crap/c
fubar
if test $? -ne 0 ; then no_result; fi

mkdir -p junk/rubbish/crap
if test $? -ne 0 ; then no_result; fi
echo a > junk/a
if test $? -ne 0 ; then no_result; fi
echo b > junk/rubbish/b
if test $? -ne 0 ; then no_result; fi
echo c > junk/rubbish/crap/c
if test $? -ne 0 ; then no_result; fi

echo junk > test.in
if test $? -ne 0 ; then no_result; fi

# run the command
tardy -auto-test -ifmt=directory test.in test.tar -list > test.out 2>&1
if test $? -ne 0
then
    cat test.out
    fail
fi

# make sure we get the expected results
diff test.ok test.out || fail

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
