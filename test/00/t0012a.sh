#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 1998, 1999, 2004, 2008, 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="new-ascii output format"
. test_prelude

#
# build a directory tree
#
mkdir a b
if test $? -ne 0 ; then no_result; fi
echo cc > a/cc
if test $? -ne 0 ; then no_result; fi
echo ddd > a/ddd
if test $? -ne 0 ; then no_result; fi
echo eeee > b/eeee
if test $? -ne 0 ; then no_result; fi
echo fffff > b/fffff
if test $? -ne 0 ; then no_result; fi

#
# make sure cpio is out there
#
find a b -print > x
if test $? -ne 0 ; then no_result; fi
cpio -o < x > x.cpio 2> LOG
if test $? -ne 0 ; then cat LOG; no_result; fi

#
# test the cpio output functionality
#
tar -cf x.tar a b
if test $? -ne 0 ; then no_result; fi
tardy -now -output-format=newascii x.tar x.cpio2
if test $? -ne 0 ; then fail; fi

#
# make sure cpio accepts it
#       (How portable is this?  How many cpios accept all the formats?)
#
cpio -i -t < x.cpio2 > x.list1 2> LOG
if test $? -ne 0 ; then cat LOG; fail; fi

#
# make sure it looks right
#
sort x.list1 > x.list2
if test $? -ne 0 ; then no_result; fi
cat > ok << 'fubar'
a
a/cc
a/ddd
b
b/eeee
b/fffff
fubar
if test $? -ne 0 ; then no_result; fi
diff ok x.list2
if test $? -ne 0 ; then fail; fi

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
