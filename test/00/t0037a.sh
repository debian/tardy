#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="read ar-bsd"
. test_prelude

sed 's|X$||' > test.ok << 'fubar'
00000000: 61 62 63 00 00 00 00 00 00 00 00 00 00 00 00 00  abc.............X
00000010: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000030: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000040: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000050: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000060: 00 00 00 00 20 20 20 20 36 34 34 00 20 20 20 20  ....    644.    X
00000070: 20 20 30 00 20 20 20 20 20 20 30 00 20 20 20 20    0.      0.    X
00000080: 20 20 20 20 20 20 34 00 20 20 20 20 20 32 35 30        4.     250X
00000090: 36 30 30 00 20 20 20 37 32 31 34 00 30 00 00 00  600.   7214.0...X
000000A0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000000B0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000000C0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000000D0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000000E0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000000F0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000100: 00 75 73 74 61 72 20 20 00 72 6F 6F 74 00 00 00  .ustar  .root...X
00000110: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000120: 00 00 00 00 00 00 00 00 00 72 6F 6F 74 00 00 00  .........root...X
00000130: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000140: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000150: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000160: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000170: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000180: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000190: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000001A0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000001B0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000001C0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000001D0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000001E0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000001F0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000200: 61 62 63 0A 00 00 00 00 00 00 00 00 00 00 00 00  abc.............X
00000210: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000220: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000230: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000240: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000250: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000260: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000270: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000280: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000290: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000002A0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000002B0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000002C0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000002D0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000002E0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000002F0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000300: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000310: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000320: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000330: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000340: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000350: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000360: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000370: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000380: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
00000390: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000003A0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000003B0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000003C0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000003D0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000003E0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
000003F0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................X
fubar
if test $? -ne 0 ; then no_result; fi

echo abc > abc
if test $? -ne 0 ; then no_result; fi
tar cf abc.tar abc
if test $? -ne 0 ; then no_result; fi
tardy abc.tar -ofmt ar-bsd abc.a
if test $? -ne 0 ; then no_result; fi

# run the command
tardy -auto-test abc.a -ofmt=tar test.out -hexdump
if test $? -ne 0 ; then fail; fi

# make sure we get the expected results
diff test.ok test.out || fail

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
