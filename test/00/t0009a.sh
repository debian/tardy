#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 1995, 1999, 2008, 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="tardy -remove-path option"
. test_prelude

#
# awk script to elide all but the name
#       (mode uid gid size name)
#
cat > af << 'fubar'
{ print $5 }
fubar
if test $? -ne 0 ; then fail; fi

#
# create files for the tar file
#
mkdir a b c a/a a/b a/c b/a b/b b/c c/a c/b c/c
if test $? -ne 0 ; then fail; fi
echo hello > a/b/one
if test $? -ne 0 ; then fail; fi
echo hello > b/c/two
if test $? -ne 0 ; then fail; fi
echo hello > c/a/three
if test $? -ne 0 ; then fail; fi

#
# create the tar file
#
tar cf test.in a b c
if test $? -ne 0 ; then fail; fi

#
# create the expected output
#
cat > test.ok << 'fubar'
one
c/two
c/a/three
fubar
if test $? -ne 0 ; then fail; fi

#
# put your test here
#
tardy -rp=a -rp=b -nd test.in test.out1
if test $? -ne 0 ; then fail; fi

tardy -l test.out1 /dev/null 2> test.out2
if test $? -ne 0 ; then fail; fi

awk -f af < test.out2 > test.out4
if test $? -ne 0 ; then fail; fi

diff test.ok test.out4
if test $? -ne 0 ; then fail; fi

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
