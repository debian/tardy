#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 1996, 1999, 2008, 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="tardy -downcase filter"
. test_prelude

#
# test the -downcase functionality
#
mkdir FooBar
if test $? -ne 0 ; then fail; fi
chmod 0755 FooBar
if test $? -ne 0 ; then fail; fi
for f in I Wonder Just How Well This Will Work
do
    cp /dev/null FooBar/$f
    if test $? -ne 0 ; then fail; fi
    chmod 0644 FooBar/$f
    if test $? -ne 0 ; then fail; fi
done
tar cf infile FooBar
if test $? -ne 0 ; then fail; fi

tardy -unu 0 -gnu 0 -downcase infile outfile
if test $? -ne 0 ; then fail; fi

tardy -list outfile /dev/null 2> test.out
if test $? -ne 0 ; then fail; fi

sort -k 4 < test.out > test.out2
if test $? -ne 0 ; then fail; fi

cat > ok << 'fubar'
0755   0   0     0 foobar/
0644   0   0     0 foobar/how
0644   0   0     0 foobar/i
0644   0   0     0 foobar/just
0644   0   0     0 foobar/this
0644   0   0     0 foobar/well
0644   0   0     0 foobar/will
0644   0   0     0 foobar/wonder
0644   0   0     0 foobar/work
fubar
if test $? -ne 0 ; then fail; fi

diff ok test.out2
if test $? -ne 0 ; then fail; fi

#
# test the -upcase functionality
#
tardy -unu 0 -gnu 0 -upcase infile outfile
if test $? -ne 0 ; then fail; fi

tardy -list outfile /dev/null 2> test.out
if test $? -ne 0 ; then fail; fi

sort -k 4 < test.out > test.out2
if test $? -ne 0 ; then fail; fi

cat > ok << 'fubar'
0755   0   0     0 FOOBAR/
0644   0   0     0 FOOBAR/HOW
0644   0   0     0 FOOBAR/I
0644   0   0     0 FOOBAR/JUST
0644   0   0     0 FOOBAR/THIS
0644   0   0     0 FOOBAR/WELL
0644   0   0     0 FOOBAR/WILL
0644   0   0     0 FOOBAR/WONDER
0644   0   0     0 FOOBAR/WORK
fubar
if test $? -ne 0 ; then fail; fi

diff ok test.out2
if test $? -ne 0 ; then fail; fi

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
