#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 1993, 1995, 1999, 2008, 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="mode set and clear"
. test_prelude

#
# sed script to orthoganalize the columns
#
cat > sf << 'fubar'
s/^........./& /
s/\// /
fubar
if test $? -ne 0 ; then fail; fi

#
# awk script to pass the uid and name
#       (mode uid gid size name)
#
cat > af << 'fubar'
{ print $1 " " $5 }
fubar
if test $? -ne 0 ; then fail; fi

#
# create a tar file
#
tar cf test.in af sf
if test $? -ne 0 ; then fail; fi

#
# create the expected output
#
cat > test.ok << 'fubar'
0644 af
0644 sf
fubar
if test $? -ne 0 ; then fail; fi

#
# put your test here
#
tardy -ms 0644 -mc 022 test.in test.out1
if test $? -ne 0 ; then fail; fi

tardy -l test.out1 > /dev/null 2> test.out2
if test $? -ne 0 ; then fail; fi

awk -f af < test.out2 > test.out4
if test $? -ne 0 ; then fail; fi

diff test.ok test.out4
if test $? -ne 0 ; then fail; fi

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
