#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 2003, 2008, 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="test gzip and gunzip"
. test_prelude

#
# test the g(un)zip functionality
#
mkdir tst
if test $? -ne 0 ; then no_result; fi
echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx > tst/hello
if test $? -ne 0 ; then no_result; fi
cat tst/hello tst/hello > tst/hello2
if test $? -ne 0 ; then no_result; fi
cat tst/hello2 tst/hello2 > tst/hello3
if test $? -ne 0 ; then no_result; fi
cat tst/hello3 tst/hello3 > tst/hello4
if test $? -ne 0 ; then no_result; fi
cat tst/hello4 tst/hello4 > tst/hello5
if test $? -ne 0 ; then no_result; fi
cat tst/hello5 tst/hello5 > tst/hello6
if test $? -ne 0 ; then no_result; fi
cat tst/hello6 tst/hello6 > tst/hello7
if test $? -ne 0 ; then no_result; fi

tar cf tst.tar tst
if test $? -ne 0 ; then no_result; fi

tardy -gzip -rp=tst -p=tst2 tst.tar tst2.tar
if test $? -ne 0 ; then fail; fi

tardy -gunzip tst2.tar tst3.tar
if test $? -ne 0 ; then fail; fi

tar xf tst3.tar
if test $? -ne 0 ; then no_result; fi

cmp tst/hello7 tst2/hello7 || fail

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
