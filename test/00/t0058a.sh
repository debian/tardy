#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="implied output gzip"
. test_prelude

cat > test.ok << 'fubar'
junk/
junk/abc
fubar
if test $? -ne 0 ; then no_result; fi

mkdir junk
if test $? -ne 0 ; then no_result; fi
echo abc > junk/abc
if test $? -ne 0 ; then no_result; fi
tar cf junk.tar junk
if test $? -ne 0 ; then no_result; fi

# run the command
tardy -auto-test junk.tar test.out.tgz
if test $? -ne 0 ; then fail; fi

tar tzf test.out.tgz > test.out
if test $? -ne 0 ; then no_result; fi

# make sure we get the expected results
diff test.ok test.out || fail

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
