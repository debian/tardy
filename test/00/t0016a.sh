#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 2002, 2008, 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="very long file names"
. test_prelude

#
# test the long dir name functionality
#
#!/bin/sh
dir=ttt
for n in 1 2 3
do
    name=$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n$n
    dir=$dir/$name
    mkdir -p $dir
    if test $? -ne 0; then no_result; fi
    touch $dir/file1
    touch $dir/file2
done

#
# GNU tar writes longer names as separate headers.
#
tar cf test.in ttt
if test $? -ne 0 ; then no_result; fi

#
# Now may sure the processing doesn't barf.
#
tardy test.in test.out
if test $? -ne 0 ; then fail; fi

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
