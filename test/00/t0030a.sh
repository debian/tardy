#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="write cpio-bin-be"
. test_prelude

sed 's|X$||' > test.ok << 'fubar'
00000000: 71 C7 00 00 00 01 41 ED 00 00 00 00 00 02 00 00  qG....Am........X
00000010: 00 01 51 80 00 05 00 00 00 00 6A 75 6E 6B 00 00  ..Q.......junk..X
00000020: 71 C7 00 00 00 02 81 A4 00 00 00 00 00 01 00 00  qG.....$........X
00000030: 00 01 51 80 00 09 00 00 00 04 6A 75 6E 6B 2F 61  ..Q.......junk/aX
00000040: 62 63 00 00 61 62 63 0A 71 C7 00 00 00 00 80 00  bc..abc.qG......X
00000050: 00 00 00 00 00 01 00 00 00 00 00 00 00 0B 00 00  ................X
00000060: 00 00 54 52 41 49 4C 45 52 21 21 21 00 00        ..TRAILER!!!..  X
fubar
if test $? -ne 0 ; then no_result; fi

mkdir junk || no_result
echo abc > junk/abc || no_result
tar cf junk.tar junk || no_result

# run the command
tardy -auto-test junk.tar -ofmt cpio-bin-be test.out -hexdump
if test $? -ne 0 ; then fail; fi

# make sure we get the expected results
diff test.ok test.out || fail

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
