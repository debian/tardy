#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 2009, 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="tardy -exclude filter"
. test_prelude

#
# test the exclude functionality
#
cat > test.ok << 'fubar'
a/
a/1
a/2
c/
c/5
c/d/
c/d/6
c/d/e/
c/d/e/7
fubar
if test $? -ne 0 ; then no_result; fi

mkdir -p a b c/d/e || no_result

date > a/1
if test $? -ne 0 ; then no_result; fi
date > a/2
if test $? -ne 0 ; then no_result; fi
date > b/3
if test $? -ne 0 ; then no_result; fi
date > b/4
if test $? -ne 0 ; then no_result; fi
date > c/5
if test $? -ne 0 ; then no_result; fi
date > c/d/6
if test $? -ne 0 ; then no_result; fi
date > c/d/e/7
if test $? -ne 0 ; then no_result; fi

tar cf in.tar a b c
if test $? -ne 0 ; then no_result; fi

tardy -exclude "b/*" in.tar out.tar
if test $? -ne 0 ; then fail; fi

tar tf out.tar > test.out.unsorted
if test $? -ne 0 ; then no_result; fi
LANG=C sort test.out.unsorted > test.out

diff test.ok test.out
if test $? -ne 0 ; then fail; fi

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
