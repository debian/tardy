#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 2002, 2008, 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="tardy input format as a file list"
. test_prelude

#
# test the --input-format=list functionality
#
date > a
if test $? -ne 0 ; then no_result; fi
date > b
if test $? -ne 0 ; then no_result; fi

cat > infile << 'fubar'
a
b
fubar

tardy -ifmt=list infile outfile
if test $? -ne 0 ; then fail; fi

#
# make sure it looks right
#
tar tf outfile > the.list 2> LOG
if test $? -ne 0 ; then cat LOG; no_result; fi

diff infile the.list
if test $? -ne 0 ; then fail; fi

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
