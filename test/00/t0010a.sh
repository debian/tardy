#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 1996, 1999, 2001, 2008, 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="tardy -list option"
. test_prelude

#
# test the -clean functionality
#
mkdir foo
if test $? -ne 0 ; then fail; fi
chmod 0755 foo
if test $? -ne 0 ; then fail; fi
for f in 'a&b|c;d' '[^abc]`' "'foo'" '(ha=ha)' "Names With Spaces"
do
    cp /dev/null "foo/$f"
    if test $? -ne 0 ; then fail; fi
    chmod 0644 "foo/$f"
    if test $? -ne 0 ; then fail; fi
done
tar cf infile foo
if test $? -ne 0 ; then fail; fi

tardy -unu 0 -gnu 0 -clean infile outfile
if test $? -ne 0 ; then fail; fi

tardy -list outfile /dev/null 2> test.out
if test $? -ne 0 ; then fail; fi

LC_ALL=C
export LC_ALL

sort -k 4 < test.out > test.out2
if test $? -ne 0 ; then fail; fi

cat > ok << 'fubar'
0755   0   0     0 foo/
0644   0   0     0 foo/--abc--
0644   0   0     0 foo/-foo-
0644   0   0     0 foo/-ha-ha-
0644   0   0     0 foo/Names-With-Spaces
0644   0   0     0 foo/a-b-c-d
fubar
if test $? -ne 0 ; then fail; fi

diff ok test.out2
if test $? -ne 0 ; then fail; fi

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
