#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 2013 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="tarballs >2GB"
. test_prelude

#                                         |||||
#    beware potential timezone mismatch:  vvvvv
cat > test.ok << 'fubar'
-rw-r--r-- root/root       256 1970-01-02 10:00 a.bogus
-rw-r--r-- root/root       256 1970-01-02 10:00 b.bogus
-rw-r--r-- root/root       256 1970-01-02 10:00 c.bogus
-rw-r--r-- root/root       256 1970-01-02 10:00 d.bogus
-rw-r--r-- root/root       256 1970-01-02 10:00 e.bogus
-rw-r--r-- root/root       256 1970-01-02 10:00 f.bogus
-rw-r--r-- root/root       256 1970-01-02 10:00 h.bogus
-rw-r--r-- root/root       256 1970-01-02 10:00 i.bogus
fubar
if test $? -ne 0 ; then no_result; fi

for c in a b c d  a e f h i
do
    dd if=/dev/zero bs=256 count=1 of=$c.bogus > LOG 2>&1
    if test $? -ne 0
    then
        echo LOG
        no_result
    fi
done

# build the tarball
tar cf bogus.tar ?.bogus
if test $? -ne 0 ; then no_result; fi

# now unpackthe tar ball, it lightly lrgerthan 2GB.
tardy bogus.tar --auto-test bogus2.tar
if test $? -ne 0 ; then fail; fi

# npw see if he real tar is OK with it.
tar tvf bogus2.tar > test.out.2

sed 's| [0-9][0-9]:[0-5][0-9] | 10:00 |' test.out.2 > test.out
if test $? -ne 0 ; then no_result; fi

# make sure we get the expected results
diff test.ok test.out || fail

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
# vim: set ts=8 sw=4 et :
