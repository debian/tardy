#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="write ar-port5"
. test_prelude

sed 's|X$||' > test.ok << 'fubar'
00000000: 3C 61 72 3E 74 65 73 74 2E 6F 75 74 00 00 00 00  <ar>test.out....
00000010: 00 00 00 00 00 00 00 00 00 00 00 00 61 62 63 00  ............abc.
00000020: 00 00 00 00 00 00 00 00 00 00 00 00 00 01 51 80  ..............Q.
00000030: 00 00 00 00 00 00 00 00 00 00 81 A4 00 00 00 05  ...........$....
00000040: 61 62 63 64 0A 0A 0A 0A 78 79 7A 00 00 00 00 00  abcd....xyz.....
00000050: 00 00 00 00 00 00 00 00 00 01 51 80 00 00 00 00  ..........Q.....
00000060: 00 00 00 00 00 00 81 A4 00 00 00 05 77 78 79 7A  .......$....wxyz
00000070: 0A 0A 0A 0A                                      ....            X
fubar
if test $? -ne 0 ; then no_result; fi

mkdir junk
if test $? -ne 0 ; then no_result; fi
echo abcd > junk/abc
if test $? -ne 0 ; then no_result; fi
echo wxyz > junk/xyz
if test $? -ne 0 ; then no_result; fi
echo junk > junk.d
tardy -ifmt=directory junk.d junk.tar

# run the command
tardy -auto-test junk.tar -ofmt=ar-port5-be test.out -hexdump
if test $? -ne 0 ; then fail; fi

# make sure we get the expected results
diff test.ok test.out || fail

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
