#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 1993, 1995, 1999, 2004, 2008, 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="set uid"
. test_prelude

#
# sed script to orthoganalize the columns
#
cat > sf << 'fubar'
s/^........./& /
s/\// /
fubar
if test $? -ne 0 ; then fail; fi

#
# awk script to pass the uid and name
#       (mode uid gid size name)
#
cat > af << 'fubar'
{ print $2 " " $5 }
fubar
if test $? -ne 0 ; then fail; fi

#
# create a tar file
#
tar cf test.in af sf
if test $? -ne 0 ; then fail; fi

#
# put your test here
#
tardy -u 0 -g 0 -ms 0644 test.in test.out1
if test $? -ne 0 ; then fail; fi

tar tvf test.out1 > /dev/null 2> LOG
if test $? -ne 0 ; then cat LOG; fail; fi

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
