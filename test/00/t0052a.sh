#!/bin/sh
#
# tardy - a tar post-processor
# Copyright (C) 2011 Peter Miller
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

TEST_SUBJECT="write ar-pdp11-le"
. test_prelude

sed 's|X$||' > test.ok << 'fubar'
00000000: 6D FF 61 62 63 64 00 00 00 00 01 00 80 51 00 00  m.abcd.......Q..X
00000010: A4 81 05 00 61 62 63 64 0A 0A                    $...abcd..      X
fubar
if test $? -ne 0 ; then no_result; fi

echo abcd > abcd
if test $? -ne 0 ; then no_result; fi
tar cf test.in abcd
if test $? -ne 0 ; then no_result; fi

# run the command
tardy -auto-test test.in -ofmt=ar-pdp11-le test.out -hexdump
if test $? -ne 0 ; then no_result; fi

# make sure we get the expected results
diff test.ok test.out || fail

#
# Only definite negatives are possible.
# The functionality exercised by this test appears to work,
# no other guarantees are made.
#
pass
