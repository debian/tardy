Summary: a tar post-processor
Name: tardy
Version: 1.28
Release: 1
Copyright: GPL
Group: System/Tape
Source: http://tardy.sourceforge.net/tardy-1.28.tar.gz
URL: http://tardy.sourceforge.net/
BuildRoot: /tmp/tardy-build-root

%description
The tardy program is a tar(1) post-processor.  It may be used to
manipulate the file headers tar(5) archive files in various ways.

The reason the tardy program was written was because the author wanted
to "spruce up" tar files before posting them to the net, mostly to
remove artefacts of the development environment, without introducing more.

The tardy program was designed to allow you to alter certain atrributes
of files after they have been included in the tar file.  Among them are:

  * change file owner (by number or name)

  * change file group (by number or name)

  * add directory prefix (for example, dot)

  * change file protections (for example, from 600 to 644)

Note that all of these affect ALL files in the archive.

%prep
%setup
./configure --prefix=/usr

%build
make

%install
make RPM_BUILD_ROOT=$RPM_BUILD_ROOT install

%files
/usr/bin/tardy
