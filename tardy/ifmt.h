//
// tardy - a tar post-processor
// Copyright (C) 2002, 2008, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef TARDY_IFMT_H
#define TARDY_IFMT_H

#include <libtardy/tar/input.h>

class file_input; // forward

/**
  * The input_opener_ty type is used to represent a pointer to a
  * function that can be used to open file file using a particular
  * archive format.
  */
typedef tar_input::pointer (*input_opener_ty)(const file_input::pointer &fp);

/**
  * The input_opener_by_name function is used to map a name into a
  * pointer to a pointer to a function that can be used to open file
  * file using a particular archive format.
  *
  * @param name
  *     The name of the input format to search for.
  */
input_opener_ty input_opener_by_name(const char *name);

#endif // TARDY_IFMT_H
