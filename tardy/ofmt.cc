//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2002, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/stdio.h>
#include <libexplain/output.h>

#include <libtardy/symtab.h>
#include <libtardy/tar/output/ar/bsd.h>
#include <libtardy/tar/output/ar/port5.h>
#include <libtardy/tar/output/ar/pdp11.h>
#include <libtardy/tar/output/ar/v7.h>
#include <libtardy/tar/output/cpio/binary.h>
#include <libtardy/tar/output/cpio/crc.h>
#include <libtardy/tar/output/cpio/newascii.h>
#include <libtardy/tar/output/cpio/oldascii.h>
#include <libtardy/tar/output/tar/bsd.h>
#include <libtardy/tar/output/tar/posix.h>
#include <libtardy/tar/output/tar/ustar.h>
#include <libtardy/tar/output/tar/v7.h>

#include <tardy/ofmt.h>


output_opener_ty
output_opener_by_name(const char *cname)
{
    struct table_ty
    {
        const char *name;
        output_opener_ty func;
    };

    static table_ty table[] =
    {
        { "ar", tar_output_ar_bsd::create },
        { "ar-bsd", tar_output_ar_bsd::create },
        { "ar-bsd-l2", tar_output_ar_bsd::create_l2 },
        { "ar-ctix", tar_output_ar_port5::create_be },
        { "ar-ctix-be", tar_output_ar_port5::create_be },
        { "ar-ctix-le", tar_output_ar_port5::create_le },
        { "ar-mc68k", tar_output_ar_port5::create_be },
        { "ar-mc68k-be", tar_output_ar_port5::create_be },
        { "ar-mc68k-le", tar_output_ar_port5::create_le },
        { "ar-pdp11", tar_output_ar_pdp11::create_le },
        { "ar-pdp11-be", tar_output_ar_pdp11::create_be },
        { "ar-pdp11-le", tar_output_ar_pdp11::create_le },
        { "ar-port5", tar_output_ar_port5::create_be },
        { "ar-port5-be", tar_output_ar_port5::create_be },
        { "ar-port5-le", tar_output_ar_port5::create_le },
        { "ar-std", tar_output_ar_bsd::create },
        { "ar-v7", tar_output_ar_v7::create_le },
        { "ar-v7-be", tar_output_ar_v7::create_be },
        { "ar-v7-le", tar_output_ar_v7::create_le },
        { "bsd", tar_output_tar_bsd::create },
        { "cpio", tar_output_cpio_newascii::create },
        { "cpio-bin", tar_output_cpio_binary::create_native_endian },
        { "cpio-bin-be", tar_output_cpio_binary::create_be },
        { "cpio-bin-le", tar_output_cpio_binary::create_le },
        { "cpio-crc", tar_output_cpio_crc::create },
        { "cpio-new-ascii", tar_output_cpio_newascii::create },
        { "cpio-newc", tar_output_cpio_newascii::create },
        { "cpio-old-ascii", tar_output_cpio_oldascii::create },
        { "cpio-oldc", tar_output_cpio_oldascii::create },
        { "gnu", tar_output_tar_posix::create },
        { "gnu-tar", tar_output_tar_posix::create },
        { "gtar", tar_output_tar_posix::create },
        { "newascii", tar_output_cpio_newascii::create },
        { "newc", tar_output_cpio_newascii::create },
        { "oldascii", tar_output_cpio_oldascii::create },
        { "oldc", tar_output_cpio_oldascii::create },
        { "posix", tar_output_tar_posix::create },
        { "tar", tar_output_tar_posix::create },
        { "tar-bsd", tar_output_tar_bsd::create },
        { "tar-gnu", tar_output_tar_posix::create },
        { "tar-posix", tar_output_tar_posix::create },
        { "tar-v7", tar_output_tar_v7::create },
        { "ustar", tar_output_tar_ustar::create },
        { "v7", tar_output_tar_v7::create },
    };

    static symtab *stp;
    if (!stp)
    {
        stp = new symtab();
        for (table_ty *tp = table; tp < ENDOF(table); ++tp)
            stp->assign(tp->name, (void *)tp->func);
    }

    rcstring name(cname);
    output_opener_ty result = (output_opener_ty)stp->query(name);
    if (!result)
    {
        rcstring guess = stp->query_fuzzy(name);
        if (guess != "")
        {
            explain_output_error_and_die
            (
                "output format %s unknown, closest is %s",
                name.quote_c().c_str(),
                guess.quote_c().c_str()
            );
        }
        explain_output_error_and_die
        (
            "output format %s unknown",
            name.quote_c().c_str()
        );
    }

    return result;
}
