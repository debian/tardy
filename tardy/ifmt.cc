//
// tardy - a tar post-processor
// Copyright (C) 2002, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/config.h>
#include <libexplain/output.h>

#include <libtardy/symtab.h>
#include <libtardy/tar/input/ar/bsd.h>
#include <libtardy/tar/input/ar/pdp11.h>
#include <libtardy/tar/input/ar/v7.h>
#include <libtardy/tar/input/cpio/binary.h>
#include <libtardy/tar/input/cpio/crc.h>
#include <libtardy/tar/input/cpio/new_ascii.h>
#include <libtardy/tar/input/cpio/old_ascii.h>
#include <libtardy/tar/input/directory.h>
#include <libtardy/tar/input/filenamelist.h>
#include <libtardy/tar/input/tar/bsd.h>
#include <libtardy/tar/input/tar/posix.h>
#include <libtardy/tar/input/tar/ustar.h>

#include <tardy/ifmt.h>


input_opener_ty
input_opener_by_name(const char *a_name)
{
    rcstring name = rcstring(a_name).downcase();
    struct table_ty
    {
        const char *name;
        input_opener_ty func;
    };

    static const table_ty table[] =
    {
        { "ar", tar_input_ar_bsd::factory },
        { "ar-bsd", tar_input_ar_bsd::factory },
        { "ar-bsd", tar_input_ar_bsd::factory },
        { "ar-pdp11", tar_input_ar_pdp11::create },
        { "ar-pdp11-be", tar_input_ar_pdp11::create_be },
        { "ar-pdp11-le", tar_input_ar_pdp11::create_le },
        { "ar-std", tar_input_ar_bsd::factory },
        { "ar-v7", tar_input_ar_v7::create },
        { "ar-v7-be", tar_input_ar_v7::create_be },
        { "ar-v7-le", tar_input_ar_v7::create_le },
        { "bsd", tar_input_tar_bsd::factory },
        { "cpio", tar_input_cpio::factory },
        { "cpio-bin", tar_input_cpio_binary::create },
        { "cpio-binary", tar_input_cpio_binary::create },
        { "cpio-crc", tar_input_cpio_crc::create },
        { "cpio-new-ascii", tar_input_cpio_new_ascii::create },
        { "cpio-odc", tar_input_cpio_old_ascii::create },
        { "cpio-old-ascii", tar_input_cpio_old_ascii::create },
        { "cpio-oldc", tar_input_cpio_old_ascii::create },
        { "cpio-posix", tar_input_cpio_old_ascii::create },
        { "cpio-susv2", tar_input_cpio_old_ascii::create },
        { "directory", tar_input_directory::create },
        { "list", tar_input_filenamelist::create },
        { "posix", tar_input_tar_ustar::factory },
        { "recursive", tar_input_directory::create },
        { "susv2", tar_input_cpio_old_ascii::create },
        { "tar", tar_input_tar::factory },
        { "tar-bsd", tar_input_tar_bsd::factory },
        { "tar-posix", tar_input_tar_ustar::factory },
        { "ustar", tar_input_tar_ustar::factory },
    };

    static symtab *stp;
    if (!stp)
    {
        stp = new symtab();
        for (const table_ty *tp = table; tp < ENDOF(table); ++tp)
            stp->assign(tp->name, (void *)tp->func);
    }

    input_opener_ty result = (input_opener_ty)stp->query(name);
    if (!result)
    {
        rcstring guess = stp->query_fuzzy(name);
        if (!guess.empty())
        {
            explain_output_error_and_die
            (
                "input format %s unknown, closest is %s",
                name.quote_c().c_str(),
                guess.quote_c().c_str()
            );
        }
        explain_output_error_and_die
        (
            "input format %s unknown",
            name.quote_c().c_str()
        );
    }

    return result;
}
