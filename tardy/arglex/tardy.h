//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef TARDY_ARGLEX_TARDY_H
#define TARDY_ARGLEX_TARDY_H

#include <libtardy/arglex.h>

/**
  * The arglex_tardy class is used to represent the command line lexical
  * analysis specific to the tardy(1) command.
  */
class arglex_tardy:
    public arglex
{
public:
    /**
      * The destructor.
      */
    virtual ~arglex_tardy();

    /**
      * The constructor.
      *
      * @param ac
      *     argument count, from main
      * @param av
      *     argument values, from main
      */
    arglex_tardy(int ac, char **av);

    enum
    {
        token_blocksize,
        token_clean,
        token_clean_meta,
        token_clean_print,
        token_clean_space,
        token_downcase,
        token_exclude,
        token_extract,
        token_format_input,
        token_format_output,
        token_group,
        token_group_name,
        token_group_number,
        token_gunzip,
        token_gzip,
        token_hexdump,
        token_list,
        token_mode_clear,
        token_mode_set,
        token_no_directories,
        token_now,
        token_mtime_debug,
        token_old_type,
        token_prefix,
        token_progress,
        token_relative_paths,
        token_remove_prefix,
        token_upcase,
        token_user,
        token_user_name,
        token_user_number
    };

    // See base class for documentation.
    void usage(void) const;

    // See base class for documentation.
    void help(void);

private:
    /**
      * The default constructor.  Do not use.
      */
    arglex_tardy();

    /**
      * The copy constructor.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    arglex_tardy(const arglex_tardy &rhs);

    /**
      * The assignment operator.  Do not use.
      *
      * @param rhs
      *     The right hand side of the initialisation.
      */
    arglex_tardy &operator=(const arglex_tardy &rhs);
};

#endif // TARDY_ARGLEX_TARDY_H
