//
// tardy - a tar post-processor
// Copyright (C) 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <libtardy/ac/stdio.h>
#include <libtardy/ac/stdlib.h>
#include <libexplain/program_name.h>

#include <tardy/arglex/tardy.h>


arglex_tardy::~arglex_tardy()
{
}

static const arglex::table_t argtab[] =
{
    { "-Automated_Testing", arglex_tardy::token_mtime_debug, },
    { "-Block_Size",    arglex_tardy::token_blocksize, },
    { "-Clean",         arglex_tardy::token_clean,    },
    { "-Clean_Meta",    arglex_tardy::token_clean_meta, },
    { "-Clean_Print",   arglex_tardy::token_clean_print, },
    { "-Clean_Space",   arglex_tardy::token_clean_space, },
    { "-COmpress",      arglex_tardy::token_gzip,     },
    { "-DeCOmpress",    arglex_tardy::token_gunzip,   },
    { "-DownCase",      arglex_tardy::token_downcase, },
    { "-EXclude",       arglex_tardy::token_exclude,  },
    { "-eXtract",       arglex_tardy::token_extract,  },
    { "-Group",         arglex_tardy::token_group,    },
    { "-Group_NAme",    arglex_tardy::token_group_name, },
    { "-Group_NUmber",  arglex_tardy::token_group_number, },
    { "-GUnZip",        arglex_tardy::token_gunzip,   },
    { "-GZip",          arglex_tardy::token_gzip,     },
    { "-HexDump",       arglex_tardy::token_hexdump,  },
    { "-Input_ForMaT",  arglex_tardy::token_format_input, },
    { "-List",          arglex_tardy::token_list,     },
    { "-Mode_Clear",    arglex_tardy::token_mode_clear, },
    { "-Mode_Set",      arglex_tardy::token_mode_set, },
    { "-MTime_Debug",   arglex_tardy::token_mtime_debug, },
    { "-No_Absolute_Paths", arglex_tardy::token_relative_paths, },
    { "-Now",           arglex_tardy::token_now,      },
    { "-No_Directories", arglex_tardy::token_no_directories, },
    { "-Old_Type",      arglex_tardy::token_old_type, },
    { "-Output_ForMaT", arglex_tardy::token_format_output, },
    { "-Prefix",        arglex_tardy::token_prefix,   },
    { "-PROgress",      arglex_tardy::token_progress, },
    { "-RELative_paths", arglex_tardy::token_relative_paths, },
    { "-Remove_Prefix", arglex_tardy::token_remove_prefix, },
    { "-UnCOmpress",    arglex_tardy::token_gunzip,   },
    { "-UpCase",        arglex_tardy::token_upcase,   },
    { "-User",          arglex_tardy::token_user,     },
    { "-User_NAme",     arglex_tardy::token_user_name, },
    { "-User_NUmber",   arglex_tardy::token_user_number, },
    { 0, 0, }, // end marker
};


arglex_tardy::arglex_tardy(int a_argc, char **a_argv) :
    arglex(a_argc, a_argv, argtab)
{
}


void
arglex_tardy::usage(void)
    const
{
    const char *progname = explain_program_name_get();
    fprintf(stderr, "Usage: %s [ <option>... ][ <infile> [ <outfile> ]]\n",
        progname);
    fprintf(stderr, "       %s -Help\n", progname);
    fprintf(stderr, "       %s -VERSion\n", progname);
    exit(1);
}


void
arglex_tardy::help(void)
{
    static const char *text[] =
    {
#include <man/man1/tardy.h>
    };

    help_f(text, SIZEOF(text));
}
