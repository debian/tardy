//
// tardy - a tar post-processor
// Copyright (C) 1998, 1999, 2008, 2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef TARDY_OFMT_H
#define TARDY_OFMT_H

#include <libtardy/tar/output.h>

class file_output; // existence

/**
  * The output_opener_ty type is used to * represent the type of
  * function pointer that accepts a file output instance, and returns
  * the appropriate function to write tar fomatted output to that file.
  */
typedef tar_output::pointer (*output_opener_ty)(const file_output::pointer &fp);

/**
  * The output_opener_by_name function is used to take a file output
  * instance, and returns the appropriate function to write tar fomatted
  * output to that file.
  *
  * @param name
  *     The name of the output format.
  */
output_opener_ty output_opener_by_name(const char *name);

#endif // TARDY_OFMT_H
