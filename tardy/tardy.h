//
// tardy - a tar post-processor
// Copyright (C) 1993-2009, 2011 Peter Miller
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef TARDY_TARDY_H
#define TARDY_TARDY_H

#include <libtardy/ac/time.h>
#include <libtardy/main.h>

void tardy_clean_meta(void);
void tardy_clean_print(void);
void tardy_clean_space(void);
void tardy_downcase(void);
void tardy_format_input(const char *);
void tardy_format_output(const char *);
void tardy_extract(void);
void tardy_group_name_by_number(long);
void tardy_group_name_by_string(const char *);
void tardy_group_number_by_number(long);
void tardy_group_number_by_string(const char *);
void tardy_list(void);
void tardy_mode_clear(int);
void tardy_mode_set(int);
void tardy_no_directories(void);
void tardy_no_fix_type(void);
void tardy_now(void);
void tardy_mtime(time_t when);
void tardy_prefix(const char *);
void tardy_remove_prefix(const char *);
void tardy_remove_prefix(long);
void tardy_upcase(void);
void tardy_user_name_by_number(long);
void tardy_user_name_by_string(const char *);
void tardy_user_number_by_number(long);
void tardy_user_number_by_string(const char *);
void tardy_gzip(void);
void tardy_gunzip(void);
void tardy_exclude(const char *pattern);
void tardy_hexdump(void);
void tardy_relative_paths(void);

/**
  * The tardy_blocksize function is used to set the block size, as a
  * multiple of 512 bytes.  For example, tar_blcoksize(20) results in
  * a block size of 10KB being used.
  */
void tardy_blocksize(int);

void tardy(const char *infile, const char *outfile);

#endif // TARDY_TARDY_H
