From: Jari Aalto <jari.aalto@cante.net>
Date: Sun, 24 Nov 2024 15:23:47 +0100
Subject: Manual page fixes

---
 man/man1/tardy.1 | 80 +++++++++++++++++++++++++++++++++++++++++++++++++++-----
 1 file changed, 74 insertions(+), 6 deletions(-)

diff --git a/man/man1/tardy.1 b/man/man1/tardy.1
index 9bc0259..fea1a70 100644
--- a/man/man1/tardy.1
+++ b/man/man1/tardy.1
@@ -16,10 +16,9 @@
 .\" along with this program. If not, see <http://www.gnu.org/licenses/>.
 .\"
 .ds n) tardy
-.TH Tardy 1
+.TH TARDY 1
 .SH NAME
 tardy \- a tar post-processor
-.XX "tardy(1)" "a tar(1) post-processor"
 .SH SYNOPSIS
 .B \*(n)
 [
@@ -169,7 +168,12 @@ .SH OPTIONS
 The number may be any arbitrary number,
 it is not restricted to a known group.
 .\" ----------  H  ---------------------------------------------------------
-.so man/man1/o_help.so
+.TP 8n
+.B \-Help
+.br
+This option may be used to obtain more information about how to use the
+.I \*(n)
+program.
 .\" ----------  I  ---------------------------------------------------------
 .TP 8n
 \fB\-Input_ForMaT\fP \f[I]name\fP
@@ -460,8 +464,44 @@ .SH OPTIONS
 .\" ----------  X  ---------------------------------------------------------
 .\" ----------  Y  ---------------------------------------------------------
 .\" ----------  Z  ---------------------------------------------------------
-.so man/man1/o__rules.so
-.so man/man1/z_exit.so
+.\" .so man/man1/o__rules.so
+.r)
+.PP
+All options may be abbreviated;
+the abbreviation is documented as the upper case letters,
+all lower case letters and underscores (_) are optional.
+You must use consecutive sequences of optional letters.
+.PP
+All options are case insensitive,
+you may type them in upper case or lower case or a combination of both,
+case is not important.
+.PP
+For example:
+the arguments \[lq]\-help\[rq], \[lq]\-HELP\[rq] and \[lq]\-h\[rq] are
+all interpreted to mean the \fB\-Help\fP option.
+The argument \[lq]-hlp\[rq] will not be understood,
+because consecutive optional characters were not supplied.
+.PP
+Options and other command line arguments may be
+mixed arbitrarily on the command line,
+after the function selectors.
+.br
+.ne 4
+.PP
+The GNU long option names are understood.
+Since all option names for
+.I \*(n)
+are long,
+this means ignoring the extra leading '-'.
+The "\fB--\fIoption\fB=\fIvalue\fR" convention is also understood.
+.\" .so man/man1/z_exit.so
+.SH EXIT STATUS
+The
+.I \*(n)
+command will exit with a status of 1 on any error.
+The
+.I \*(n)
+command will only exit with a status of 0 if there are no errors.
 .SH SEE ALSO
 .TP 8n
 \f[I]ar\fP(1)
@@ -484,5 +524,33 @@ .SH SEE ALSO
 .TP 8n
 \f[I]tar\fP(5)
 Format of \f[I]tar\fP(1) archive files
-.so man/man1/z_cr.so
+.\" .so man/man1/z_cr.so
+.br
+.ne 2i
+.SH COPYRIGHT
+.ds V) 1.28.D001
+.ds v) 1.28
+.ds Y) 1993, 1994, 1995, 1996, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2006, 2008, 2009, 2010, 2011, 2012, 2013
+.ds W 1.28.D001
+\*(n) version \*(V)
+.br
+.if t .ds C) \(co
+.if n .ds C) (C)
+Copyright \*(C) \*(Y) Peter Miller
+.br
+.PP
+The \*(n) program comes with ABSOLUTELY NO WARRANTY;
+for details use the \[lq]\fI\*(n)\-VERSion License\fP\[rq] command.
+This is free software
+and you are welcome to redistribute it under certain conditions;
+for details use the \[lq]\fI\*(n)\-VERSion License\fP\[rq] command.
+.br
+.ne 1i
+.SH AUTHOR
+.TS
+tab(;);
+l r l.
+Peter Miller;EMail:;pmiller@opensource.org.au
+\f[CW]/\e/\e*\f[R];WWW:;http://miller.emu.id.au/pmiller/
+.TE
 .\" vim: set ts=8 sw=4 et :
