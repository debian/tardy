tardy (1.28-1) unstable; urgency=medium

  * QA upload.
  * New upstream version 1.28
    + Build-Depends: libbz2-dev
    + Build-Depends: liblzma-dev
    - Build-Depends: groff-base
      d/rules: don't try to build PDFs
    + d/rules: force -D_FILE_OFFSET_BITS=64
    + d/copyright: update years
  * d/p/
    + Rebase
      - fix-tardy_mtime-declaration.patch dropped, applied upstream
    + 0004 Replace boost with std::shared_ptr
    + 0005 Remove mismatched new[]/delete
    + 0006 Fix memory leak due to mismatched new[]/delete
    + 0007 Remove guaranteed-to-infinitely-loop functions
    + 0008 Clean up maybe-fallthrough warnings by adding [[noreturn]]s
    + 0009 Fix parallel builds
    + 0010 tests don't need all-doc
    + 0011 Insulate new "tarballs >2GB" test against timezone variations
  * d/control: drop build-depends: libboost-dev
  * Standards-Version: 4.7.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Secure URI in copyright format (routine-update)
  * watch file standard 4 (routine-update)
  * d/rules: actually run dh_autoreconf (upstream ships it under etc/)
  * d/copyright: fix obsolete-url-in-packaging
  * d/control: fix Homepage: obsolete-url-in-packaging
  * d/watch: trim upstream Aegis revision suffix to remove false positive

 -- наб <nabijaczleweli@nabijaczleweli.xyz>  Sun, 24 Nov 2024 15:40:34 +0100

tardy (1.25-3) unstable; urgency=medium

  * QA upload.
  * Import more history and move repository to salsa.debian.org.
  * Fix tardy_mtime declaration and format strings, thanks to наб.
    (Closes: #996482, #1068115)
  * The bullseye toolchain defaults to --as-needed.
  * Declare Rules-Requires-Root: no.

 -- Andreas Beckmann <anbe@debian.org>  Fri, 12 Apr 2024 11:24:17 +0200

tardy (1.25-2) unstable; urgency=medium

  * QA upload.
  * Set maintainer to Debian QA Group <packages@qa.debian.org>. (see: #920110)

 -- Marcos Talau <talau@debian.org>  Sun, 13 Nov 2022 21:38:48 -0300

tardy (1.25-1) unstable; urgency=low

  * New maintainer (Closes: #675765).
    - New upstream release.
  * debian/clean
    - New file.
  * debian/compat
    - Update to 9
  * debian/control
    - (Build-Depends): Update to debhelper 9. Add libexplain-dev,
      libboost-dev, cpio
    - (Description): Update first line for Lintian.
    - (Standards-Version): Update to 3.9.3.
    - (Vcs-*): Add.
  * debian/copyright
    - Update to format 1.0.
    - Correct license to GPL-3 (Closes: #604129).
  * debian/install
    - Rename. Was <package>.install.
  * debian/manpages
    - Update path.
  * debian/patches
    - (10): New. Correct manpage issues.
  * debian/rules
    - Update to dh(1).
    - Use hardened build flags.
      http://wiki.debian.org/ReleaseGoals/SecurityHardeningBuildFlags

 -- Jari Aalto <jari.aalto@cante.net>  Fri, 08 Jun 2012 21:08:49 +0300

tardy (1.20-0.1) unstable; urgency=low

  * Non-maintainer upload.
    - New upstream release (Closes: #573767).
    - Update to packaging format "3.0 (quilt)".
  * debian/compat
    - Update to 8.
  * debian/changelog
    - Delete EOL whitespaces.
  * debian/control
    - (Build-Depends): Update to debhelper 8. Remove dpatch.
    - (Depends): Add ${misc:Depends}.
    - (Homepage): Update to Freshmeat.
    - (Standards-Version): Update to 3.9.1.
  * debian/copyright
    - Point to GPL-2.
    - Update layout and use FSF URL.
  * debian/manpages
    - New file.
  * debian/patches
     - Remove. All included in upstream.
  * debian/rules
    - Remove dpatch; obsoleted by new package format.
    - Delete EOL whitespaces.
    - (clean): Fix lintian debian-rules-ignores-make-clean-error.
      Move config.* copy commands to target 'config.status'.
      Delete config.* files copied from autotools-dev so that they
      don't appear in diff.
    - (config.status): Add copy commands for config.* files.
    - (install): Update dh_clean to dh_prep. install new config.*
  * debian/source/format
    - New file.
  * debian/watch
    - update from version 2 to 3. Tighten regexp.

 -- Jari Aalto <jari.aalto@cante.net>  Fri, 15 Oct 2010 14:29:24 +0300

tardy (1.12-3.1) unstable; urgency=low

  [Jari Aalto]
  * Non-maintainer upload.
  * debian/patches
    - (number 10): Add patch to fix GCC 4.4 invalid conversion from const
      char* to char*. (RC bug FTBFS serious; Closes: #560485).

 -- Jari Aalto <jari.aalto@cante.net>  Mon, 18 Jan 2010 00:02:15 +0200

tardy (1.12-3) unstable; urgency=low

  * Fixup for wrong manpage section in package description
    (Closes: #407961)
  * Fixup for FTBFS with GCC 4.3: missing #includes
    (Closes: #454859)
  * Cleaned up lintian warnings / errors
    - W: tardy source: debian-rules-sets-DH_COMPAT line 9
      Moved DH_COMPAT to debian/compat
    - W: tardy source: debian-rules-sets-DH_COMPAT line 9
    - W: tardy source: ancient-standards-version 3.6.1 (current is 3.7.3)
      Bumped Standards to 3.7.3
    - W: tardy source: dpatch-build-dep-but-no-patch-list tardy
      Including patch 01_gcc4.3_headers.dpatch to fix #454859
    - I: tardy: hyphen-used-as-minus-sign usr/share/man/man1/tardy.1.gz:80
      Including patch 02_fix_hyphen_in_manpage to fix it
    - W: tardy: description-contains-homepage
      Moved Homepage statement to Source section in debian/control

 -- Michael Schiansky <ms@debian.org>  Sun, 30 Dec 2007 09:00:00 +0100

tardy (1.12-2) unstable; urgency=high

  * Added build-dep zlib1g-dev

 -- Michael Schiansky <ms@debian.org>  Tue, 31 Aug 2004 15:34:20 +0200

tardy (1.12-1) unstable; urgency=high

  * New upstream release
  * Fixes:
    - 'output makes tar print a warning'
      (Closes: #255310)
    - '-remove_prefix should remove prefix directory as well'
      (Closes: 255305)
    - 'please consider adding -remove_prefix_by_number'
      (Closes: 254879)
  * Updated control
    - New Homepage
    - Updated Builddeps
  * Updated rules
    - Now using debhelper and dpatch
  * Added watch file
  * Urgency=high so Sarge is possible.
    Old version of tardy sucks with tar >= 1.14

 -- Michael Schiansky <ms@debian.org>  Mon, 30 Aug 2004 02:02:08 +0200

tardy (1.11-4) unstable; urgency=low

  * New maintainer (now DD)
  * Bumped Standards to 3.6.1

 -- Michael Schiansky <ms@debian.org>  Mon, 23 Feb 2004 22:16:50 +0100

tardy (1.11-3) unstable; urgency=low

  * New maintainer (Closes: #188105)
  * Updated debian/control added homepage
  * Updated debian/copyright

 -- Michael Schiansky <michael@schiansky.de>  Thu, 11 Sep 2003 02:59:12 +0200

tardy (1.11-2) unstable; urgency=low

  * Remove full stop from short description.

 -- Matt Kraai <kraai@debian.org>  Wed, 16 Jul 2003 16:42:50 +0200

tardy (1.11-1) unstable; urgency=low

  * Package new version.
  * Remove postinst and prerm.
  * Update copyright.
  * Update Standards-Version.
  * Stamp build.

 -- Matt Kraai <kraai@debian.org>  Fri,  8 Nov 2002 17:00:53 -0800

tardy (1.9-3) unstable; urgency=low

  * Add a `using std::deque;' declaration.

 -- Matt Kraai <kraai@debian.org>  Wed, 25 Sep 2002 21:21:00 -0700

tardy (1.9-2) unstable; urgency=low

  * Correct -Output_ForMaT documentation (closes: #162216).

 -- Matt Kraai <kraai@debian.org>  Tue, 24 Sep 2002 22:25:50 -0700

tardy (1.9-1) unstable; urgency=low

  * Package new version.
  * Set CFLAGS to `-O2 -g -Wall'.

 -- Matt Kraai <kraai@debian.org>  Mon, 16 Sep 2002 16:35:53 -0700

tardy (1.8-4) unstable; urgency=low

  * Install this file as changelog.Debian.gz.

 -- Matt Kraai <kraai@debian.org>  Sat, 31 Aug 2002 19:44:29 -0700

tardy (1.8-3) unstable; urgency=low

  * Add a `using std::vector;' declaration (closes: #155733).

 -- Matt Kraai <kraai@debian.org>  Wed,  7 Aug 2002 08:53:39 -0700

tardy (1.8-2) unstable; urgency=low

  * Add groff-base to Build-Depends.

 -- Matt Kraai <kraai@debian.org>  Tue,  6 Aug 2002 11:38:53 -0700

tardy (1.8-1) unstable; urgency=low

  * Package.

 -- Matt Kraai <kraai@debian.org>  Fri,  2 Aug 2002 07:19:57 -0700
