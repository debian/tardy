".br",
".ne 2i",
".SH COPYRIGHT",
".so etc/version.so",
"\\*(n) version \\*(V)",
".br",
".if t .ds C) \\(co",
".if n .ds C) (C)",
"Copyright \\*(C) \\*(Y) Peter Miller",
".br",
".PP",
"The \\*(n) program comes with ABSOLUTELY NO WARRANTY;",
"for details use the \\[lq]\\fI\\*(n)\\-VERSion License\\fP\\[rq] command.",
"This is free software",
"and you are welcome to redistribute it under certain conditions;",
"for details use the \\[lq]\\fI\\*(n)\\-VERSion License\\fP\\[rq] command.",
".br",
".ne 1i",
".SH AUTHOR",
".TS",
"tab(;);",
"l r l.",
"Peter Miller;EMail:;pmiller@opensource.org.au",
"\\f[CW]/\\e/\\e*\\f[R];WWW:;http://miller.emu.id.au/pmiller/",
".TE",
